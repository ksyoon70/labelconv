import os, shutil
import sys
from pathlib import Path

#------------------------------
# 이 파일을 yolo dataset에서 이미지와 txt 쌍이 맞는지 확인한다.
#------------------------------

src_dir = r'E:\SPB_Data\labelconv\dataset\yolo\images'
src_dir = os.path.normpath(src_dir)
src_dir = Path(src_dir)



image_ext = ['jpg','JPG','png','PNG']
files = [fn for fn in os.listdir(src_dir)
                if any(fn.endswith(ext) for ext in image_ext)]

yolo_ext = ['txt']
yolo_files = [fn for fn in os.listdir(src_dir)
                if any(fn.endswith(ext) for ext in yolo_ext)]

err_image_list = []

for fn in files:

    basename, ext = os.path.splitext(fn)
    yfile = basename + '.txt'

    if yfile not in yolo_files:
        err_image_list.append(fn)

print('다음 txt 파일이 매칭 되지 않습니다. {}'.format(err_image_list))

err_label_list = []
for fn in yolo_files:

    basename, ext = os.path.splitext(fn)
    image_file = basename + '.png'

    if image_file not in files:
        err_label_list.append(fn)

print('다음 이미지 파일이 매칭 되지 않습니다. {}'.format(err_label_list))