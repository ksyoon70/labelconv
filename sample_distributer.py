"""
Created on 2022년 7월 31일
레이블 리스트를 읽어 레이블를 확인한다.
영상파일에서 레이블에 해당하는 폴더를 만들고 해당 폴더로 영상을 이동한다.
예) python image_distributer -l ./LPR_Labels1.txt -i ./image -o ./result -t ch -r [0.7,0.3]
-l label 파일
-i 이미지 위치
-o 결과 파일 저장 위치
-r train validation 분배 비율 default : 0.7,0.3 이다.
@author:  윤경섭
"""

import os,sys,shutil
import argparse
import pandas as pd
from label_tools import *
import random
import math

#------------------------------
# 수정할 내용
ROOT_DIR = os.path.dirname(__file__)
MIDDLE_PATH_NAME = 'datasets'
OUTPUT_FOLDER_NAME = 'out' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
DEFAULT_OBJ_TYPE = 'or'
DEFAULT_LABEL_FILE = "./LPR_Total_Labels.txt" #"./LPR_Labels1.txt"  #라벨 파일이름
option_move = False # 원 파일을 옮길지 여부
option_remsrc_tree = True #원본 폴더를 삭제할지 여부
INCULUDE_R6_TO_OREGION = True   #r6 번호판을 or 에 포함 할지 여부
MIDDLE_PATH =  os.path.join('dataset','test')
OUTPUT_MIDDLE_FOLDER_NAME  ='result'
CHAR_DET_FOLDER = os.path.join(MIDDLE_PATH,OUTPUT_MIDDLE_FOLDER_NAME,'CHAR')  #복사할 폴더의 디렉토리 이름
MCHAR_DET_FOLDER = os.path.join(MIDDLE_PATH,OUTPUT_MIDDLE_FOLDER_NAME,'MCHAR')  #이륜차 번호인식 문자 복사할 폴더의 디렉토리 이름

#------------------------------
class_str = None   #클래스의 이름을 저장한다.

for SOURCE_DIR in [CHAR_DET_FOLDER,MCHAR_DET_FOLDER]:

    OBJECT_TYPES = ['ch','hr','vr','or','r6','n','veh']

    for DEFAULT_OBJ_TYPE in OBJECT_TYPES :

        if DEFAULT_OBJ_TYPE == 'ch':        #문자 검사
            IMAGE_FOLDER_NAME = 'ch_images'
            class_str = "character"
        elif DEFAULT_OBJ_TYPE == 'hr':       #가로 지역문자 검사
            IMAGE_FOLDER_NAME = 'hr_images'
            class_str = "hregion"
        elif DEFAULT_OBJ_TYPE == 'vr':     # 세로지역문자 검사
            IMAGE_FOLDER_NAME = 'vr_images'
            class_str = "vregion"
        elif DEFAULT_OBJ_TYPE == 'or':       #o 지역문자 검사
            IMAGE_FOLDER_NAME = 'or_images'
            class_str = "oregion"
        elif DEFAULT_OBJ_TYPE == 'r6':     #r6 지역문자 검사
            IMAGE_FOLDER_NAME = 'r6_images'
            if INCULUDE_R6_TO_OREGION:
                class_str = "oregion"
            else :
                class_str = "region6"
        elif DEFAULT_OBJ_TYPE == 'n':       # 숫자 검사
            IMAGE_FOLDER_NAME = 'n_images'
            class_str = "number"
        elif DEFAULT_OBJ_TYPE == 'veh':       #자동차 검사
            IMAGE_FOLDER_NAME = 'veh_images'
            class_str = "vehicle"   
        elif DEFAULT_OBJ_TYPE == 'r' :       #r 지역 검사
            IMAGE_FOLDER_NAME = 'r_images'
            class_str = "region"  
        else:
            print("{0} image folder name is Not supporeted".format(IMAGE_FOLDER_NAME))
            sys.exit(0)
        
        print('{}을 처리합니다.'.format(IMAGE_FOLDER_NAME))
            
        OUTPUT_FOLDER_NAME = class_str

        
        DEFAULT_IMAGES_PATH = os.path.join(SOURCE_DIR,IMAGE_FOLDER_NAME)
        DEFAULT_OUPUT_PATH = os.path.join(SOURCE_DIR,OUTPUT_FOLDER_NAME)


        # Initiate argument parser
        parser = argparse.ArgumentParser(
            description="object split and save in jpeg and annotation files")

        parser.add_argument("-l",
                            "--labelfile",
                            help="Label file where the text files are stored.",
                            type=str,default=DEFAULT_LABEL_FILE)

        parser.add_argument("-i",
                            "--image_dir",
                            help="Path to the folder where the input image files are stored. ",
                            type=str, default=DEFAULT_IMAGES_PATH)
        # 출력 디렉토리를 설정ㅎㄴ다.
        parser.add_argument("-o",
                            "--output_dir",
                            help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)
        # 검색할 object type를 설정한다. 
        parser.add_argument("-t",
                            "--object_type",
                            help="object type ch : character n: number r: region", type=str,default=DEFAULT_OBJ_TYPE)
        # training / validateion  비율을 설정한다.
        parser.add_argument("-r",
                            "--ratio", type=float,
                            help="train validation ratio ex[1.0,0.0] ",default=[0.7,0.3], required=False)


        args = parser.parse_args()

        fLabels = pd.read_csv(args.labelfile, header = None )
        LABEL_FILE_CLASS = fLabels[0].values.tolist()
        LABEL_FILE_HUMAN_NAMES = fLabels[1].values.tolist()
        CLASS_DIC = dict(zip(LABEL_FILE_CLASS, LABEL_FILE_HUMAN_NAMES))

        #클래스를 각각 그룹별로 나눈다.
        CH_CLASS =  LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('Ga'):LABEL_FILE_CLASS.index('Char') + 1] #문자열 클래스
        NUM_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('n1'):LABEL_FILE_CLASS.index('n0') + 1]  #숫자 클래스
        REGION_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('vSeoul'):LABEL_FILE_CLASS.index('UlSan6') + 1] #지역문자 클래스
        VREGION_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('vSeoul'):LABEL_FILE_CLASS.index('vReg') + 1] #Vertical 지역문자 클래스
        HREGION_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('hSeoul'):LABEL_FILE_CLASS.index('hReg') + 1] #Horizontal 지역문자 클래스
        OREGION_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('OpSeoul'):LABEL_FILE_CLASS.index('OpUlSan') + 1] #Orange 지역문자 클래스
        REGION6_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('Seoul6'):LABEL_FILE_CLASS.index('UlSan6') + 1] #6 지역문자 클래스
        VEHICLE_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('car'):LABEL_FILE_CLASS.index('hood') + 1] #6 자동차 클래스
        
        #사람이 볼수있는 이름으로 나눈다.     
        CH_HUMAN_NAMES = LABEL_FILE_HUMAN_NAMES[LABEL_FILE_CLASS.index('Ga'):LABEL_FILE_CLASS.index('Char') + 1]  #문자열 클래스
        NUM_HUMAN_NAMES = LABEL_FILE_HUMAN_NAMES[LABEL_FILE_CLASS.index('n1'):LABEL_FILE_CLASS.index('n0') + 1]  #숫자 클래스
        REGION_HUMAN_NAMES = LABEL_FILE_HUMAN_NAMES[LABEL_FILE_CLASS.index('vSeoul'):LABEL_FILE_CLASS.index('UlSan6') + 1] #지역문자 클래스
        VREGION_HUMAN_NAMES = LABEL_FILE_HUMAN_NAMES[LABEL_FILE_CLASS.index('vSeoul'):LABEL_FILE_CLASS.index('vReg') + 1] #Vertical 지역문자 클래스
        HREGION_HUMAN_NAMES = LABEL_FILE_HUMAN_NAMES[LABEL_FILE_CLASS.index('hSeoul'):LABEL_FILE_CLASS.index('hReg') + 1] #Horizontal 지역문자 클래스
        OREGION_HUMAN_NAMES = LABEL_FILE_HUMAN_NAMES[LABEL_FILE_CLASS.index('OpSeoul'):LABEL_FILE_CLASS.index('OpUlSan') + 1] #Orange 지역문자 클래스
        REGION6_HUMAN_NAMES = LABEL_FILE_HUMAN_NAMES[LABEL_FILE_CLASS.index('Seoul6'):LABEL_FILE_CLASS.index('UlSan6') + 1] #6 지역문자 클래스
        VEHICLE_HUMAN_NAMES = LABEL_FILE_HUMAN_NAMES[LABEL_FILE_CLASS.index('car'):LABEL_FILE_CLASS.index('hood') + 1] #자동차 클래스
        
        train_ratio = args.ratio[0]
        validation_ratio = 1.0 - args.ratio[0]

        class_label = [];
        human_names= [];

        if args.object_type == 'ch':        #문자 검사
            class_label = CH_CLASS
            human_names = CH_HUMAN_NAMES
        elif args.object_type == 'n':       #숫자검사
            class_label = NUM_CLASS
            human_names = NUM_HUMAN_NAMES
            #print("{0} type is Not supporeted yet".format(args.object_type))
            #sys.exit(0)
        elif args.object_type == 'r':       #지역문자 검사
            class_label = REGION_CLASS
            human_names = REGION_HUMAN_NAMES
        elif args.object_type == 'vr':       #v 지역문자 검사
            class_label = VREGION_CLASS
            human_names = VREGION_HUMAN_NAMES
        elif args.object_type == 'hr':       #h 지역문자 검사
            class_label = HREGION_CLASS
            human_names = HREGION_HUMAN_NAMES
        elif args.object_type == 'or':       #o 지역문자 검사
            class_label = OREGION_CLASS
            human_names = OREGION_HUMAN_NAMES
        elif args.object_type == 'r6':       #6 지역문자 검사
            if INCULUDE_R6_TO_OREGION:
                class_label = OREGION_CLASS
            else:
                class_label = REGION6_CLASS
            human_names = REGION6_HUMAN_NAMES
        elif args.object_type == 'n':       #number 지역문자 검사
            class_label = NUM_CLASS
            human_names = NUM_HUMAN_NAMES
        elif args.object_type == 'veh':      #자동차  검사
            class_label = VEHICLE_CLASS
            human_names = VEHICLE_HUMAN_NAMES       
        else:
            print("{0} type is Not supporeted".format(args.object_type))
            sys.exit(0)
 
        #이미지 폴더가 있는지 확인한다.

        if not os.path.exists(args.image_dir) :
            print("No images folder exists. check the folder :",args.image_dir)
            sys.exit(0)
        
        #기존 폴더 아래 있는 출력 폴더를 지운다.
        # r6이고 r6를 or에 포함 시킬 때가 아닐 때만 삭제한다.
        if not (DEFAULT_OBJ_TYPE == 'r6' and INCULUDE_R6_TO_OREGION == True):
            if os.path.exists(args.output_dir) :
                shutil.rmtree(args.output_dir) 

        
        

        #클래스 디렉토리를 만든다.
        #파일이 없는데도 만들어야 하는지 모르겠지만, 일단 만드는 코드는 아래 내용이다.
        #확인 결과 없는 영상에 대해서는 class가 무의미하다.

        # for label in class_label :
        #     tlabel_dir = os.path.join(args.output_dir,'train',label)
        #     if not os.path.exists(tlabel_dir):
        #         createFolder(tlabel_dir)
        #     vlabel_dir = os.path.join(args.output_dir,'validation',label)
        #     if not os.path.exists(vlabel_dir):
        #         createFolder(vlabel_dir)


        # images 디렉토리에서 image 파일을 하나씩 읽어 들인다. 
        src_dir = args.image_dir
        dst_dir = args.output_dir

        print('{} 에 파일에서 영상을 읽습니다.'.format(src_dir ))
        print('{} 에 파일을 저장합니다.'.format(dst_dir ))

        if os.path.exists(args.image_dir):
            image_ext = ['jpg','JPG','png','PNG']
            files = [fn for fn in os.listdir(src_dir)
                        if any(fn.endswith(ext) for ext in image_ext)]
            sfiles = []  #source file list
            for file in files:
                label = file.split('_')[-1]
                label = label[0:-4]
                # english class label을 얻는다. 
                if label in human_names :
                    sfiles.append(file)
                else :
                    continue
                
            # sfile 을 랜덤하게 섞고 난 후 비율 대로 분리한다.
            fileLen = len(sfiles)
            if fileLen :
                print('처리할 파일 갯수 {0}'.format(fileLen))
                train_file_count = int(fileLen)
                tfiles = sfiles
                if len(tfiles) :
                    if not os.path.exists(args.output_dir) :
                        createFolder(args.output_dir)
                    for tfile in tfiles:
                        label = tfile.split('_')[-1]
                        label = label[0:-4]
                        en_label = class_label[human_names.index(label)]
                        en_label_dir = os.path.join(dst_dir,en_label)
                        if not os.path.exists(en_label_dir):
                            createFolder(en_label_dir)
                        #파일을 레이블 디렉토리로 복사한다.
                        src = os.path.join(src_dir,tfile)
                        dst = os.path.join(en_label_dir,tfile)
                        if option_move :
                            shutil.move(src,dst)
                        else :
                            shutil.copy(src,dst)
                    #복사가 끝나면 폴더를 삭제한다.
                    if os.path.exists(src_dir) and option_remsrc_tree:
                        shutil.rmtree(src_dir)
                    
            else :
                print('처리할 파일이 없습니다')
                if os.path.exists(src_dir) and option_remsrc_tree:
                        shutil.rmtree(src_dir)
                
            print('처리완료')      
        else :
            print("Error! no json directory:",args.json_dir)       