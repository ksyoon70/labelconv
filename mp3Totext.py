import speech_recognition as sr
from pydub import AudioSegment
import os, shutil
import sys
from pathlib import Path, PureWindowsPath
from pathlib import Path
from pydub.utils import which
#-------------바꿀 내용-----------------------------------
# MP3 파일 경로
file_path = r'D:\share\20240716_atc_a_victory_for_opponents_of_female_genital_mutilation_in_the_gambia.mp3'
ffmpeg_path = r'C:\ffmpeg\bin'
#------------------------------------------------
file_path = os.path.normpath(file_path)
file_path = Path(file_path)

ffmpeg_path = os.path.normpath(ffmpeg_path)
ffmpeg_path = Path(ffmpeg_path)

def file_exists(file_path):
    return os.path.exists(file_path)

if file_exists(file_path):
    print(f"{file_path} 파일이 존재합니다.")
else:
    print(f"{file_path} 파일이 존재하지 않습니다.")
    sys.exit(0)

# MP3 파일 이름 추출
file_name = os.path.splitext(os.path.basename(file_path))[0]

AudioSegment.converter = os.path.join(ffmpeg_path,'ffmpeg.exe')
AudioSegment.ffmpeg = os.path.join(ffmpeg_path,'ffmpeg.exe')
AudioSegment.ffprobe = os.path.join(ffmpeg_path,'ffprobe.exe')

# MP3를 WAV로 변환
audio = AudioSegment.from_mp3(file_path)
wav_path = f"{file_name}.wav"
audio.export(wav_path, format="wav")
# 음성 인식
recognizer = sr.Recognizer()

with sr.AudioFile(wav_path) as source:
    audio_data = recognizer.record(source)
    try:
        #text = recognizer.recognize_google(audio_data, language='ko-KR')  #한국어
        text = recognizer.recognize_google(audio_data, language='en-US')
        sentences = text.split('. ')
        formatted_text = '.\n'.join(sentence.strip() + '.' for sentence in sentences if sentence.strip())
    except sr.UnknownValueError:
        text = "오디오에서 음성을 인식할 수 없습니다."
    except sr.RequestError as e:
        text = f"Google 음성 인식 서비스에 접근할 수 없습니다; {e}"

# 텍스트 파일로 저장
text_file_path = f"{file_name}.txt"

if file_exists(text_file_path):
    os.remove(text_file_path)

with open(text_file_path, "w", encoding='utf-8') as text_file:
    text_file.write(text)

print(f"{text_file_path} 파일변환이 끝났습니다.")