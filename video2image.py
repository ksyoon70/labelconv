


import cv2, os,sys
import argparse
from pathlib import Path, PureWindowsPath
from pathlib import Path
from label_tools import *

#------------------------------
# 수정할 내용

OUTPUT_FOLDER_NAME = 'images' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
VIDEO_FOLDER_NAME = 'video' #mp4 파일에 있는 영상 파일이 있는 경로
MIDDLE_PATH = os.path.join('dataset','yolo') #'dataset' #os.path.join('dataset','test')
RESIZE_RES = (1280, 720)
#------------------------------

ROOT_DIR = os.path.dirname(__file__)

src_dir = r'D:\SPB_Data\labelconv\dataset\yolo\video'
src_dir = os.path.normpath(src_dir)
src_dir = Path(src_dir)

#DEFAULT_VIDEO_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,VIDEO_FOLDER_NAME)
DEFAULT_VIDEO_PATH = src_dir

DEFAULT_OUPUT_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,OUTPUT_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="mp4-to-images converter")
parser.add_argument("-v",
                    "--video_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_VIDEO_PATH)
parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)


args = parser.parse_args()

if not os.path.exists(args.video_dir) :
    print("No video folder : {} exists. check the folder :".format(args.video_dir))
    sys.exit(0)

if not os.path.exists(args.output_path) :
    createFolder(args.output_path)
    

if os.path.exists(args.video_dir):
    
    video_ext = 'mp4'
    video_file_names = [fn for fn in os.listdir(args.video_dir)
                  if any(fn.endswith(ext) for ext in video_ext)]

if len(video_file_names) > 0 :

    v_cap = cv2.VideoCapture(os.path.join(args.video_dir, video_file_names[0]))
    
    basename, ext = os.path.splitext(video_file_names[0])
    cnt = 0
    while v_cap.isOpened():
        try:
            ret, image = v_cap.read()
            #image = cv2.resize(image, (1920, 1080))
            image = cv2.resize(image, RESIZE_RES)
            if int(v_cap.get(1)) % 10 == 0:
                imwrite(os.path.join(args.output_path, basename + '_' + '%d.png' % v_cap.get(1)), image)
                print("Frame Captured: %d" % v_cap.get(1))
            cnt += 1
        except :
            print("Error: image write error")
            break
    v_cap.release()
else :
    print('{}에 video(mp4) 파일이 없습니다.'.format(args.video_dir))
