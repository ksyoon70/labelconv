"""
Created on 2023년 07월11일
빠진 오브젝트에 대한 정보를 찾아서 추가한다.
@author:  윤경섭
"""

import os,sys
import argparse
import pandas as pd
import cv2
from label_tools import *
import matplotlib.pyplot as plt
#------------------------------
# 수정할 내용

OUTPUT_MIDDLE_FOLDER_NAME  ='result'

IMAGE_FOLDER_NAME = 'images' #이미지 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'images' #json annots 폴더가 있는 경로
MIDDLE_PATH =  os.path.join('dataset','labels')
DEFAULT_LABEL_FILE = "./LPR_Total_Labels.txt"  #라벨 파일이름
CROP_MARGIN = 0
DEFULT_FIXED_RATIO=False         #영상의 크기를 싸이즈에 맞게 늘일지 여부 True : 고정 False : 늘림
DEFAULT_TYPE='vr' #읽어올 default 타입을 설정한다. ch: 문자  n: number r:지역문자 vr:세로 지역문자 hr:가로지역문자 or: 영지역문자 r6: 6지역문자

DEFAULT_OUPUT_PATH = os.path.join(MIDDLE_PATH,OUTPUT_MIDDLE_FOLDER_NAME)  #복사할 폴더의 디렉토리 이름


OBJTYPES = ['car','truck','bus','motorcycle','bicycle','kickboard','human']
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)

parser = argparse.ArgumentParser(
        description="object split and save in jpeg and annotation files")

parser.add_argument("-l",
                        "--labelfile",
                        help="Label file where the text files are stored.",
                        type=str,default=DEFAULT_LABEL_FILE)

parser.add_argument("-j",
                    "--json_dir",
                    help="Complete Path to the file where the input .json files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
# 출력 디렉토리를 설정한다.
parser.add_argument("-o",
                    "--output_dir",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)

args = parser.parse_args()

if not os.path.exists(args.output_dir) :
    createFolder(args.output_dir)
    
    
json_data = OrderedDict()


# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.json_dir):

    json_ext = 'json'
    file_names = [fn for fn in os.listdir(args.json_dir)
                if any(fn.endswith(ext) for ext in json_ext)]

    process_num = len(file_names)
    
    block_num = int(process_num / 20)
    
    print("Total process file count is {0}".format(process_num))
    cnt = 0
    
    
    for filename in file_names :
        cnt = cnt + 1
        #print("Processing : {0}".format(filename))
        if cnt % block_num == 0 :
            print("Processing : {0} %".format(cnt*100/process_num))
        
        #json  파일을 연다.
        try:
            
            with open(os.path.join(args.json_dir,filename), 'r',encoding="UTF-8") as f:
                json_data = json.load(f)
                
        except IOError:
                print("Error: File does not appear to exist")
                continue
            
            
        json_data['imageData'] = None
        src_image_filename = json_data['imagePath']
        
        basename, ext = os.path.splitext(filename)
        
        find_object = False
        
        for item, shape in enumerate(json_data['shapes']):
            label = shape['label']
            
            if label in OBJTYPES:
                find_object = True
                break
            
        if find_object:
            continue
        #해당 오브젝트가 없으면 영상을 복사한다.
        
        src_dir = args.image_dir
        dst_dir = args.output_dir
        
        if not os.path.exists(args.output_dir) :
            createFolder(args.output_dir)
        
        # copy json
        src = os.path.join(src_dir,filename)
        dst = os.path.join(dst_dir,filename)
        if os.path.exists(src):
            shutil.copy(src,dst)
        #copy image
        src = os.path.join(src_dir,src_image_filename)
        dst = os.path.join(dst_dir,src_image_filename)
        if os.path.exists(src):
            shutil.copy(src,dst)
    
    
    print("Processing : 100 % Completed")            