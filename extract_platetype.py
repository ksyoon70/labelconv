# 이미지 파일과 json 파일에서 원하는 타입의 번호판 만 추출하는 스크립트이다.
# 작성자 윤경섭
# 작성일 2022년 12월 14일

import os, shutil
import sys
import argparse
import json	
from pathlib import Path, PureWindowsPath
from pathlib import Path
#---------------------------------------------------------
DEFAULT_PLATETYPE_LABEL = ['helmet'] #['car','bus','truck', 'motorcycle','bicycle','human','kickboard'] #['plate'] #['car','bus','truck'] #['type1','type4','type5','type6','type7']  #추출하고자 하는 plate type를 지정한다. type1 ~ type13까지 가능
PLATETYPE_MOVE = True  #번호판 파일을 옮길지 여부
"""
base_dir = os.path.abspath(os.path.dirname(__file__))
database_dir  =  os.path.join(base_dir,'dataset')
labels_dir = os.path.join(database_dir,'labels')
annots_dir = os.path.join(labels_dir,'images')
images_dir = os.path.join(labels_dir,'images')
out_dir = os.path.join(labels_dir,'out')
"""
src_dir = r'E:\dataset\경성고\경성고221105'
src_dir = os.path.normpath(src_dir)
src_dir = Path(src_dir)

images_dir = src_dir
annots_dir = src_dir

out_dir = r'D:\SPB_Data\labelconv\dataset\test\out'
out_dir = os.path.normpath(out_dir)
out_dir = Path(out_dir)

#---------------------------------------------------------

if not os.path.exists(out_dir) :
    os.makedirs(out_dir)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="이미지 파일과 json 파일에서 원하는 타입의 번호판 만 추출하는 스크립트이다")

parser.add_argument("-t",
                    "--type",
                    help="찾으려는 번호판 type을 입력한다",
                    type=str,default=DEFAULT_PLATETYPE_LABEL)

args = parser.parse_args()    

image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
imagefile_list = [fn for fn in os.listdir(images_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]

total_files_count = len(imagefile_list)

print('검토할 총 영상 수: {}'.format(total_files_count))

for filename in imagefile_list:
    
    # 해당 이미지에 해당하는 json 파일을 찾는다.
    bFind = False #원하는 레이블을 찾았는지 여부
    jsonfilename = os.path.splitext(filename)[0] +'.json'
    if os.path.isfile(os.path.join(annots_dir,jsonfilename)):
        #json 파일을 읽는다.
        
        with open(os.path.join(annots_dir,jsonfilename), 'r',encoding="UTF-8") as f:
            json_data = json.load(f)
            
            for item, shape in enumerate(json_data['shapes']):
                label = shape['label']
                if label in args.type :
                    bFind = True
                    break
                elif 'type13' in args.type:  #이륜차이면 이륜차 번호판 없어도 헬멧이나 모터싸이클이란 차량만 있어도 찾은 것으로 한다.
                    if label == 'helmet' or label == 'motorcycle':
                        bFind = True
                        break
                else :
                    continue
                
        f.close
        
        if bFind :
           src_file = os.path.join(images_dir,filename) 
           dst_file = os.path.join(out_dir,filename)
           if PLATETYPE_MOVE:
               shutil.move(src_file,dst_file)
           else:
               shutil.copy(src_file,dst_file) 
                    
           src_file = os.path.join(annots_dir,jsonfilename)
           dst_file = os.path.join(out_dir,jsonfilename)
           if PLATETYPE_MOVE:
               shutil.move(src_file,dst_file)
           else:
               shutil.copy(src_file,dst_file)    
    
    
print('작업이 종료되었습니다.')