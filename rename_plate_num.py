# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 14:21:05 2023

@author: user1
"""

import os,sys
import cv2
import colorsys
import time 
#import tensorflow as tf

import glob
import json
import pandas as pd
import io
import xml.etree.ElementTree as ET
import argparse
from shutil import move
from shutil import copyfile

from os import makedirs
from os import listdir
from shutil import copyfile
from random import seed
from random import random

import os,sys
import numpy as np
import shutil

from os import makedirs
from os import listdir
from shutil import copyfile
from random import seed
from random import random



#========================
# 여기의 내용을 용도에 맞게 수정한다.

# 여기의 내용을 용도에 맞게 수정한다.
test_dir_name = '01_124차_파일명변경전'
move_dir_name = '90_학습데이터_124차'
move_json_error_dir_name = '90_학습데이터_124차\\move_json_error'

test_dir_name = r'01_124차_파일명변경전'
move_dir_name = '90_학습데이터_124차'
move_json_error_dir_name = 'move_json_error'
move_json_cartype_ok_dir_name = 'move_cartype_ok'
move_json_cartype_nk_dir_name = 'move_cartype_nk'
move_json_nomatching_dir_name = 'move_nomatching'
#========================

ROOT_DIR = os.getcwd()   

debug_flag = False
#debug_flag = True


# 포인트가 Polygon 내에 있는지 확인한다. 포인트가 폴리곤 안에 있으면 True 아니면 False를 리턴한다.
# 모든 입력 포인트는 numpy로 입력 받는다.
def PointInPolygon(tpoint, polygon):
    """
    한 점(point)이 다각형(polygon)내부에 위치하는지 외부에 위치하는지 판별하는 함수
    입력값
        polygon -> 다각형을 구성하는 리스트 정보
        point -> 판별하고 싶은 점
    출력값
        내부에 위치하면 res = 1
        외부에 위치하면 res = 0
    """
    
    N = len(polygon)   # N각형을 의미
    #print(f'N:{N} tpoint:{tpoint} polygon:{polygon}')
    counter = 0
    p1 = polygon[0]
    for i in range(1, N+1):
        p2 = polygon[i%(N)]
        #print(f'=1= i:{i} tpoint:{tpoint} p1:{p1} p2:{p2}')
        #print(f'i:{i} tpoint[0]:{tpoint[0]} p1[0]:{p1[0]}, p2[0]:{p2[0]}')
        #print(f'i:{i} tpoint[1]:{tpoint[1]} p1[1]:{p1[1]}, p2[1]:{p2[1]}')
        if tpoint[1] > min(p1[1], p2[1]) and tpoint[1] <= max(p1[1], p2[1]) and tpoint[0] <= max(p1[0], p2[0]) and p1[1] != p2[1]:
            xinters = (tpoint[1]-p1[1])*(p2[0]-p1[0])/(p2[1]-p1[1]) + p1[0]
            #print(f'=1= {xinters} = ({tpoint[1]}-{p1[1]})*({p2[0]}-{p1[0]})/({p2[1]}-{p1[1]}) + {p1[0]}' )
            if(p1[0]==p2[0] or tpoint[0]<=xinters):
                #print(f'=2= i:{i} tpoint:{tpoint} p1:{p1} p2:{p2}')
                counter += 1
        p1 = p2
        #print(f'=3= i:{i} tpoint:{tpoint} p1:{p1} p2:{p2}')
        
    
    #print(f'counter:{counter} p1:{p1} p2:{p2}')
    res=False
    if(counter==0):
        p1 = polygon[0]
        for i in range(1, N+1):
            p2 = polygon[i%(N)]
            """
            #sense 20230705
            #확인하려는 포인트가 직선위에 있는 경우 counter가 0이 되므로 이 경우는 다각형 안으로 처리한다
            #따라서 두점을 지나는 직선의 방정식에 포인트를 대입하여 0이 되는 경우에는 내부로 처리한다.
            if(p2[1]!=p1[1]):
                xinters = (tpoint[1]-p1[1])*(p2[0]-p1[0])/(p2[1]-p1[1]) + p1[0]
                print(f'=2= {xinters} = ({tpoint[1]}-{p1[1]})*({p2[0]}-{p1[0]})/({p2[1]}-{p1[1]}) + {p1[0]}' )
            else :
                xinters = p1[0]
                
            if(xinters != p1[0]):
                res = False
            else:
                res = True
                break
            """
            if tpoint[1] >= min(p1[1], p2[1]) and tpoint[1] <= max(p1[1], p2[1]) and tpoint[0] <= max(p1[0], p2[0]) and tpoint[0] >= min(p1[0], p2[0]):
                if(p1[0]==p2[0]):
                    if((tpoint[0]==p1[0]) or (tpoint[0]==p2[0])):
                        res = True
                        break
                elif(p1[1]==p2[1]):
                    if((tpoint[1]==p1[1]) or (tpoint[1]==p2[1])):
                        res = True
                        break
                else:
                    gradient  = (p1[0]-p2[0])/(p1[1]-p2[1])
                    
                    if(p1[1]!=tpoint[1]):
                        tp_gradient = ((p1[0]-tpoint[0])/(p1[1]-tpoint[1]))
                    else: 
                        tp_gradient = 0
                    
                    if(gradient == tp_gradient):
                        res = True
                        break
            p1 = p2    
    else :    
        if counter % 2 == 0:
            res = False  # point is outside
        else:
            res = True  # point is inside
    """           
    if (counter % 2 == 0):
        res = False  # point is outside
    else:
        res = True  # point is inside
    """       
    return res


# 폴리콘이 폴리곤 안에 겹치는지 화인한다. 겹치는 부분이 있으면 True 아니면 False를 리턴한다
# 모든 입력 포인트는 numpy로 입력 받는다.
def PolygonOverlab(spolygon, tpolygon) :
    
    bresult = False
    
    for point in spolygon :
        
        bresult = PointInPolygon(point, tpolygon)
        
        if (bresult):
            break
    
    #print(f'PolygonOverlab:{bresult}')    
    return bresult

# box 좌표를 polygon 형태로 만든다.
def box2polygon( box):
    box_x = box[:,0]
    box_y = box[:,1]
    
    min_x = np.min(box_x,axis=0)
    min_y = np.min(box_y,axis=0)
    max_x = np.max(box_x,axis=0)
    max_y = np.max(box_y,axis=0)
    
    polygon = np.array([[min_x,min_y],[max_x,min_y],[max_x,max_y],[min_x,max_y]])
    
    return polygon

import math
# 두점을 잇는 직선에서 임의의 점 px, py까지의 거리를 구한다.
def dist4linepoint(lx1,ly1,lx2,ly2,px,py):
    #두점을 지나는 직선의 방정식 (ax+by+c=0)
    a = (ly2-ly1)
    b = -(lx2-lx1)
    c = lx2*ly1-lx1*ly2
    
    #점과 직선사이의 거리
    
    if(a==0 and b==0):
        print(f'lx1:{lx1},ly1:{ly1},lx2:{lx2},ly2:{ly2},px:{px},py:{py}')
        return 99999
    
    dist = (a*px+b*py+c)/math.sqrt(a*a+b*b)
    
    return dist

# json box position에서 중심좌표를 구한다.
def getCenterPointFromBox(labelData):
    #json파일의 point가 2포인트과 4포인트가 있어 구분해야 한다.
    json_point_length = len(labelData[1]) 
        
    min_x=0 
    min_y=0
    max_x=0 
    max_y=0
    
    cx=0
    cy=0
    
    #print(f'json_point_length:{json_point_length}')
    if(json_point_length>2):
        sortx_boxpos = sorted(labelData[1], key=lambda x:(x[0]))
        sorty_boxpos = sorted(labelData[1], key=lambda x:(x[1]))
        #print(f'sortx_boxpos:{sortx_boxpos}')
        #print(f'sorty_boxpos:{sorty_boxpos}')
        min_x = sortx_boxpos[0][0]
        min_y = sorty_boxpos[0][1]
        max_x = sortx_boxpos[3][0]
        max_y = sorty_boxpos[3][1]
        
        #print(f'labelData center x:{(min_x+max_x)/2} y:{(min_y+max_y)/2}')
        cx = (min_x+max_x)/2
        cy = (min_y+max_y)/2
    else:
        min_x = labelData[1][0][0]
        min_y = labelData[1][0][1]
        max_x = labelData[1][1][0]
        max_y = labelData[1][1][1]
       
        cx = (min_x+max_x)/2
        cy = (min_y+max_y)/2
        #print(f'labelData center x:{(min_x+max_x)/2} y:{(min_y+max_y)/2}')
        
    return cx,cy

# NpEncoder class ================================================================
class NpEncoder(json.JSONEncoder):
   def default(self, obj):
       if isinstance(obj, np.integer):
           return int(obj)
       elif isinstance(obj, np.floating):
           return float(obj)
       elif isinstance(obj, np.ndarray):
           return obj.tolist()
       else:
           return super(NpEncoder, self).default(obj)
#==============================================================================

# json imagepath를 수정하고 이전 파일명을 기록한다.
def changeJsonImagepath(prevPath, prevFilename, newPath, newFileName):

    image_basefilename, ext = os.path.splitext(prevFilename)
    
    json_path = prevPath + r'\\'+ image_basefilename + r".json"   
    
    with open(json_path, 'r', encoding='UTF-8') as f:
        json_data = json.load(f) 
        f.close()
        
        json_data['imageData'] = None
        json_data['imagePath'] = newFileName + ext
        json_data['previousName'] = prevFilename
        
        #json 파일로 저장한다.
        new_json_path = newPath + r'\\' + newFileName + r".json"   
        #print(f'new_json_path:{new_json_path}')
        try:
            with open(new_json_path,'w', encoding='utf-8') as fw:
                json.dump(json_data,fw,ensure_ascii=False,indent="\t", cls=NpEncoder)
                    
        except IOError:
            print("Error: json write error")
        

label_text_name = os.path.join(ROOT_DIR, r'LPR_Total_Labels.txt')   

from enum import Enum

class ENUM_PLATE_DATA(Enum):
    POS = 0
    TYPE = 1
    REGION = 2
    CHAR = 3
    NUMLIST = 4
    
class ENUM_LABEL_DATA(Enum):
    TYPE = 0
    PLATE = 1
    NUM = 2
    CHAR = 3

def diplomacy_hr2or(SOURCE, DEST):   
    print('program start')
    file_count = len(os.listdir(SOURCE))
    print('처리할 총 파일 갯수는 {} 입니다.'.format(file_count))
    
    if not os.path.isdir(DEST):
        os.makedirs(DEST) 
        
    #json file list를 확인한다.
    file_list = [file for file in os.listdir(SOURCE) if file.endswith((".json"))]    
    
    for filename in file_list:
        try:    
            json_path = SOURCE + r'\\' + filename
            basefilename, ext = os.path.splitext(filename)
            
            plate_info_list = []
            with open(json_path, 'r', encoding='UTF-8') as f:
                json_data = json.load(f)
                
                image_filename = json_data['imagePath']
                f.close()
                
            for item, shape in enumerate(json_data['shapes']):
                label = shape['label']              
                
                if(label == "oDiplomacy"):
                    print(f'파일명:{filename}') 
                    #print(f"item:{item}")
                    #print(f"hDiplomacy:{json_data['shapes'][item]['label']}")
                    json_data['shapes'][item]['label'] = 'OpDiplomacy'
                    
                    #json file만 복사
                    src_path = json_path
                    dst_path = DEST + r"\\" + filename
                    #copyfile(src_path, dst_path);
                    with open(dst_path,'w', encoding='utf-8') as fw:
                        json.dump(json_data,fw,ensure_ascii=False,indent="\t", cls=NpEncoder)  

                    #image 파일복사
                    image_basefilename, ext = os.path.splitext(image_filename)
                    src_path = SOURCE + r"\\" + image_filename
                    dst_path = DEST + f"\\" + image_filename
                    copyfile(src_path, dst_path);    
                    
        except IOError:
            print("Error: json write error")
        
def rename_plate_num(SOURCE, DEST, start):
    print('program start')
    file_count = len(os.listdir(SOURCE))
    print('처리할 총 파일 갯수는 {} 입니다.'.format(file_count))

    if  not os.path.exists(label_text_name):
        print("Label file {0} isn't exists".format(label_text_name))
        sys.exit()
        
    if not os.path.isdir(DEST):
        os.makedirs(DEST)    
    
    ConvMap = {}
    fLabels = pd.read_csv(label_text_name, header = None )
    for i, value in enumerate(fLabels[0]):
        ConvMap[value] = fLabels[1][i]
        #print(f'ConvMap[{value}]:{ConvMap[value]}')

    #json file list를 확인한다.
    file_list = [file for file in os.listdir(SOURCE) if file.endswith((".json"))]
 
    #차량번호판 정보 집합
    plate_info = {r'cartype':"", r'region':"", r'char':"", r'num':"", r'cml':False}    
    
    #전체차량번호
    all_plate_name_list = []
    
    #이륜차 숫자 최소X좌표값
    mplate_num_min_x_pos = 0  
    
    #file_count
    #폴더별로 하는경우 시작값 적용
    new_file_index = 10000 * (start -1)
    
    for filename in file_list:
        try:    
            json_path = SOURCE + r'\\' + filename
            basefilename, ext = os.path.splitext(filename)
            print(f'파일명:{filename}') 
            plate_info_list = []
            with open(json_path, 'r', encoding='UTF-8') as f:
                json_data = json.load(f)  
                
                #차종 위치 리스트  
                label_cartype_list = []
                label_cartype = ['',[[0,0],[0,0],[0,0],[0,0]]]
                #번호판 위치 리스트
                label_platetype_list = []   
                label_platetype = ['',[[0,0],[0,0],[0,0],[0,0]]]
                #숫자리스트
                label_num_list = [] 
                label_num = ['',[[0,0],[0,0],[0,0],[0,0]]]               
                #지역리스트
                label_region_list = [] 
                label_region = ['',[[0,0],[0,0],[0,0],[0,0]]]
                #문자리스트
                label_char_list = [] 
                label_char = ['',[[0,0],[0,0],[0,0],[0,0]]]
                                
                num_count=0
                char_count=0
                
                image_filename = json_data['imagePath']
                
                for item, shape in enumerate(json_data['shapes']):
                    
                    label = shape['label']              
                    points = np.array(shape['points']).astype(int)
                    
                    #print(f'label:{label}')
                    #print(f'points:{points}')
                    
                    try:
                        label_string = ConvMap[label]
                    except KeyError:                            
                        if not os.path.isdir(DEST + r"\\ERROR2\\"):
                            os.makedirs(DEST + r"\\ERROR2\\")    
                        print(f'error filename:{filename} json label:{label}')
                        src_path = json_path
                        dst_path = DEST + r"\\ERROR2\\" + basefilename + r".json" 
                        copyfile(src_path, dst_path);
                        #image 파일복사
                        image_basefilename, ext = os.path.splitext(image_filename)
                        src_path = SOURCE + r"\\" + image_filename
                        dst_path = DEST + r"\\ERROR2\\" + image_basefilename + ext           
                        copyfile(src_path, dst_path);
                        break
                    
                    if(label.find('type') >= 0) or (label == 'plate'):
                        #번호판 영역을 저장한다.
                        #if(label=='type13'):
                        #    label_platetype[0] = 'mplate'
                        #else:
                        #    label_platetype[0] = ConvMap[label]
                        
                        if(label=='plate'):
                            label_platetype[0] = r'T00'
                        else:
                            label_platetype[0] = r'T'+f'{int(label[4:])}'.zfill(2)
                        
                        #print(r'label_platetype:{label_platetype[0]}')
                        label_platetype[1] = points
                        label_platetype_list.append(list(label_platetype))
                        #print(r'add plate type')
                        #print(r'label_platetype_list:{label_platetype_list}')
                        
                        
                    elif (label == "car") or (label == "truck") or (label == "bus") or (label == "motorcycle") or (label == "bicycle") or (label == "human") or (label == "helmet") or (label == "kickboard"): 
                        label_cartype[0] = ConvMap[label]
                        label_cartype[1] = points
                        label_cartype_list.append(list(label_cartype))
                        #print(r'add car type')
                    elif (label == "n0") or (label == "n1") or (label == "n2") or (label == "n3") or (label == "n4") or (label == "n5") or (label == "n6") or (label == "n7") or (label == "n8") or (label == "n9"): 
                        #plate_info['num'] += ConvMap[label]
                        #print(f'points:{points}')
                        label_num[0] = ConvMap[label]
                        label_num[1] = points
                        label_num_list.append(list(label_num))
                        
                        #point_length = len(label_num[1])
                        #if(point_length<=2):
                        #    print(f'point_length:{filename}')
                        
                        '''
                        plate_number[0] = ConvMap[label]
                        plate_number[1] = points
                        #plate_number_pos[num_count] = points
                        #plate_number[num_count+1][0] = int(plate_info['num'])
                        #plate_number[num_count+1][1] = points
                        #plate_number[num_count+1][2] = int(plate_info['num'])
                        #plate_number[num_count+1][3] = int(plate_info['num'])
                        #plate_number[num_count+1][4] = int(plate_info['num'])
                        
                        #print(f'num:{plate_number[0]}:{plate_number[1]}')
                        plate_number_lists.append(list(plate_number))
                        #print(f'plate_number_lists:{plate_number_lists}')
                        '''
                        num_count+=1
                    elif label.find(r'v') == 0:
                        #VRegion
                        #plate_info['region'] = ConvMap[label]
                        label_region[0] = ConvMap[label]
                        label_region[1] = points
                        label_region_list.append(list(label_region))
                    elif label.find(r'h') == 0:
                        #HRegion
                        #plate_info['region'] = ConvMap[label] 
                        label_region[0] = ConvMap[label]
                        label_region[1] = points
                        label_region_list.append(list(label_region))
                    elif label.find(r'Op') == 0:
                        #HRegion
                        #plate_info['region'] = ConvMap[label]    
                        label_region[0] = ConvMap[label]
                        label_region[1] = points
                        label_region_list.append(list(label_region))
                    elif label.rfind(r'6') == (len(label)-1):
                        #6Region
                        #plate_info['region'] = ConvMap[label]  
                        label_region[0] = ConvMap[label]
                        label_region[1] = points
                        label_region_list.append(list(label_region))                           
                    else:
                        #plate_info['char'] += ConvMap[label]
                        #char_count+=1
                        label_char[0] = ConvMap[label]
                        label_char[1] = points
                        label_char_list.append(list(label_char))       
                        
                f.close()
                
                
                if(debug_flag==True):
                    #차종
                    for cartype in label_cartype_list:
                        print(f'차종:{cartype[0]} points:{cartype[1]}')
                    #번호판 위치 리스트
                    for platetype in label_platetype_list:
                        print(f'번호판:{platetype[0]} points:{platetype[1]}')
                    #숫자리스트
                    for num in label_num_list:
                        print(f'숫자:{num[0]} points:{num[1]}')  
                    #지역리스트
                    for region in label_region_list:
                        print(f'지역:{region[0]} points:{region[1]}')
                    #문자리스트
                    for char in label_char_list:
                        print(f'문자:{char[0]} points:{char[1]}')
                
                
                #그룹화
                for platetype in label_platetype_list:

                    plate_info['cartype'] = ""
                    plate_info['region'] = ""
                    plate_info['char'] = ""
                    plate_info['num1'] = ""
                    plate_info['num2'] = ""
                    plate_info['mregion'] = ""
                    plate_info['cml'] = False
                    plate_info['platetype'] = ""
                    
                    #번호판을 포함하면 cartype 결정
                    plate_polygon = box2polygon(platetype[1])
                    
                    if(debug_flag==True):
                        print(f'plate_polygon:{plate_polygon}')
                    
                    #번호판 영역을 포함하는 차량영역리스트
                    overlap_list = []
                    
                    for index, cartype in enumerate(label_cartype_list):
                        #번호판을 포함하면 cartype 결정
                        
                        #print(f'index:{index} cartype:{cartype[1]} platetype[1]:{platetype[1]}')
                        cartype_polygon = box2polygon(cartype[1])   
                        
                        #영역이 겹치면 차종을 저장한다.
                        if(PolygonOverlab(plate_polygon, cartype_polygon)==True):
                            #plate_info['cartype'] = cartype[0]
                            
                            center_plate = plate_polygon[0][0]+(plate_polygon[1][0]-plate_polygon[0][0])/2
                            center_cartype = cartype_polygon[0][0]+(cartype_polygon[1][0]-cartype_polygon[0][0])/2
                            center_distance = abs(center_plate - center_cartype)
                            overlap_list.append([index, center_distance])
                            
                            #print(f'plate_polygon[0]:{plate_polygon[0]}')
                            #print(f'plate_polygon[1][0]:{plate_polygon[1][0]}')
                            #print(f'plate_polygon[0][0]:{plate_polygon[0][0]}')
                            #print(f'cartype_polygon[0]:{cartype_polygon[0]}')
                            #print(f'cartype_polygon[1]:{cartype_polygon[1]}')
                            
                            #print(f'center plate:{plate_polygon[0][0]+(plate_polygon[1][0]-plate_polygon[0][0])/2}')
                            #print(f'center carplate:{cartype_polygon[0][0]+(cartype_polygon[1][0]-cartype_polygon[0][0])/2}')
                            
                            #print(plate_info['cartype'])
                            #print(f'cartype_polygon:{cartype_polygon}')
                            #print(f'plate_polygon:{plate_polygon}')
              
                    #중심위치가 가까운 번호판과 차량을 매칭한다.
                    if(len(overlap_list)>0):
                        sort_overlap_list = sorted(overlap_list, key=lambda x:(x[1]))
                        #print(f'sort_overlap_list[0]:{sort_overlap_list[0]}')
                        plate_info['cartype'] = label_cartype_list[index][0]
                    
                    #-----------------------------------------------------
                    overlap_list=[]
                    for index, region in enumerate(label_region_list):
                       #지역 포함하면 결정
                       #숫자보다 지역을 먼저 결정하는 이유는 지역에 '외교'가 들어가면 twodigit가 3자리가 된다.
                       
                       region_polygon = box2polygon(region[1])   
                       
                       #영역이 겹치면 지역명과 위치를  저장한다.
                       if(PolygonOverlab(region_polygon, plate_polygon)==True):
                           plate_info['region'] = region[0]      
                    
                    #for char in label_char_list: 
                    #print(f"plate_info['region']:{plate_info['region']}")   
                    
                    #-----------------------------------------------------
                    overlap_list=[]
                    four_digit = ""
                    two_digit = ""
                    all_digit = ""
                    flag_check_y = False
                    flag_digit_continuous = True
                    
                    if(len(label_num_list)>0):
                        
                        for index, num in enumerate(label_num_list):
                           #숫자를 포함하면 결정
                           #print(f'index:{index} num:{num[0]} platetype[1]:{platetype[1]}')
                           num_polygon = box2polygon(num[1])   
                           
                           #영역이 겹치면 번호와 위치를  저장한다.
                           if(PolygonOverlab(num_polygon, plate_polygon)==True):
                               overlap_list.append(num)
                                   
                        xsort_overlap_list = sorted(overlap_list, key=lambda x:(x[1][0][0]))
                        ysort_overlap_list = sorted(overlap_list, key=lambda x:(x[1][0][1]))
                        
                        if(debug_flag==True):
                            print(f'xsort_overlap_list:{xsort_overlap_list}')
                            #print(f'ysort_overlap_list:{ysort_overlap_list}')
                        
                        if(len(xsort_overlap_list)>0):
                            plate_start_num = xsort_overlap_list[0]    #최좌측시작숫자
                            plate_end_num = xsort_overlap_list[-1]   #최우측종료숫자
                            
                            if(platetype[0] == 'T13'):
                                mplate_num_min_xpoint = sorted(plate_start_num[1], key=lambda x:(x[0]))
                                mplate_num_min_x_pos = mplate_num_min_xpoint[0][0]
                            
                            #좌측과 우측의 숫자영역의 센터의 두점을 연결하여 직선의 방정식을 구한다.
                            #print(f'plate_start_num:{plate_start_num[0]} pos:{plate_start_num[1]}')
                            #print(f'plate_end_num:{plate_end_num[0]} pos:{plate_end_num[1]}')
                            
                            #번호판 최좌측 숫자의 중점을 구한다. (4점 또는 2점 사각형인 경우)
                            #json_point_length = len(plate_start_num[1]) # json파일의 point가 2포인트과 4포인트가 있어 구분해야 한다.
                            
                            plate_start_num_cx, plate_start_num_cy = getCenterPointFromBox(plate_start_num)
                            plate_end_num_cx, plate_end_num_cy = getCenterPointFromBox(plate_end_num)
                            
                            if(len(xsort_overlap_list)>1):
                                
                                prev_start = 0;
                                prev_end = 0;
                                
                                #전체 숫자에서 중심점과 (시작숫자와 끝숫자를 이은 직선까지의 거리를 구한다)
                                for index, num in enumerate(xsort_overlap_list):  
                                    
                                    num_cx, num_cy = getCenterPointFromBox(num)
                                    distance = dist4linepoint(plate_start_num_cx,plate_start_num_cy,plate_end_num_cx,plate_end_num_cy,num_cx,num_cy)
                                    
                                    if(distance == 99999):
                                        print(f'num1:{num}')
                                        print(f'error position! filename:{filename}')
                                        
                                    sort_num_y_pos = sorted(list(num[1]), key=lambda x:(x[1]))
                                    #print(f'num:{num[0]} sort_num_pos:{sort_num_y_pos}')
                                    point_length = len(num[1]) # json파일의 point가 2포인트과 4포인트가 있어 구분해야 한다.
                                    if(point_length>2):
                                        half_height = abs(sort_num_y_pos[0][1] - sort_num_y_pos[3][1])/2
                                    else:    
                                        half_height = abs(sort_num_y_pos[0][1] - sort_num_y_pos[1][1])/2
                                    #print(f'half_height:{half_height} distance:{distance}')
                                    if(half_height < distance):
                                        flag_check_y = True
                                        #임시번호판은 두줄없음
                                        two_digit += num[0]
                                    else:
                                        four_digit += num[0]
                                        all_digit += num[0]
                                    
                                    sort_num_x_pos = sorted(list(num[1]), key=lambda x:(x[0]))
                    
                                    if(index > 0):
                                        #전체 숫자 폴리곤의 간격이 3이하이면 임시번호판이다
                                       
                                        if(point_length>2):
                                            cur_start = sort_num_x_pos[0][0]
                                            cur_end = sort_num_x_pos[3][0]
                                            #print(f'cur_start:{cur_start} cur_end:{cur_end}')
                                        else:    
                                            cur_start = sort_num_x_pos[0][0]
                                            cur_end = sort_num_x_pos[1][0]
                                            #print(f'cur_start:{cur_start} cur_end:{cur_end}')
                                        
                                        between_dist = abs(cur_start - prev_end)
                                        
                                        #print(f'between_dist:{between_dist}')
                                        #print(f'prev_start:{prev_start} prev_end:{prev_end}')
                                        if (between_dist > 4):					
                                            #숫자의 거리가 4를 초과하면 임시번호판 아님
                                            flag_digit_continuous = False
                                        
                                        prev_start = cur_start
                                        prev_end = cur_end
  										  
                                    else:
                                        if(point_length>2):
                                            prev_start = sort_num_x_pos[0][0]
                                            prev_end = sort_num_x_pos[3][0]
                                        else:    
                                            prev_start = sort_num_x_pos[0][0]
                                            prev_end = sort_num_x_pos[1][0]     
                                            
                            else:
                                #숫자가 1개만 존재하는 경우
                                num = xsort_overlap_list[0]
                                four_digit = num[0]
                                all_digit = num[0]
                            
                                
                            if(flag_check_y==False):            
                                num_length = len(all_digit)
                                four_digit = all_digit[-4:]
                                #print(f'all_digit:{all_digit}')
                                #print(f'four_digit:{four_digit}')
                        
                                if(num_length>4):
                                    #외교번호판은 XXX-XXX 3자리로 two_digit와 four_digit가 3자리이다.
                                    if(plate_info['region']=="외교"):
                                        #print(f"plate_info['region']:{plate_info['region']}")
                                        two_digit = all_digit[0:num_length-3]
                                        four_digit = all_digit[-3:]
                                    else:
                                        two_digit = all_digit[0:num_length-4]
                                    
                                        
                        
                        else: #if(len(xsort_overlap_list)>0):
                            #번호판 영역이 없거나 번호판 영역안에 들어가는 숫자가 없는 경우로 에러파일이다.
                            if(debug_flag==True):
                                print(f'plate has no number! {filename}')
                       
                        plate_info['num1'] = two_digit
                        plate_info['num2'] = four_digit
                        
                        
                        #번호판 타입 정보
                        plate_info['platetype'] = platetype[0]
                        
                        if(debug_flag==True):
                            print(f'two_digit:{two_digit}')
                            print(f'four_digit:{four_digit}')
                            
                    
                     
                    #-----------------------------------------------------
                    overlap_list=[]
                    for index, char in enumerate(label_char_list):
                        #문자를 포함하면 결정
                        
                        char_polygon = box2polygon(char[1])   
                        
                        #영역이 겹치면 문자 위치를  저장한다.
                        if(PolygonOverlab(char_polygon, plate_polygon)==True):
                            overlap_list.append(char) 
                        
                        
                    #이륜차 번호판의 경우 char가 3개이상으로 지자체명을 따로 분리한다.
                    #숫자의 x좌표보다 작은 값의 문자는 이륜차 문자로 처리하고 그외 문자는 지자체명으로 한다.
                    if(platetype[0] == 'T13'):
                        char_length = len(overlap_list)  
                        xsort_overlap_list = sorted(overlap_list, key=lambda x:(x[1][0][0]))
                        
                        if(char_length > 0):
                            for index, char_data in enumerate(xsort_overlap_list):
                                sort_char_x_point = sorted(char_data[1], key=lambda x:(x[0]))
                                #문자의 x위치가 이륜차 숫자의 최소 x위치보다 작으면 이륜차 문자확정
                                if(sort_char_x_point[0][0] < mplate_num_min_x_pos):
                                    plate_info['char']=char_data[0]
                                else:
                                    plate_info['mregion']+=char_data[0]
                                
                            #plate_info['char'] = overlap_list[0]
                            #plate_info['char'] = mplate_char
                    else:    
                        #CML의 경우 char가 2개임
                        char_length = len(overlap_list)
                        
                        if(char_length > 0):
                            if(char_length>1):
                                for char_data in overlap_list:
                                    if char_data[0] == '○':
                                        plate_info['cml'] = True
                                        continue
                                    else:
                                        plate_info['char'] = char_data[0]
                            else:
                                plate_info['char'] = overlap_list[0][0]
                    
                    
                    #-----------------------------------------------------   
                    plate_info_list.append(dict(plate_info))   
                    
                
                #최종 차량번호(2개이상)
                make_plate_list = [] 
                for index, data in enumerate(plate_info_list): 
                    #print(f'index:{index}')
                    if(data['cml']==True):
                        make_plate_list.append((data['platetype'] + r'_' + data['region'] + data['num1'] + data['char'] +  data['num2'] + r"영"))
                        #print(f"index:{index} 차종:{data['cartype']}:{data['region']}{data['num1']}{data['char']}{data['num2']}영")
                    else:
                        #print(f"index:{index} 차종:{data['cartype']} region:{data['region']} num1:{data['num1']} char:{data['char']} num2:{data['num2']}")
                        if(flag_digit_continuous == True) and (data['region']=="") and (data['char']==""):
                            make_plate_list.append(data['platetype'] + r'_' + r"임" + (data['region'] + data['num1'] + data['char'] +  data['num2']))
                            #print(f"index:{index} 차종:{data['cartype']}:{data['region']}{data['num1']}{data['char']}{data['num2']}")
                        else:
                            make_plate_list.append((data['platetype'] + r'_' + data['region'] + data['num1'] + data['mregion'] + data['char'] +  data['num2']))
                            #print(f"index:{index} 차종:{data['cartype']}:{data['region']}{data['num1']}{data['char']}{data['num2']}")
               
                create_new_file = True
                final_plate_num = ""
                final_plate_count = 0
                
                #기존 파일명의 차량번호 조회 (읽어들인 차량번호을 검색해서 한개라도 없는 경우 기존 파일명이 틀림)
                for index, data in enumerate(make_plate_list): 
               
                    if(len(data)>0 and filename.find(data)<0):
                        create_new_file = True
                        
                    if(len(data)<=0):
                        continue
                    
                    if(index>0):
                        final_plate_num += r"_"
                        
                    #final_plate_num += r'(' + data + r')'
                    final_plate_num += data
                    
                    final_plate_count +=1
                    
                '''    
                #basefilename의 변경작업
                new_basefilename = basefilename
                #파일명이 추가된 번호판 정보로 길어지므로 이를 수정한다.
                #_FAIL_,_0_,_1_,_2_ 기준으로 확장자부터 검색위치까지 잘라낸다.
                trim_pos = basefilename.rfind('_FAIL_')
                if(trim_pos>=0):
                    #print(basefilename[0:trim_pos+5])
                    new_basefilename = basefilename[0:trim_pos+5]
                else:
                    trim_pos = basefilename.rfind('_0_')
                    if(trim_pos>=0):
                        #print(basefilename[0:trim_pos+2])
                        new_basefilename = basefilename[0:trim_pos+2]
                    else:
                        trim_pos = basefilename.rfind('_1_')
                        if(trim_pos>=0):
                            #print(basefilename[0:trim_pos+2])
                            new_basefilename = basefilename[0:trim_pos+2]
                        else:
                            trim_pos = basefilename.rfind('_2_')
                            if(trim_pos>=0):
                                #print(basefilename[0:trim_pos+2])
                                new_basefilename = basefilename[0:trim_pos+2]
                            else:
                                new_basefilename = basefilename
                                
                basefilename = new_basefilename + f'_{new_file_index:03d}'   
                
                image_basefilename, ext = os.path.splitext(image_filename)
                image_basefilename = new_basefilename + f'_{new_file_index:03d}'
                '''
                new_file_index+=1
                
                #파일명에 차량번호가 없는 경우(번호판 영역이 없거나 번호판이 없는 경우)
                #if(len(final_plate_num)<=0):
                if(False):    
                    
                    if not os.path.isdir(DEST + r"\\ERROR\\"):
                        os.makedirs(DEST + r"\\ERROR\\")
                        
                    #LABEL 딕셔너리의 키값이 틀린경우 중복복사가 될수 있다. 단 덮어쓰기가 되므로 파일이 2개 생성되지 않으므로 문제없음
                    #중복작업을 제거해야 할 필요는 있음.
                    print(f'error filename:{filename} final_plate_num:{final_plate_num}')
                    src_path = json_path
                    dst_path = DEST + r"\\ERROR\\" + basefilename + r".json" 
                    copyfile(src_path, dst_path);
                    #image 파일복사
                    src_path = SOURCE + r"\\" + image_filename
                    dst_path = DEST + r"\\ERROR\\" + image_basefilename + ext           
                    copyfile(src_path, dst_path);
                else:
                    if(create_new_file==True):
                        #데이터셋 파일명 규칙
                        #생성연월일_연번_구분기호_번호판갯수_번호판타입1-차량번호1_번호판타입2-차량번호2                       
                        #생성년월일: 
                        #연번: 00000001 ~ 99999999
                        #구분기호: E0:도로공사, P0:경찰청, A0:구분없음 
                        #차로정보: L00:구분없음, L11:1차로
                        #번호판갯수: N01(1개)
                        #번호판정보: T13-경기안양카1234(타입13,경기안양카1234), T03-52조1100(타입3,52조1100)
                        #EX) 20240306_00000001_A0_L00_N02_T13-경기안양카1234_T03-52조1100.jpg, 
                        #    20240306_00000001_A0_L00_N02_T13-경기안양카1234_T03-52조1100.json
                        now = time.localtime()
                        new_tag = f'{now.tm_year}' + f'{now.tm_mon}'.zfill(2) + f'{now.tm_mday}'.zfill(2) + '_' + f'{new_file_index}'.zfill(8)
                        #+"-"+ f'{now.tm_hour}'.zfill(2) + f'{now.tm_min}'.zfill(2) + f'{now.tm_sec}'.zfill(2)  
                       
                        #same_count = all_plate_name_list.count(final_plate_num)
                        all_plate_name_list.append(final_plate_num)
                        new_tag += r"_E0_L00_N" + f'{final_plate_count}'.zfill(2) + r"_"
                        
                        #json 파일복사
                        src_path = json_path
                        new_plate_name = new_tag + final_plate_num 
                        #dst_path = DEST + r"\\" + basefilename + "_" + new_plate_name + r".json"
                        dst_path = DEST + r"\\" + new_plate_name + r".json"
                        copyfile(src_path, dst_path);
                        #image 파일복사
                        image_basefilename, ext = os.path.splitext(image_filename)
                        src_path = SOURCE + r"\\" + image_filename
                        #dst_path = DEST + r"\\" + image_basefilename + "_" + new_plate_name + ext
                        dst_path = DEST + r"\\" + new_plate_name + ext
                        copyfile(src_path, dst_path);
                        
                        #json imagepath 변경
                        changeJsonImagepath(SOURCE, image_filename, DEST, new_plate_name)
                        
                        #if not os.path.isdir(DEST + r"\\WRONG\\"):
                        #    os.makedirs(DEST + r"\\WRONG\\")
                            
                        #json 파일복사
                        #src_path = json_path
                        #dst_path = DEST + r"\\WRONG\\" + basefilename + "_" + final_plate_num + r".json"
                        #copyfile(src_path, dst_path);
                        #image 파일복사
                        
                        #src_path = SOURCE + r"\\" + image_filename
                        #dst_path = DEST + r"\\WRONG\\" + image_basefilename + "_" + final_plate_num + ext
                        #copyfile(src_path, dst_path);    
                            
                        #json imagepath 변경
                        #changeJsonImagepath(SOURCE, image_filename, DEST + r"\\WRONG", basefilename + "_" + final_plate_num)
                    else:
                        if(debug_flag==True):
                            print(f'exist new file:{final_plate_num}')
                        
                        if not os.path.isdir(DEST + r"\\CORRECT\\"):
                            os.makedirs(DEST + r"\\CORRECT\\")
                            
                        #json 파일복사
                        src_path = json_path
                        dst_path = DEST + r"\\CORRECT\\" + basefilename + "_" + final_plate_num + r".json"
                        copyfile(src_path, dst_path);
                        #image 파일복사
                        src_path = SOURCE + r"\\" + image_filename
                        dst_path = DEST + r"\\CORRECT\\" + image_basefilename + "_" + final_plate_num + ext
                        copyfile(src_path, dst_path);    
                            
                        #json imagepath 변경
                        changeJsonImagepath(SOURCE, image_filename, DEST + r"\\CORRECT", basefilename + "_" + final_plate_num)    
                   
        except Exception as e:
            print(e)
           
            
def findfile(name, path):
    for dirpath, dirname, filename in os.walk(path):
        if name in filename:
            return os.path.join(dirpath, name)  
           
def move_same_plate(SOURCE, DEST):
     
    print('program start')
    file_count = len(os.listdir(SOURCE))
    print('처리할 총 파일 갯수는 {} 입니다.'.format(file_count))

    if not os.path.isdir(DEST):
        os.makedirs(DEST)    
 
    #json file list를 확인한다.
    file_list = [file for file in os.listdir(SOURCE) if file.endswith((".json"))]
    
    same_count=0
    for filename in file_list:
        try:    
            #basefilename, ext = os.path.splitext(filename)
            #print(f'DEST:{DEST} filename:{filename}')
            if(findfile(filename,DEST)==None):
                continue
            else:
                same_count+=1
                print(f'No:{same_count} 중복 파일명:{filename}')
                
        except IOError as e:
            print(e)
            continue            
        
        
def seperate_in_folder(SOURCE, DEST, sep_count=5000):
     
    print('program start')
    file_count = len(os.listdir(SOURCE))
    print('처리할 총 파일 갯수는 {} 입니다.'.format(file_count))

    if not os.path.isdir(DEST):
        os.makedirs(DEST)    
 
    #json file list를 확인한다.
    file_list = [file for file in os.listdir(SOURCE) if file.endswith((".json"))]
    
    copy_count=0
    for filename in file_list:
        try:    
            with open(SOURCE + r"\\" + filename, 'r', encoding='UTF-8') as f:
                json_data = json.load(f)  
                image_filename = json_data['imagePath']
                f.close()
            
    
                seperate_folder = f"{int(copy_count/sep_count + 1)}"
                
                if not os.path.isdir(DEST + f"\\{seperate_folder}"):
                    os.makedirs(DEST + f"\\{seperate_folder}\\")
                
                basefilename, ext = os.path.splitext(filename)
                #json 파일복사
                src_path = SOURCE + r"\\" + filename
                dst_path = DEST + f"\\{seperate_folder}\\" + filename
                copyfile(src_path, dst_path);
                #image 파일복사
                image_basefilename, ext = os.path.splitext(image_filename)
                src_path = SOURCE + r"\\" + image_filename
                dst_path = DEST + f"\\{seperate_folder}\\" + image_filename
                copyfile(src_path, dst_path);    
                
                copy_count+=1
        except IOError as e:
            print(e)
            continue                    


#JPG 파일의 유효성을 검사한다.
from PIL import Image

#IMAGE_PATH = 'D:\\01_SRC\\labelconv\\dataset\\labels\\01_원본_34342_86_93차'
IMAGE_PATH = 'D:\\01_SRC\\RealTimeObjectDetection-main\\Tensorflow\\workspace\\images\\car-plate\\train'
IMG_FORMATS = ['bmp', 'jpg', 'jpeg', 'png', 'tif', 'tiff', 'dng', 'webp', 'mpo'] 
def verify_jpg(src):

    #테스트할 이미지 디렉토리
    file_count = len(os.listdir(src))
    print('처리할 총 파일 갯수는 {} 입니다.'.format(file_count))

    count = 0                  
    file_list = [file for file in os.listdir(src) if file.endswith((".jpg"))]

    for index, filename in enumerate(file_list):

        # verify images 
        try:
            #print(IMAGE_PATH + r'\\' + filename)
            im = Image.open(src + r'\\' + filename)
            
            #print('load success')
            im.verify() #I perform also verify, don't know if he sees other types o defects
            im.close() #reload is necessary in my case
            im = Image.open(src + r'\\' + filename) 
            im.transpose(Image.FLIP_LEFT_RIGHT)
            im.close()
        except IOError: 
            #manage excetions here
            print(f'{index}' + r':' + filename)
             


#파일이동을 하는 폴더
MOVES_DIR = 'MOVES'
#파일복사를 하는 폴더
COPIES_DIR = 'COPIES'
#파일 매칭이 실패한 파일의 결과폴더
MISMATCHING_DIR = 'MISMATCHING'

#경로 하드코딩(파일복사가 더 시간걸림...)

SOURCES_DIR = r'F:\추가'
DESTS_DIR = r'F:\추가_rename'
    
#print('jpg 파일 검증')
#verify_jpg(SOURCES_DIR)
#print('jpg 파일 검증 완료')
#시작폴더번호와 종료폴더번호+1을 인자로 넘겨 전체 1만장 단위로 분리된 원본이미지의 파일명을 변환한다.
for i in range(1, 2):    
    rename_plate_num(SOURCES_DIR,DESTS_DIR, i)
    
    