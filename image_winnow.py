import os, shutil
import sys
from pathlib import Path, PureWindowsPath
from pathlib import Path
from label_tools import *
from JsonMng import *

#이 파일은 yolo 모델로 영상을 detection한 후 필요한 파일과 아닌 파일을 분리 하기 위함이다.
#------------------------------------------------
#라벨링 대상이 되는 이미지 파일의 위치
src_image_dir = r'D:\SPB_Data\labelconv\dataset\yolo\images'
src_image_dir = os.path.normpath(src_image_dir)
src_image_dir = Path(src_image_dir)


#yolo detection이 있는 테스트 파일.
ref_image_dir = r'D:\SPB_Data\yolov8\runs\detect\predict'
ref_image_dir = os.path.normpath(ref_image_dir)
ref_image_dir = Path(ref_image_dir)

#yolo label 파일이 있는 txt 파일 있늘 곳.
label_dir = r'D:\SPB_Data\yolov8\runs\detect\predict\labels'
label_dir = os.path.normpath(label_dir)
label_dir = Path(label_dir)

#yolo label 파일이 있는 txt 파일 있늘 곳.
filtered_dir = r'D:\SPB_Data\labelconv\dataset\yolo\filtered'
filtered_dir = os.path.normpath(filtered_dir)
filtered_dir = Path(filtered_dir)

ROOT_DIR = os.path.dirname(__file__)

DEFAULT_LABEL_FILE =  "./tracking_yolo_labels.txt"
#------------------------------------------------

if not os.path.exists(src_image_dir) :
    print("Error :folder exists. So make new folder : {}".format(src_image_dir))
    createFolder(src_image_dir)

if not os.path.exists(ref_image_dir) :
    print("Error :folder exists. So make new folder : {}".format(ref_image_dir))
    createFolder(ref_image_dir)

if not os.path.exists(label_dir) :
    print("Error :folder exists. So make new folder : {}".format(label_dir))
    createFolder(label_dir)

if os.path.exists(filtered_dir):
    shutil.rmtree(filtered_dir)

if not os.path.exists(filtered_dir) :
    print("Error :folder exists. So make new folder : {}".format(filtered_dir))
    createFolder(filtered_dir)


image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
src_image_list = [fn for fn in os.listdir(src_image_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]
imagefile_num = len(src_image_list)

print("총 처리할 이미지는 {} 입니다.".format(imagefile_num))

ref_image_list = [fn for fn in os.listdir(ref_image_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]

fLabels = pd.read_csv(DEFAULT_LABEL_FILE, header = None )
CLASS_NAMES = fLabels[0].values.tolist()
HUMAN_NAMES= dict(zip(CLASS_NAMES, fLabels[1].values.tolist()))

for filename in src_image_list:
    basename, ext = os.path.splitext(filename)
    readlines = None
    dw = None
    dh = None
    if filename in ref_image_list:
        #소스에 있는 이미지파일이 ref 리스트에도 있는지 확인한다.
        yolo_label_file = os.path.join(label_dir,basename + '.txt') # yolo label 파일 이름을 만든다.
        if os.path.exists(yolo_label_file): #label파일을 image가 있는 곳으로 복사한다.
            src = yolo_label_file
            dst = os.path.join(src_image_dir,yolo_label_file)
            json_dir = src_image_dir

            imgRGB  = imread(os.path.join(src_image_dir,filename))
            dh = imgRGB.shape[0]
            dw = imgRGB.shape[1]
            try:
                with open(os.path.join(label_dir,basename + '.txt'), 'r',encoding="UTF-8") as f:
                    readlines = f.readlines()
            except IOError:
                if not os.path.exists(os.path.join(json_dir,basename + '.txt')) :
                    print('Yolo label file not exit : {}'.format(basename + '.txt'))
                else:
                    print("Error: File does not appear to exist")
                continue

            jsonMng = JsonMng(json_dir,imgRGB.shape,filename)
            for dt in readlines:
        
                # Split string to float
                c, x, y, w, h = map(float, dt.split(' ')) # c : category

                box_sx = int((x - w / 2) * dw)
                box_ex = int((x + w / 2) * dw)
                box_sy = int((y - h / 2) * dh)
                box_ey = int((y + h / 2) * dh)
                
                obj_box = [[box_sx, box_ex, box_ex, box_sx],[box_sy,box_sy,box_ey,box_ey]]
                category = HUMAN_NAMES[int(c)]
                jsonMng.addObject(box=obj_box, label = category)
            
            jsonMng.save()
            #이미지 파일을 삭제한다.
            os.remove(os.path.join(ref_image_dir,filename ))
        else:
            #레이블이 없으므로 이미지를 filtered로 복사한다.
            src = os.path.join(ref_image_dir,filename)
            dst = os.path.join(filtered_dir,filename)
            shutil.move(src,dst)
    else: #필요 없는 이미지 파일 일 때...
        yolo_label_file = os.path.join(label_dir,basename + '.txt') # yolo label 파일 이름을 만든다.
        if os.path.exists(yolo_label_file): #label파일을 image가 있는 곳으로 복사한다.
            src = yolo_label_file
            dst = os.path.join(src_image_dir,yolo_label_file)
            json_dir = filtered_dir

            imgRGB  = imread(os.path.join(src_image_dir,filename))
            dh = imgRGB.shape[0]
            dw = imgRGB.shape[1]
            try:
                with open(os.path.join(label_dir,basename + '.txt'), 'r',encoding="UTF-8") as f:
                    readlines = f.readlines()
            except IOError:
                if not os.path.exists(os.path.join(json_dir,basename + '.txt')) :
                    print('Yolo label file not exit : {}'.format(basename + '.txt'))
                else:
                    print("Error: File does not appear to exist")
                continue

            jsonMng = JsonMng(json_dir,imgRGB.shape,filename)
            for dt in readlines:
        
                # Split string to float
                c, x, y, w, h = map(float, dt.split(' ')) # c : category

                box_sx = int((x - w / 2) * dw)
                box_ex = int((x + w / 2) * dw)
                box_sy = int((y - h / 2) * dh)
                box_ey = int((y + h / 2) * dh)
                
                obj_box = [[box_sx, box_ex, box_ex, box_sx],[box_sy,box_sy,box_ey,box_ey]]
                category = HUMAN_NAMES[int(c)]
                jsonMng.addObject(box=obj_box, label = category)
            
            jsonMng.save()
        src = os.path.join(src_image_dir,filename)
        dst = os.path.join(filtered_dir,filename)
        shutil.move(src,dst)

print("처리가 끝났습니다.")