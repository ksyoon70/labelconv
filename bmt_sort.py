#-*- encoding: utf8 -*-
"""
Created on Thu Jan  5 18:25:18 2023
BMT 영상을 순서대로 소팅하여 001.jpg 002.jpg 이런식으로 만든다.
@author: headway
"""

import os, shutil
import sys
from pathlib import Path, PureWindowsPath
#------------------------------------------------


ROOT_DIR = os.path.dirname(__file__)
MIDDLE_PATH = 'dataset'
DEST_ROOT_FOLDER_NAME = 'test'
IMAGE_FOLDER_NAME = 'images'
OUT_FOLDER_NAME = 'out'
DEFAULT_SRC_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,DEST_ROOT_FOLDER_NAME,IMAGE_FOLDER_NAME)
DEFAULT_DEST_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,DEST_ROOT_FOLDER_NAME,OUT_FOLDER_NAME)
DIV_COUNT = 20
#------------------------------------------------

src_dir = DEFAULT_SRC_IMAGES_PATH
dst_dir = DEFAULT_DEST_IMAGES_PATH
if not os.path.exists(src_dir) :
    print("Error : source images folder exists. check the folder :",src_dir)
    sys.exit(0)

if not os.path.exists(dst_dir) :
    print("Error : dest images folder exists. check the folder :",dst_dir)
    sys.exit(0)

totalcount = 0
proccnt = 0
count = 0

#총파일 갯수 파악
for path, dirs, files in os.walk(src_dir):
    totalcount += len(files)

print('총파일은 {}개 '.format(totalcount))
mod = int(totalcount / DIV_COUNT)  # 기본 5%마다 표시
    
for path, dirs, files in os.walk(src_dir):
    sfiles = sorted(files)
    for ix, file in enumerate(sfiles):
        basename, ext = os.path.splitext(file)
        new_name = "{0:03d}".format(ix+1) + ext
        count += 1
        if count % mod == 0 :
            proccnt += 1
            print('processing : {}%'.format(proccnt*100/DIV_COUNT))
        src = os.path.join(path,file)
        dst = os.path.join(dst_dir,new_name)
        try:
            shutil.copyfile(src, dst)
        except IOError:
                print("Error: File does not appear to exist")
                continue    

print('작업을 완료했습니다')