"""
Created on Tue Mar 15 14:19:22 2022
차량 영상 및 해당 json을 읽어 번호판 취득 후 번호판영상 및 labelme용 json으로 저장하는 기능 구현 
json과 image를 복사 할수 있고 json을 빼고 복사할 수 있다.
FIX_SIZE 가 True이면 해당 크기로 저장을 하고
그렇지 않으면 실제 크기로 저장한다.
번호판 번호의 최대길이 보다 크면 이상 파일로 판단하여 다른 곳으로 옮기는 기능을 수행 할수도 있다. 
- type을 json 저장
- parent image width 저장
- parent image height 저장
- parent image name 저장
- plate start x 저장
- plate start y 저장
-22.05.10 resize 옵션을 추가 함. --resize 320 320으로 하면 그 크기로 영상이 잘라지게 수정
@author:  윤경섭
"""

import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile
from label_tools import insertlabel_with_points, PolygonOverlab,SupressInBox,box2polygon
import numpy as np
from label_tools import *
import matplotlib.pyplot as plt
import random

#------------------------------
# 수정할 내용

OUTPUT_FOLDER_NAME = 'out' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
MOTORCYCLE_FOLDER_SEPERATE = True   #모터싸이클 패스를 분리할지 여부
MOTORCYCLE_FOLDER_NAME = 'type13'
IMAGE_FOLDER_NAME = 'images' #csv 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'images' #json 폴더가 있는 경로
ABNORMAL_FOLDER_NAME = 'abnormal' #이상 번호판 폴더 경로
MIDDLE_PATH =  os.path.join('dataset','test')
IMAGE_FILE_COPY = True #이미지 파일 복사 여부
PLATE_SUFFIX = "_Plate_" #추가로 붙일 이름.
PLATE_CROP_MARGIN = 5 #번호판을 딸 때 얼마나 여유를 주는지를 정한다
INCLUDE_TYPE_INFO = True #번호판 타입 데이터를 포함할지 여부이다.
DEFAULT_LABEL_FILE = "./LPR_Total_Labels.txt"  #라벨 파일이름
COPY_ABNORMAL_FILES = True     #이상 번호판 json / image를 다른 곳으로 복사할지 여부 True : 옮김 False : 안옮김.
COPY_JSON = True   # json 파일을 복사할지 여부이다.
FIX_SIZE = True     #번호판을 고정 크기로 잘나낼지 여부 고정 크기이면 아래 IMG_SIZE가 적용된다.
IMG_SIZE = 320      # fix size 일때 번호판 발라낼 크기
default_train_ratio = 0.8  # train validation 비율을 나눈다.
groupname = ['train','valid']
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)
DEFAULT_OUPUT_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,OUTPUT_FOLDER_NAME)
DEFAULT_ABNORMAL_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,ABNORMAL_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="Sample TensorFlow XML-to-TFRecord converter")
parser.add_argument("-l",
                    "--labelfile",
                    help="Label file where the text files are stored.",
                    type=str,default=DEFAULT_LABEL_FILE)

parser.add_argument("-j",
                    "--json_dir",
                    help="Complete Path to the file where the input .csv files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
# 이미지의 크기를 일정 크기로 변경한다.
parser.add_argument('-r', '--resize', type=int, nargs=2,help='resize imagesize width, height',default=[320,320], required=False)


parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)

parser.add_argument("-a",
                    "--abnormal_path",
                    help="Path of abnormal images and jsons", type=str,default=DEFAULT_ABNORMAL_PATH)

# training / validateion  비율을 설정한다.
parser.add_argument("-rt",
                    "--ratio", type=float,
                    help="train validation ratio ex[0.8,0.2] ",default=default_train_ratio, required=False)

args = parser.parse_args()

resize = []

b_RESIZE = False
RESIZE_IMAGE_WIDTH = 0
RESIZE_IMAGE_HEIGHT = 0

if args.resize[0] : 
    b_RESIZE = True

for i in args.resize:
    resize.append(i)

RESIZE_IMAGE_WIDTH = resize[0]
RESIZE_IMAGE_HEIGHT = resize[1]

fLabels = pd.read_csv(args.labelfile, header = None )
LABEL_FILE_CLASS = fLabels[0].values.tolist()
HUMAN_NAMES= dict(zip(LABEL_FILE_CLASS, fLabels[1].values.tolist()))

# 디렉토리 생성
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)
    
if not os.path.exists(args.json_dir) :
    print("No json folder exists. check the folder :",args.json_dir)
    sys.exit(0)

if not os.path.exists(args.output_path) :
    createFolder(args.output_path)

if not os.path.exists(os.path.join(args.output_path,groupname[0])) :
    createFolder(os.path.join(args.output_path,groupname[0]))
    
if MOTORCYCLE_FOLDER_SEPERATE:
    if not os.path.exists(os.path.join(args.output_path,groupname[0],MOTORCYCLE_FOLDER_NAME)) :
        createFolder(os.path.join(args.output_path,groupname[0],MOTORCYCLE_FOLDER_NAME))
    
    if not os.path.exists(os.path.join(args.output_path,groupname[1],MOTORCYCLE_FOLDER_NAME)) :
        createFolder(os.path.join(args.output_path,groupname[1],MOTORCYCLE_FOLDER_NAME))

#create abnormal folder
if not os.path.exists(args.abnormal_path) :
    createFolder(args.abnormal_path)   

json_data = OrderedDict()

ofilename = ""

train_ratio = args.ratio


# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.json_dir):

    json_ext = 'json'
    file_names = [fn for fn in os.listdir(args.json_dir)
                  if any(fn.endswith(ext) for ext in json_ext)]
   
    process_num = len(file_names)
    
    print("Total process file count is {0}".format(process_num))
    
    random.shuffle(file_names)
    train_file_count = int(process_num*train_ratio)
    tfiles = file_names[0:train_file_count]
    vfiles = file_names[train_file_count :]
    imagefilelist = [tfiles,vfiles]
    for n in range(2):

        for filename in imagefilelist[n] :
    
            print("Processing : {0}".format(filename))
            
            #json  파일을 연다.
            try:
                
                with open(os.path.join(args.json_dir,filename), 'r',encoding="UTF-8") as f:
                    json_data = json.load(f)
                    
            except IOError:
                    print("Error: File does not appear to exist")
                    continue
                
                
            json_data['imageData'] = None
            
            plateNames, pboxes, plate_polygons = GetPlateNameFromJson(json_data = json_data, enlabel = LABEL_FILE_CLASS, human_dic = HUMAN_NAMES )
            
            basename, ext = os.path.splitext(filename)
            
            src_image_filename = json_data['imagePath']
            
            image_width = int (json_data['imageWidth'])
            image_height = int (json_data['imageHeight'])
            
            orgshapes = json_data['shapes'] 
            
            for index, plateName in enumerate(plateNames):
                json_data['shapes'] = orgshapes
                if plateName :
                    if len(plateName) > 10:
                        if COPY_ABNORMAL_FILES:
                            #파일을 복사한다.
                            src = os.path.join(args.json_dir, filename)
                            dst = os.path.join(args.abnormal_path, filename)
                            copyfile(src,dst)
                            src = os.path.join(args.image_dir, src_image_filename)
                            dst = os.path.join(args.abnormal_path, src_image_filename)
                            copyfile(src,dst)
                        continue    
                    else :
                        ofilename = basename + PLATE_SUFFIX + plateName
                else:
                    if COPY_ABNORMAL_FILES:
                        #파일을 복산한다.
                        src = os.path.join(args.json_dir, filename)
                        dst = os.path.join(args.abnormal_path, filename)
                        copyfile(src,dst)
                        src = os.path.join(args.image_dir, src_image_filename)
                        dst = os.path.join(args.abnormal_path, src_image_filename)
                        copyfile(src,dst)
                        continue

                #plate 가 있는지 확인한다.
                plate_type = None
                #번호판의 시작 위치이다.
                plate_sx = None
                plate_sy = None
                #번호판 종료 위치이다.
                plate_ex = None
                plate_ey = None
                #번호판의 넓이 높이 이다.
                plate_width = None
                plate_height = None
                
                plate_polygon = None
                
                find_plate = False
        
                # plate_sx = pboxes[index][1] - PLATE_CROP_MARGIN
                # plate_sy = pboxes[index][0] - PLATE_CROP_MARGIN
                # plate_ex = pboxes[index][3] + PLATE_CROP_MARGIN
                # plate_ey = pboxes[index][2] + PLATE_CROP_MARGIN
                points = plate_polygon = plate_polygons[index]
                point_sx = points[:,0]
                point_sy = points[:,1]
                plate_sx = np.min(point_sx,axis=0) - PLATE_CROP_MARGIN
                if plate_sx < 0:
                    plate_sx = 0
                plate_sy = np.min(point_sy,axis=0) - PLATE_CROP_MARGIN
                if plate_sy < 0:
                    plate_sy = 0
                plate_ex = np.max(point_sx,axis=0) + PLATE_CROP_MARGIN
                if plate_ex >= image_width:
                    plate_ex = image_width - 1
                plate_ey = np.max(point_sy,axis=0) + PLATE_CROP_MARGIN
                if plate_ey >= image_height:
                    plate_ey = image_height - 1
                plate_width =  plate_ex - plate_sx + 1
                plate_height = plate_ey - plate_sy + 1
                #print(plate_sx, plate_sy)
                #영상 크기 업데이트
                if b_RESIZE :
                    json_data['imageWidth'] = RESIZE_IMAGE_WIDTH
                    json_data['imageHeight'] = RESIZE_IMAGE_HEIGHT                        
                else:
                    json_data['imageWidth'] = plate_width
                    json_data['imageHeight'] = plate_height
                    
                find_plate = True
                
                if not INCLUDE_TYPE_INFO :
                    json_data['shapes'].pop(item)
                        
                    break
                box = np.array([[ plate_sx, plate_sy],[ plate_ex, plate_ey]])
                plate_polygon_expand = [[plate_sx,plate_sy],[plate_ex,plate_sy],[plate_ex,plate_ey],[plate_sx,plate_ey]]
            
                new_shapes = []
                if find_plate :   # x, y 번호판 좌표가 있으면....
                    val = np.array([plate_sx, plate_sy])
                    for item, shape in enumerate(json_data['shapes']):
                        points = np.array(shape['points']).astype(int) # numpy로 변형
                        shape_type = shape['shape_type']
                        label = shape['label']
                        
                        # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                        tpoints = []
                        if shape_type == 'rectangle':
                            tpoints = box2polygon(points) #test point를 polygon으로 만든다.
                        else:
                            tpoints = points
                        
                        check_overlab = PolygonOverlab(tpoints,plate_polygon_expand)
                        
                        if shape['label'] == pboxes[index][4]:
                            if check_overlab and INCLUDE_TYPE_INFO: #type 정보를 넣어 준다.
                                check_overlab = True
                            else:
                                check_overlab = False

                        
                        if check_overlab :
                            points = SupressInBox(points, box) #box 범위 안으로 points를 제한 한다.
                            points = points - val + 1
                            if 'type' in label:
                                plate_type = label
                            if b_RESIZE :
                                #이미지의 크기를 resize하는 옵션이면.
                                ratio = 0
                                if plate_width >= plate_height:
                                    ratio = RESIZE_IMAGE_WIDTH / plate_width
                                    points[:,0] = points[:,0]*ratio
                                    points[:,1] = points[:,1]*ratio + (RESIZE_IMAGE_HEIGHT/2 - plate_height*ratio/2)
                                else :
                                    ratio = RESIZE_IMAGE_HEIGHT / plate_height
                                    points[:,1] = points[:,1]*ratio
                                    points[:,0] = points[:,0]*ratio + (RESIZE_IMAGE_WIDTH/2 - plate_width*ratio/2)
                            shape['points'] = list(points) # 값을 업데이트 한다.
                            new_shapes.append(shape)
                    json_data['shapes'] = new_shapes

                else:
                    print("Not find any plate box in {0}".format(filename))
                    continue
         
            
                #이미지 파일이 있는지 확인한다.
                if not os.path.exists(os.path.join(args.image_dir,src_image_filename)) :
                    print("Error! no image file:",src_image_filename)
                    continue
                

                #json 이미지 파일 이름을 바꾼다. 
                image_filename, image_ext = os.path.splitext(src_image_filename)
                json_data['imagePath'] = ofilename + image_ext
                dst_image_filename = ofilename + image_ext
                
                #추가 내용을 저장하다. 
                json_data['plateType'] = plate_type
                json_data['plateStartX'] =  plate_sx
                json_data['plateStartY'] =  plate_sy
                json_data['cropMargin'] = PLATE_CROP_MARGIN
                json_data['parentImageWidth'] = image_width
                json_data['parentImageHeight'] = image_height
                json_data['parentImageName'] = src_image_filename
           
    
                #json 파일로 저장한다.
                output_path = None
                if MOTORCYCLE_FOLDER_SEPERATE:
                    if plate_type == 'type13':
                        output_path = os.path.join(DEFAULT_OUPUT_PATH,groupname[n],MOTORCYCLE_FOLDER_NAME)
                    else:
                        output_path = os.path.join(DEFAULT_OUPUT_PATH,groupname[n])
                else:
                    output_path = os.path.join(DEFAULT_OUPUT_PATH,groupname[n])
                if COPY_JSON:
                    try:
                        with open(os.path.join(output_path,ofilename) +'.json','w', encoding='utf-8') as f:
                            json.dump(json_data,f,ensure_ascii=False,indent="\t", cls=NpEncoder)
                            
                    except IOError:
                        print("Error: json write error")
                        continue
            
                #영상 파일을 연다.
                try:
                    if FIX_SIZE :
                        imgRGB  = imread(os.path.join(args.image_dir,src_image_filename))
                        img_np = np.array(imgRGB)
                        h,w,c = img_np.shape
                        area = (plate_sy/h, plate_sx/w, plate_ey/h, plate_ex/w)
                        det_image_np = extract_sub_image(img_np,area,IMG_SIZE,IMG_SIZE,fixratio=True)
                        imwrite(os.path.join(output_path, dst_image_filename),det_image_np)
        
                    else:
                        img = Image.open(os.path.join(args.image_dir,src_image_filename))
                        area = (plate_sx, plate_sy, plate_ex, plate_ey)
                        cropped_img = img.crop(area)
                        cropped_img.save( os.path.join(output_path, dst_image_filename))
    
                except IOError:
                    print("Error: open jpeg imaeg error {0}".format(src_image_filename))
                    continue
else :
    print("Error! no json directory:",args.json_dir)       


