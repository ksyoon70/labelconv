"""
Created on 2022년 7월 27일
차량 영상 및 해당 json을 읽어 지역문자, 용도문자, 번호를 읽어내어 가로 세로의 비 표시하고, 평균 가로 세로 크기를 알려 준다.
영상을 리싸이즈 할때 어느정도로 해야 할지 알아보기 위하여 사용한다. 
예) python objsplit -i ./image -j ./annots  -t ch
-i 이미지 위치
-j json 위치
-t type ch: 문자  n: number r:지역문자
@author:  윤경섭
"""

import os,sys,shutil
import argparse
import pandas as pd
from label_tools import *
#------------------------------
# 수정할 내용


IMAGE_FOLDER_NAME = 'images' #이미지 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'images' #'annots' #json 폴더가 있는 경로
EXCEPTION_FOLDER_NAME = 'exception'  #예외로 뽑아낼 폴더
MOVE_EXCEPT_FILES = True          #제외 대상으로 파일을 옮길지 여부 True : 옮김 False : 안옮김.
MIDDLE_PATH =  os.path.join('dataset','test')
DEFAULT_LABEL_FILE = "./LPR_Total_Labels.txt" #"./LPR_Labels1.txt"  #라벨 파일이름
CROP_MARGIN = 0
DEFAULT_TYPE = 'a'
PLATE_NUMBER_CHECK = True          #번호판 첵크 여부
MOTOCYCLE_CHECK = False              #이륜차 첵크 여부
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)
DEFAULT_EXCEPT_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,EXCEPTION_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="object split and save in jpeg and annotation files")
parser.add_argument("-l",
                    "--labelfile",
                    help="Label file where the text files are stored.",
                    type=str,default=DEFAULT_LABEL_FILE)
parser.add_argument("-j",
                    "--json_dir",
                    help="Complete Path to the file where the input .json files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)

parser.add_argument("-t",
                    "--object_type",
                    help="object type ch : character n: number r: region", type=str,default=DEFAULT_TYPE)

args = parser.parse_args()

fLabels = pd.read_csv(args.labelfile, header = None )
CLASS_NAMES = fLabels[0].values.tolist()
HUMAN_NAMES= dict(zip(CLASS_NAMES, fLabels[1].values.tolist()))

#클래스를 각각 그룹별로 나눈다.
# CH_CLASS = CLASS_NAMES[21:111]  #문자열 클래스
# NUM_CLASS = CLASS_NAMES[11:21]  #숫자 클래스
# REGION_CLASS = CLASS_NAMES[111:-1] #지역문자 클래스
# VREGION_CLASS = CLASS_NAMES[111:128] #Vertical 지역문자 클래스
# HREGION_CLASS = CLASS_NAMES[128:145] #Horizontal 지역문자 클래스
# OREGION_CLASS = CLASS_NAMES[145:162] #Orange 지역문자 클래스
# REGION6_CLASS = CLASS_NAMES[162:-1] #6 지역문자 클래스
CH_CLASS =  CLASS_NAMES[CLASS_NAMES.index('Ga'):CLASS_NAMES.index('Bae') + 1] #문자열 클래스
NUM_CLASS = CLASS_NAMES[CLASS_NAMES.index('n1'):CLASS_NAMES.index('n0') + 1]  #숫자 클래스
REGION_CLASS = CLASS_NAMES[CLASS_NAMES.index('vSeoul'):CLASS_NAMES.index('UlSan6') + 1] #지역문자 클래스
VREGION_CLASS = CLASS_NAMES[CLASS_NAMES.index('vSeoul'):CLASS_NAMES.index('vUlSan') + 1] #Vertical 지역문자 클래스
HREGION_CLASS = CLASS_NAMES[CLASS_NAMES.index('hSeoul'):CLASS_NAMES.index('hUlSan') + 1] #Horizontal 지역문자 클래스
OREGION_CLASS = CLASS_NAMES[CLASS_NAMES.index('OpSeoul'):CLASS_NAMES.index('OpUlSan') + 1] #Orange 지역문자 클래스
REGION6_CLASS = CLASS_NAMES[CLASS_NAMES.index('Seoul6'):CLASS_NAMES.index('UlSan6') + 1] #6 지역문자 클래스

check_class = [];

if args.object_type == 'ch':        #문자 검사
    check_class = CH_CLASS
elif args.object_type == 'n':       #숫자검사
    check_class = NUM_CLASS
    print("{0} type is Not supporeted yet".format(args.object_type))
    sys.exit(0)
elif args.object_type == 'r':       #지역문자 검사
    check_class = REGION_CLASS
elif args.object_type == 'vr':       #v 지역문자 검사
    check_class = VREGION_CLASS
elif args.object_type == 'hr':       #h 지역문자 검사
    check_class = HREGION_CLASS
elif args.object_type == 'or':       #o 지역문자 검사
    check_class = OREGION_CLASS
elif args.object_type == 'r6':       #6 지역문자 검사
    check_class = REGION6_CLASS
elif args.object_type == 'a':       # 모든 object
    check_class = CLASS_NAMES
else:
    print("{0} type is Not supporeted".format(args.object_type))
    sys.exit(0)
    
    
#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)
    
if not os.path.exists(args.json_dir) :
    print("No json folder exists. check the folder :",args.json_dir)
    sys.exit(0)

if not os.path.exists(DEFAULT_EXCEPT_PATH) :
    print('미인식 제외대상 폴더를 만듭니다. : {}'.format(DEFAULT_EXCEPT_PATH))
    createFolder(DEFAULT_EXCEPT_PATH)

    
json_data = OrderedDict()

ofilename = ""

# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.json_dir):

    json_ext = 'json'
    json_file_names = [fn for fn in os.listdir(args.json_dir)
                  if any(fn.endswith(ext) for ext in json_ext)]
   
    process_json_num = len(json_file_names)
    
    
    
    image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
    imagefile_list = [fn for fn in os.listdir(args.image_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]
    imagefile_num = len(imagefile_list)
    #이미지 이름 중에서 확장자를 뺀이름만 남긴다.
    image_basename_list = [os.path.splitext(file)[0] for file in imagefile_list]
    if imagefile_num != process_json_num :
        print('영상파일과 json 파일의 수가 일치하지 않습니다!!!')
        if imagefile_num > process_json_num :
            print('남는 영상파일이름들:')
            over_image_file_list = [file for file in imagefile_list if not os.path.splitext(file)[0]+'.json' in json_file_names]
            for index, fname in enumerate(over_image_file_list)  :
                print('{} : 이미지파일 : {}'.format(index+1,fname))
                if MOVE_EXCEPT_FILES :
                    src_file = os.path.join(args.image_dir,fname)
                    dst_file = os.path.join(DEFAULT_EXCEPT_PATH,fname)
                    shutil.move(src_file,dst_file)
                    #이미지 파일리스트에서 목록을 삭제한다.
                    imagefile_list.remove(fname)
                    
        elif process_json_num > imagefile_num :
            print('남는 json파일이름들:')
            over_json_file_list = [file for file in json_file_names if not os.path.splitext(file)[0] in image_basename_list]
            for index, fname in enumerate(over_json_file_list)  :
                print('{} : json파일 : {}'.format(index+1,fname))
                if MOVE_EXCEPT_FILES :
                    src_file = os.path.join(args.json_dir,fname)
                    dst_file = os.path.join(DEFAULT_EXCEPT_PATH,fname)
                    shutil.move(src_file,dst_file)
                    #json 목록에서 삭제한다.
                    json_file_names.remove(fname)
            
                    
        
    print("총 json 파일 수 : {0}".format(process_json_num))
    print("총 이미지 파일수 : {0}".format(imagefile_num))
    
    
    crop_width_sum = 0
    crop_height_sum = 0
    label_error = False
    for filename in json_file_names :

        #print("Processing : {0}".format(filename))
        
        #json  파일을 연다.
        try:
            
            with open(os.path.join(args.json_dir,filename), 'r',encoding="UTF-8") as f:
                json_data = json.load(f)
                
        except IOError:
                print("Error: File({0}) does not appear to exist!!!".format(filename))
                continue
        
               
                    
            
        json_data['imageData'] = None
        image_filename = json_data['imagePath']
        basename, ext = os.path.splitext(filename)
        #json에 있는 이미지 파일을 나눈다.
        ibasename, iext = os.path.splitext(image_filename)
        ofilename = basename
        
        #object의 시작 위치이다.
        crop_sx = None
        crop_sy = None
        #object 종료 위치이다.
        crop_ex = None
        crop_ey = None
        #object 넓이 높이 이다.
        crop_width = None
        crop_height = None
        
        crop_polygon = None
        
        find_object = False
        obj_name_ext = None
           
        image_width = int (json_data['imageWidth'])
        image_height = int (json_data['imageHeight'])  

        new_shapes = []
        
        ratio_h = 0;
        ratio_v = 0;
        
        label_error = False
        
        motorcycleBoxList = []
        motorcyclePlateBoxList = []
        if MOTOCYCLE_CHECK:  #이륜차 체크 여부
            for item, shape in enumerate(json_data['shapes']):
                label = shape['label']
                
                if label == 'motorcycle': #오토바이 인지 확인한다.
                    points = np.array(shape['points']).astype(int) # numpy로 변형
                    shape_type = shape['shape_type']
                    
                    # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                    tpoints = []
                    if shape_type == 'rectangle':
                        tpoints = box2polygon(points) #test point를 polygon으로 만든다.
                    else:
                        tpoints = points
                        
                    # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                    tpoints = []
                    if shape_type == 'rectangle':
                        tpoints = box2polygon(points) #test point를 polygon으로 만든다.
                    else:
                        tpoints = points
                        
                    #줄이기 전에 잘라낼 위치를 정한다.
                    crop_xs = points[:,0]
                    crop_ys = points[:,1]

                    crop_sx = np.min(crop_xs,axis=0) - CROP_MARGIN
                    if crop_sx < 0:
                        crop_sx = 0
                    crop_sy = np.min(crop_ys,axis=0) - CROP_MARGIN
                    if crop_sy < 0:
                        crop_sy = 0
                    crop_ex = np.max(crop_xs,axis=0)  + CROP_MARGIN 
                    if crop_ex >= image_width:
                        crop_ex = image_width - 1
                    crop_ey = np.max(crop_ys,axis=0)  + CROP_MARGIN
                    if crop_ey >= image_height:
                        cropey = image_height - 1
                    
                    pbox = [crop_sy,crop_sx,crop_ey,crop_ex]
                    
                    motorcycleBoxList.append(pbox)
                elif label == 'type13':
                    points = np.array(shape['points']).astype(int) # numpy로 변형
                    shape_type = shape['shape_type']
                    
                    # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                    tpoints = []
                    if shape_type == 'rectangle':
                        tpoints = box2polygon(points) #test point를 polygon으로 만든다.
                    else:
                        tpoints = points
                        
                    # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                    tpoints = []
                    if shape_type == 'rectangle':
                        tpoints = box2polygon(points) #test point를 polygon으로 만든다.
                    else:
                        tpoints = points
                        
                    #줄이기 전에 잘라낼 위치를 정한다.
                    crop_xs = points[:,0]
                    crop_ys = points[:,1]

                    crop_sx = np.min(crop_xs,axis=0) - CROP_MARGIN
                    if crop_sx < 0:
                        crop_sx = 0
                    crop_sy = np.min(crop_ys,axis=0) - CROP_MARGIN
                    if crop_sy < 0:
                        crop_sy = 0
                    crop_ex = np.max(crop_xs,axis=0)  + CROP_MARGIN 
                    if crop_ex >= image_width:
                        crop_ex = image_width - 1
                    crop_ey = np.max(crop_ys,axis=0)  + CROP_MARGIN
                    if crop_ey >= image_height:
                        cropey = image_height - 1
                    
                    pbox = [crop_sy,crop_sx,crop_ey,crop_ex]
                    motorcyclePlateBoxList.append(pbox)
                    
            # 이륜차 리스트를 가져오면 오류가 있는지 확인한다. 
            motorcycleExit = False      #이륜차 존재 여부
            if len(motorcyclePlateBoxList) > 0 or len(motorcycleBoxList) > 0 :
                if len(motorcyclePlateBoxList) != len(motorcycleBoxList) :
                    label_error = True   #이륜차와 번호판 갯수는 같아야 한다.
                    motorcycleExit = True
                else:
                    motorcycleExit = True  #
                    
            else:
                motorcycleExit = False
                
                
            if motorcycleExit and label_error:
                #이류차가 존재하고 레이블 에러이면...
                print('{} 이륜차가 레이블갯수 {} 와 이륜번호판 레이블갯수{}가 다름!'.format(filename,len(motorcycleBoxList),len(motorcyclePlateBoxList) ))
                if MOVE_EXCEPT_FILES :
                    #json 파일을 옮긴다.
                    src_file = os.path.join(args.image_dir,filename)
                    dst_file = os.path.join(DEFAULT_EXCEPT_PATH,filename)
                    shutil.move(src_file,dst_file)
                    print('json 점검 : {} 파일을 {}롤 옮겼습니다'.format(filename,dst_file))
                    src_file = os.path.join(args.image_dir,image_filename)
                    dst_file = os.path.join(DEFAULT_EXCEPT_PATH,image_filename)
                    if os.path.isfile(src_file):
                        shutil.move(src_file,dst_file)
                    else:
                        print('오류!!! json 파일 {} 의 imagePath : {} 파일이 존재하지 않습니다.'.format(filename,image_filename))
                        
                continue
            

        for item, shape in enumerate(json_data['shapes']):
            label = shape['label']
            
            if label in check_class:
                
                find_object = True
                obj_name_ext = HUMAN_NAMES[label]
                points = np.array(shape['points']).astype(int) # numpy로 변형
                shape_type = shape['shape_type']
                
                # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                tpoints = []
                if shape_type == 'rectangle':
                    tpoints = box2polygon(points) #test point를 polygon으로 만든다.
                else:
                    tpoints = points
                    
                #줄이기 전에 잘라낼 위치를 정한다.
                crop_xs = points[:,0]
                crop_ys = points[:,1]

                crop_sx = np.min(crop_xs,axis=0) - CROP_MARGIN
                if crop_sx < 0:
                    crop_sx = 0
                crop_sy = np.min(crop_ys,axis=0) - CROP_MARGIN
                if crop_sy < 0:
                    crop_sy = 0
                crop_ex = np.max(crop_xs,axis=0)  + CROP_MARGIN 
                if crop_ex >= image_width:
                    crop_ex = image_width - 1
                crop_ey = np.max(crop_ys,axis=0)  + CROP_MARGIN
                if crop_ey >= image_height:
                    cropey = image_height - 1
                crop_width =  crop_ex - crop_sx
                crop_height = crop_ey - crop_sy
                crop_width_sum += crop_width
                crop_height_sum += crop_height
            else:
                print('레이블 목록에 없습니다. {} filename = {}'.format(label,filename))
                if MOVE_EXCEPT_FILES :
                    #json 파일을 옮긴다.
                    src_file = os.path.join(args.image_dir,filename)
                    dst_file = os.path.join(DEFAULT_EXCEPT_PATH,filename)
                    shutil.move(src_file,dst_file)
                    print('json 점검 : {} 파일을 {}롤 옮겼습니다'.format(filename,dst_file))
                    src_file = os.path.join(args.image_dir,image_filename)
                    dst_file = os.path.join(DEFAULT_EXCEPT_PATH,image_filename)
                    if os.path.isfile(src_file):
                        shutil.move(src_file,dst_file)
                    else:
                        print('오류!!! json 파일 {} 의 imagePath : {} 파일이 존재하지 않습니다.'.format(filename,image_filename))
                    label_error = True
                    break
                
        if label_error:
            continue
               
                
        #번호판 읽었을 때 이상 유무를 확인한다.
        if PLATE_NUMBER_CHECK:
            plateName = GetPlateNameFromJson(json_data = json_data, enlabel = CLASS_NAMES, human_dic = HUMAN_NAMES )
            if(plateName != None and len(plateName) > 9 ):   #현재 번호판 인식문자의 길이가 9글자 보다 클수 없다.
                print('인식번호이상 : {} 파일명 = {} '.format(plateName,filename))
                
                if MOVE_EXCEPT_FILES :
                    #json 파일을 옮긴다.
                    src_file = os.path.join(args.image_dir,filename)
                    dst_file = os.path.join(DEFAULT_EXCEPT_PATH,filename)
                    shutil.move(src_file,dst_file)
                    print('json 점검 : {} 파일을 {}롤 옮겼습니다'.format(filename,dst_file))
                    src_file = os.path.join(args.image_dir,image_filename)
                    dst_file = os.path.join(DEFAULT_EXCEPT_PATH,image_filename)
                    if os.path.isdir(src_file):
                        shutil.move(src_file,dst_file)
                    else:
                            print('오류!!! json 파일 {} 의 imagePath : {} 파일이 존재하지 않습니다.'.format(filename,image_filename))
                    continue
        
        #json 파일의 imagePath 가 유효한지 확인한다.
        src_file = os.path.join(args.image_dir,image_filename)
        if not os.path.isfile(src_file):
            #json 과 같은 이름의 이미지 파일이 있는지 확인한다.
            src_file = os.path.join(args.image_dir,basename+iext)
            if os.path.isfile(src_file):
                #해당 파일이 있으므로 json파일 수정한다.
                json_data['imageData'] = None
                json_data['imagePath'] = basename+iext
                #json 파일로 저장한다.
                print('JSON 오류 이미지 {}가 없으므로 ==> {}로 수정'.format(image_filename,basename+iext))
                
                try:
                    with open( os.path.join(args.json_dir,filename),'w', encoding='utf-8') as f:
                        json.dump(json_data,f,ensure_ascii=False,indent="\t", cls=NpEncoder)
                        
                except IOError:
                    print("Error: json write error")
                    continue
    
    if args.object_type != 'a' :
        crop_width_avr = crop_width_sum/process_json_num
        crop_height_avr = crop_height_sum/process_json_num
                    
        print('평균 가로길이 : {0}'.format(crop_width_avr))
        print('평균 세로길이 : {0}'.format(crop_height_avr))
        print('가로 / 세로 비:  1:{0}'.format(crop_height_avr/crop_width_avr))
     
          
else :
    print("Error! no json directory:",args.json_dir)       