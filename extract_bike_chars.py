# -*- coding: utf-8 -*-
"""
Created on Fri Oct 28 19:40:35 2022
이 파일은 이륜차 인식시에 필요한 한글을 뽑아내는 스크립트이다.
@author: headway
"""
import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile, move
from label_tools import *
from label_tools import NpEncoder


fLabels = pd.read_csv('전국이륜차지역.txt', header = None )
regions = fLabels[0].values.tolist()
sub_regions = fLabels[1].values.tolist()

LprLabels = pd.read_csv('LPR_Labels1.txt', header = None )
humanchars = LprLabels[1].values.tolist()

usage_chars = humanchars[21:111]

#구, 군, 시가 문자끝에 있으면 삭제한다.

removelist = ['구','군','시']

for i, sregion  in enumerate(sub_regions):
    if sregion[-1]  in removelist:
        sub_regions[i] = sregion[:-1]
        

schars = []

for sregion in sub_regions:
    for char in sregion:
        if  char not in schars  and char not in usage_chars:
            schars.append(char)

schars = sorted(schars)
#이륜차에 추가로 필요한 한글            
print(schars )
print(len(schars))