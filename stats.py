"""
통계데이터를 산출한다.
@author:  윤경섭
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:05:28 2022

@author: 윤경섭
"""

from distutils.file_util import move_file
import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile,move
from label_tools import insertlabel_with_points
from label_tools import NpEncoder
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import font_manager, rc

font_path = "C:/Windows/Fonts/NGULIM.TTF"
font = font_manager.FontProperties(fname=font_path).get_name()
rc('font', family=font)
plt.rcParams['axes.unicode_minus'] = False  ## 추가해줍니다. 
#------------------------------
# 수정할 내용

OUTPUT_FOLDER_NAME = 'result' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
IMAGE_FOLDER_NAME = 'images' #csv 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'images' #json 폴더가 있는 경로
MIDDLE_PATH = os.path.join('dataset','labels') #'dataset' #os.path.join('dataset','test')
FILE_COPY = False  #이미지 파일 복사 여부
ADD_RECOGNITION_RESULT = True #인식 결과를 파일에 붙일지 여부
DEFAULT_LABEL_FILE = "./LPR_Labels1.txt"  #라벨 파일이름
FIND_STR = 'c_'         #찾으려는 문자열.
USELESS_COUNT = 21 #필요ㄱ없는 갯수
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)
DEFAULT_OUPUT_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,OUTPUT_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="json을 통해서 통계데이터를 산출한다.")
parser.add_argument("-l",
                    "--labelfile",
                    help="Label file where the text files are stored.",
                    type=str,default=DEFAULT_LABEL_FILE)
parser.add_argument("-j",
                    "--json_dir",
                    help="Complete Path to the file where the input .csv files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)


args = parser.parse_args()

fLabels = pd.read_csv(args.labelfile, header = None )
LABEL_FILE_CLASS = fLabels[0].values.tolist()
HUMAN_NAMES= dict(zip(LABEL_FILE_CLASS, fLabels[1].values.tolist()))
stats_label_list = LABEL_FILE_CLASS[USELESS_COUNT:]  #통계 내려는 레이블만 따로 떼어 낸다. Ga Na..
zerolist = [0 for i in range(len(stats_label_list))] #22는 BG ~ 0 까지
STATS_DIC = dict(zip(stats_label_list, zerolist))




#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)
    
if not os.path.exists(args.json_dir) :
    print("No json folder exists. check the folder :",args.json_dir)
    sys.exit(0)


json_data = OrderedDict()

ofilename = ""
# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.json_dir):

    json_ext = 'json'
    file_names = [fn for fn in os.listdir(args.json_dir)
                  if any(fn.endswith(ext) for ext in json_ext)]
   
    process_num = len(file_names)
    
    print("Total process file count is {0}".format(process_num))
    
    for filename in file_names :

        #print("processing : {0}".format(filename))
        
        #json  파일을 연다.
        try:
            
            with open(os.path.join(args.json_dir,filename), 'r',encoding="UTF-8") as f:
                json_data = json.load(f)
                
        except IOError:
                print("Error: File does not appear to exist")
                continue
            
        for item, shape in enumerate(json_data['shapes']):
            label =  shape['label']
            if label in stats_label_list:
                STATS_DIC[label] += 1
                        
        
    #완료가 되면 print
    keyList = STATS_DIC.keys()
 
    human_list = []
    for item in keyList : 
        print("%s\t%d"%(item,STATS_DIC[item]))
        human_list.append(HUMAN_NAMES[item])
        
    x = np.arange(len(stats_label_list))
    colors = ['y', 'dodgerblue', 'C2']
    plt.bar(x, STATS_DIC.values(),color=colors)
    plt.xticks(x, human_list)
    plt.title('전체 통계')
    plt.show()
    
    #한글자 한글만 표시한다.
    char_len = 111 - 22 + 1
    x1 = np.arange(len(stats_label_list[0:char_len]))
    keylist1 = stats_label_list[0:char_len]

    y1 = []
    human_list1 = []
    for item in keylist1: 
        print("%s\t%d"%(item,STATS_DIC[item]))
        human_list1.append(HUMAN_NAMES[item])
        y1.append(STATS_DIC[item])
          
    
    plt.bar(x1, y1,color=colors)
    plt.xticks(x1, human_list1)
    plt.title('용도문자 문자')
    plt.show()
    
    #vreg 갯수를 표시한다.
    vreg_len = 128 - 112 + 1
    stix = char_len
    x2 = np.arange(len(stats_label_list[stix : stix + vreg_len]))
    keylist2 = stats_label_list[stix : stix + vreg_len]

    y2 = []
    human_list2 = []
    for item in keylist2: 
        human_list2.append(HUMAN_NAMES[item])
        y2.append(STATS_DIC[item])
          
    plt.bar(x2, y2,color=colors)
    plt.xticks(x2, human_list2)
    plt.title('세로 지역 문자')
    plt.show()
    
    #hreg 갯수를 표시한다.
    hreg_len = 145 - 129 + 1
    stix = stix + vreg_len
    x3 = np.arange(len(stats_label_list[stix : stix + hreg_len]))
    keylist3 = stats_label_list[stix : stix + hreg_len]

    y3 = []
    human_list3 = []
    for item in keylist3: 
        human_list3.append(HUMAN_NAMES[item])
        y3.append(STATS_DIC[item])
          
    plt.bar(x3, y3,color=colors)
    plt.xticks(x3, human_list3)
    plt.title('가로 지역 문자')
    plt.show()
    
    #oreg 갯수를 표시한다.
    oreg_len = 162 - 146 + 1
    stix = stix + hreg_len
    x4 = np.arange(len(stats_label_list[stix : stix + oreg_len]))
    keylist4 = stats_label_list[stix : stix + oreg_len]

    y4 = []
    human_list4 = []
    for item in keylist4: 
        human_list4.append(HUMAN_NAMES[item])
        y4.append(STATS_DIC[item])
          
    plt.bar(x4, y4,color=colors)
    plt.xticks(x4, human_list4)
    plt.title('영 지역 문자')
    plt.show()
    
    #reg6 갯수를 표시한다.
    reg6_len = 179 - 163 + 1
    stix = stix + oreg_len
    x5 = np.arange(len(stats_label_list[stix : stix + reg6_len]))
    keylist5 = stats_label_list[stix : stix + reg6_len]
    
    y5 = []
    human_list5 = []
    for item in keylist5: 
        human_list5.append(HUMAN_NAMES[item])
        y5.append(STATS_DIC[item])
          
    plt.bar(x5, y5,color=colors)
    plt.xticks(x5, human_list5)
    plt.title('6 지역 문자')
    plt.show()
    
          
else :
    print("Error! no json directory:",args.json_dir)          


