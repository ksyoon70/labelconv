"""
Created on Tue Mar 15 14:19:22 2022
인펙영상 파일에서 인식된 내용에 추가로 인식된 내용을 파일 명에서 지우는 역할을 한다.
이미지 파일이 있으면 json 파일과 이름을 동일하게 만든다.
example 
00317318_38더1739.jpg
--> 
00317318.jpg

@author:  윤경섭
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:05:28 2022

@author: 윤경섭
"""

import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile, move
from pathlib import Path
from label_tools import *
from label_tools import NpEncoder

#------------------------------
# 수정할 내용

OUTPUT_FOLDER_NAME = 'result' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
IMAGE_FOLDER_NAME = 'images' #csv 파일에 있는 영상 파일이 있는 경로
MIDDLE_PATH = os.path.join('dataset','test') #'dataset' #os.path.join('dataset','test')
DEFAULT_LABEL_FILE = "./LPR_Labels1.txt"  #라벨 파일이름

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = r'F:\SPB_Data\RealTimeObjectDetection-main\Tensorflow\workspace\images\car-plate\BMT' #os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_IMAGES_PATH = os.path.normpath(DEFAULT_IMAGES_PATH)
DEFAULT_IMAGES_PATH = Path(DEFAULT_IMAGES_PATH)
DEFAULT_OUPUT_PATH = r'F:\SPB_Data\RealTimeObjectDetection-main\Tensorflow\workspace\images\car-plate\BMT_RENAME' #os.path.join(ROOT_DIR,MIDDLE_PATH,OUTPUT_FOLDER_NAME)
DEFAULT_OUPUT_PATH = os.path.normpath(DEFAULT_OUPUT_PATH)
DEFAULT_OUPUT_PATH = Path(DEFAULT_OUPUT_PATH)
#------------------------------

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="Sample TensorFlow XML-to-TFRecord converter")
parser.add_argument("-l",
                    "--labelfile",
                    help="Label file where the text files are stored.",
                    type=str,default=DEFAULT_LABEL_FILE)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)


args = parser.parse_args()

# 디렉토리 생성
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)


if not os.path.exists(args.output_path) :
    createFolder(args.output_path)


ofilename = ""
# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.image_dir):


    image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
    imagefile_list = [fn for fn in os.listdir(args.image_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]
    imagefile_num = len(imagefile_list)
    #이미지 이름 중에서 확장자를 뺀이름만 남긴다.
    image_basename_list = [os.path.splitext(file)[0] for file in imagefile_list]

    print("총 이미지 파일수 : {0}".format(imagefile_num))
    
    for filename in imagefile_list :

        print("processing : {0}".format(filename))
        
        basename, ext = os.path.splitext(filename)
        
        index_ch = filename.rfind('_')
        
        if (index_ch == -1):
            ofilename = filename
        else:
            ofilename = filename[0:index_ch] + ext
            
        # 파일을 result 디렉토리에 복사한다.
        src = os.path.join(args.image_dir,filename)
        dst = os.path.join(args.output_path,ofilename)
        if os.path.exists(src):
            copyfile(src,dst)
            #move(src,dst)
        else:
            print("Error! no image file:",filename)

        #json 파일이 있으면 복사한다.
        src = os.path.join(args.image_dir,basename + '.json')
        jbasename, ext = os.path.splitext(ofilename)
        dst = os.path.join(args.output_path,jbasename + '.json')
        if os.path.exists(src):
            try:
                #json파일의 내용을 수정해야 한다.
                with open(os.path.join(src), 'r',encoding="UTF-8") as f:
                    json_data = json.load(f)
                    json_data['imagePath'] = ofilename
                
                f.close

                #json 파일로 저장한다.
            
                with open(dst,'w', encoding='utf-8') as f:
                    json.dump(json_data,f,ensure_ascii=False,indent="\t", cls=NpEncoder)
                    
                f.close
                    
            except IOError:
                print("Error: json write error")
            continue
            copyfile(src,dst)
            
          
else :
    print("Error! no json directory:",args.json_dir)            


