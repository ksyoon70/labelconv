import numpy as np
import cv2

class SutherlandHodgman:
    def __init__(self, subject_polygon, clip_polygon):
        """
        subject_polygon: 교집합을 구하고자 하는 임의 오목/볼록 다각형 (list of [x,y])
        clip_polygon:    직사각형(또는 다른 다각형) 꼭짓점. 반시계(CCW)로 정의된 list of [x,y]
        """
        self.subject_polygon = np.array(subject_polygon, dtype=np.float32)
        self.clip_polygon = np.array(clip_polygon, dtype=np.float32)

    def is_inside(self, point, edge_start, edge_end):
        """
        point가 edge( edge_start->edge_end )의 '왼쪽(반시계방향 기준)'에 있는지 판별
        cross(v1, v2)가 양수이면 왼쪽, 음수이면 오른쪽
        """
        v1 = edge_end - edge_start
        v2 = point - edge_start
        cross = np.cross(v1, v2)
        return cross >= 0  # >= 0이면 '왼쪽 또는 경계선 위'로 보고 inside

    def compute_intersection(self, p1, p2, edge_start, edge_end):
        """
        선분 p1->p2와, 클리핑 edge(edge_start->edge_end)의 교차점을 구한다.
        """
        A1 = p2[1] - p1[1]   # (y2 - y1)
        B1 = p1[0] - p2[0]   # (x1 - x2)
        C1 = A1 * p1[0] + B1 * p1[1]

        A2 = edge_end[1] - edge_start[1]
        B2 = edge_start[0] - edge_end[0]
        C2 = A2 * edge_start[0] + B2 * edge_start[1]

        denominator = A1 * B2 - A2 * B1

        # 평행(또는 거의 평행)하면 p1 반환 (간단 처리; 엄밀하게 하려면 추가 로직)
        if abs(denominator) < 1e-12:
            return p1

        x = (B2 * C1 - B1 * C2) / denominator
        y = (A1 * C2 - A2 * C1) / denominator

        return np.array([x, y], dtype=np.float32)

    def clip(self):
        """
        Sutherland-Hodgman 알고리즘으로 subject_polygon을 clip_polygon으로 클리핑한다.
        교집합 폴리곤의 꼭짓점 리스트를 반환한다.
        """
        output_list = self.subject_polygon

        # clip_polygon의 각 edge를 순회하며, output_list를 점차 clip
        for i in range(len(self.clip_polygon)):
            edge_start = self.clip_polygon[i]
            edge_end = self.clip_polygon[(i + 1) % len(self.clip_polygon)]

            input_list = output_list
            output_list = []

            if len(input_list) == 0:
                break  # 교집합이 이미 비었다면 중단

            for j in range(len(input_list)):
                current_point = input_list[j]
                prev_point = input_list[(j - 1) % len(input_list)]

                curr_inside = self.is_inside(current_point, edge_start, edge_end)
                prev_inside = self.is_inside(prev_point, edge_start, edge_end)

                if prev_inside and curr_inside:
                    # 이전 점도 inside, 현재 점도 inside => 현재점 그대로 출력
                    output_list.append(current_point)
                elif prev_inside and not curr_inside:
                    # 이전 점은 inside, 현재 점은 outside => 교차점만 출력
                    intersection = self.compute_intersection(prev_point, current_point, edge_start, edge_end)
                    output_list.append(intersection)
                elif not prev_inside and curr_inside:
                    # 이전 점은 outside, 현재 점은 inside => 교차점 + 현재점
                    intersection = self.compute_intersection(prev_point, current_point, edge_start, edge_end)
                    output_list.append(intersection)
                    output_list.append(current_point)
                # else: 둘 다 outside => 아무것도 추가 X

        return np.array(output_list, dtype=np.float32)

    def compute_area(self, polygon):
        """
        OpenCV의 contourArea를 사용하여 다각형의 면적을 계산.
        polygon: np.array([[x1, y1], [x2, y2], ...], dtype=np.float32)
        """
        if len(polygon) < 3:
            return 0.0
        area = cv2.contourArea(polygon)
        return area
