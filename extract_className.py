# 이미지 파일과 json 파일에서 클래스 label만 추출하는 코드이다.
# 작성자 윤경섭
# 작성일 2024년 12월 09일

import os, shutil
import sys
import argparse
import json	
from pathlib import Path, PureWindowsPath
from pathlib import Path
#---------------------------------------------------------
DEFAULT_CLASS_LABEL = ['motorcycle'] #['car','bus','truck', 'motorcycle','bicycle','human','kickboard'] #['plate'] #['car','bus','truck'] #['type1','type4','type5','type6','type7']  #추출하고자 하는 plate type를 지정한다. type1 ~ type13까지 가능
FILE_MOVE = True  #번호판 파일을 옮길지 여부

src_dir = r'D:\SPB_Data\labelconv\dataset\yolo\images'
src_dir = os.path.normpath(src_dir)
src_dir = Path(src_dir)

images_dir = src_dir
annots_dir = src_dir

out_dir = r'D:\SPB_Data\labelconv\dataset\yolo\out'
out_dir = os.path.normpath(out_dir)
out_dir = Path(out_dir)

#---------------------------------------------------------

if not os.path.exists(out_dir) :
    os.makedirs(out_dir)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="이미지 파일과 json 파일에서 원하는 타입의 번호판 만 추출하는 스크립트이다")

parser.add_argument("-t",
                    "--type",
                    help="찾으려는 번호판 type을 입력한다",
                    type=str,default=DEFAULT_CLASS_LABEL)

args = parser.parse_args()    

image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']

imagefile_list = []
for path, dirs, files in os.walk(images_dir):
    for file in files:
        if any(file.lower().endswith(ext.lower()) for ext in image_ext):
            imagefile_list.append(os.path.join(path, file))  # 전체 경로를 포함한 파일 추가


total_files_count = len(imagefile_list)

print('검토할 총 영상 수: {}'.format(total_files_count))

for file_path in imagefile_list:
    
    # 해당 이미지에 해당하는 json 파일을 찾는다.
    bFind = False #원하는 레이블을 찾았는지 여부
    jsonfile_path = os.path.splitext(file_path)[0] +'.json'
    if os.path.isfile(os.path.join(annots_dir,jsonfile_path)):
        #json 파일을 읽는다.
        
        with open(os.path.join(annots_dir,jsonfile_path), 'r',encoding="UTF-8") as f:
            json_data = json.load(f)
            
            for item, shape in enumerate(json_data['shapes']):
                label = shape['label']
                if label in args.type :
                    bFind = True
                    break
                else :
                    continue
                
        f.close
        
        if bFind :
           src_file = file_path
           filename = os.path.basename(file_path)
           dst_file = os.path.join(out_dir,filename)
           if FILE_MOVE:
               shutil.move(src_file,dst_file)
           else:
               shutil.copy(src_file,dst_file) 
                    
           src_file = jsonfile_path
           filename = os.path.basename(jsonfile_path)
           dst_file = os.path.join(out_dir,filename)
           if FILE_MOVE:
               shutil.move(src_file,dst_file)
           else:
               shutil.copy(src_file,dst_file)    
    
    
print('작업이 종료되었습니다.')