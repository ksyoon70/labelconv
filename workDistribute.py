# 이미지 파일과 json 파일에서 폴더별로 분배하는 스크립트이다.
# 작성자 윤경섭
# 작성일 2023년 2월 17일

import os, shutil
import sys
import argparse
import json
from label_tools import *
#---------------------------------------------------------
PLATETYPE_MOVE = True  #번호판 파일을 옮길지 여부
base_dir = os.path.abspath(os.path.dirname(__file__))
database_dir  =  os.path.join(base_dir,'dataset')
labels_dir = os.path.join(database_dir,'yolo')
annots_dir = os.path.join(labels_dir,'images')
DEFAULT_INPUT = os.path.join(labels_dir,'images')
DEFAULT_OUTPUT = os.path.join(labels_dir,'result')
MAX_FOLDER_CAP  = 100    #한폴더에 들어가는 영상 수. 이미지 json 포함한 갯수
ALLOW_ONE_IMAGE_FILE = False
#---------------------------------------------------------



# Initiate argument parser
parser = argparse.ArgumentParser(
    description="이미지 파일과 json 파일을 작업분배 해주는 스크립트이다")

parser.add_argument("-i",
                    "--input",
                    help="입력 영상 위치이다.",
                    type=str,default=DEFAULT_INPUT)
parser.add_argument("-o",
                    "--out",
                    help="저장하려는 영상 위치이다.",
                    type=str,default=DEFAULT_OUTPUT)

args = parser.parse_args()    

images_dir= args.input
out_dir = args.out

if not os.path.exists(out_dir) :
    os.makedirs(out_dir)

image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
imagefile_list = [fn for fn in os.listdir(images_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]

total_files_count = len(imagefile_list)

print('분배할 총 영상 수: {}'.format(total_files_count))

cur_folder_cap = 0;
folder_index = 1
folder ='{0:03}'.format(folder_index)

if not os.path.exists( os.path.join(out_dir,folder)) :
    createFolder(os.path.join(out_dir,folder))
for filename in imagefile_list:
    
    # 해당 이미지에 해당하는 json 파일을 찾는다.
    bFind = False #원하는 레이블을 찾았는지 여부
    jsonfilename = os.path.splitext(filename)[0] +'.json'
    txtfilename = [ 'X' + os.path.splitext(filename)[0] + '.txt', 'X' + os.path.splitext(filename)[0] + '.txt']
    basefilename, ext = os.path.splitext(filename)
    strsplited = basefilename.split('_')
    #imagefilepair = [ basefilename + 'F' + ext, basefilename + 'R' + ext]
    filesplitlen = len(strsplited)
    if filesplitlen > 2 :
        imagefilepair = [ strsplited[0] + 'F_' + strsplited[1] + ext, strsplited[0] + 'R_' + strsplited[1] + ext]
        
    if os.path.isfile(os.path.join(annots_dir,jsonfilename)):
        
        #json 파일을 읽는다.
        src_file = os.path.join(images_dir,filename) 
        dst_file = os.path.join(out_dir,folder,filename)
        if PLATETYPE_MOVE:
            shutil.move(src_file,dst_file)
        else:
            shutil.copy(src_file,dst_file) 
                
        src_file = os.path.join(annots_dir,jsonfilename)
        dst_file = os.path.join(out_dir,folder,jsonfilename)
        if PLATETYPE_MOVE:
            shutil.move(src_file,dst_file)
        else:
            shutil.copy(src_file,dst_file)
            
        cur_folder_cap = (cur_folder_cap + 1) % MAX_FOLDER_CAP
        if cur_folder_cap == 0:  #폴더를 더 만든다.
            folder_index += 1
            folder ='{0:03}'.format(folder_index)
            if not os.path.exists( os.path.join(out_dir,folder)) :
                createFolder(os.path.join(out_dir,folder)) 
    elif os.path.isfile(os.path.join(annots_dir,imagefilepair[0])) and os.path.isfile(os.path.join(annots_dir,imagefilepair[1])) and filesplitlen > 2:
         #json 파일을 읽는다.
        src_file = os.path.join(images_dir,imagefilepair[0]) 
        dst_file = os.path.join(out_dir,folder,imagefilepair[0])
        if PLATETYPE_MOVE:
            if os.path.isfile(src_file):
                shutil.move(src_file,dst_file)
        else:
            if os.path.isfile(src_file):
                shutil.copy(src_file,dst_file) 
        
        src_file = os.path.join(images_dir,imagefilepair[1]) 
        dst_file = os.path.join(out_dir,folder,imagefilepair[1])
        
        if PLATETYPE_MOVE:
            if os.path.isfile(src_file):
                shutil.move(src_file,dst_file)
        else:
            if os.path.isfile(src_file):
                shutil.copy(src_file,dst_file)
        
        src_file = os.path.join(images_dir,filename)         
        if os.path.isfile(src_file):
          os.remove(src_file)
    
        cur_folder_cap = (cur_folder_cap + 1) % MAX_FOLDER_CAP
        if cur_folder_cap == 0:  #폴더를 더 만든다.
            folder_index += 1
            folder ='{0:03}'.format(folder_index)
            if not os.path.exists( os.path.join(out_dir,folder)) :
                createFolder(os.path.join(out_dir,folder))

    else:
        
        if ALLOW_ONE_IMAGE_FILE:
            src_file = os.path.join(images_dir,filename) 
            dst_file = os.path.join(out_dir,folder,filename)
            
            if os.path.isfile(src_file):
                if PLATETYPE_MOVE:
                    shutil.move(src_file,dst_file)
                else:
                    shutil.copy(src_file,dst_file) 
                
                cur_folder_cap = (cur_folder_cap + 1) % MAX_FOLDER_CAP
                if cur_folder_cap == 0:  #폴더를 더 만든다.
                    folder_index += 1
                    folder ='{0:03}'.format(folder_index)
                    if not os.path.exists( os.path.join(out_dir,folder)) :
                        createFolder(os.path.join(out_dir,folder))
            
        continue
                   
    
    
print('작업이 종료되었습니다.')