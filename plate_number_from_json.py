import os,sys,shutil
import argparse
import pandas as pd
from label_tools import *
#------------------------------
#이 파일은 json으로 부터 번호판 이름을 읽어서 그이름을 표시한다.
# 파일 이름을 잘 읽는지 테스트 하는 프로그램이다.
# 수정할 내용


IMAGE_FOLDER_NAME = 'images' #이미지 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'images' #'annots' #json 폴더가 있는 경로
OUTPUT_FOLDER_NAME = 'out' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
DEFAULT_LABEL_FILE = "./LPR_Labels1.txt"  #라벨 파일이름
EXCEPTION_FOLDER_NAME = 'exception'  #예외로 뽑아낼 폴더
MIDDLE_PATH =  os.path.join('dataset','labels')
CROP_MARGIN = 0
#------------------------------



ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)

DEFAULT_OUPUT_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,OUTPUT_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="extract plate numbers from json directory")
parser.add_argument("-l",
                    "--labelfile",
                    help="Label file where the text files are stored.",
                    type=str,default=DEFAULT_LABEL_FILE)
parser.add_argument("-j",
                    "--json_dir",
                    help="Complete Path to the file where the input .json files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)

# 출력 디렉토리를 설정합니다.
parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)

args = parser.parse_args()


#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)
    
if not os.path.exists(args.json_dir) :
    print("No json folder exists. check the folder :",args.json_dir)
    sys.exit(0)

if not os.path.exists(args.output_path) :
    createFolder(args.output_path)
    
fLabels = pd.read_csv(args.labelfile, header = None )
LABEL_FILE_CLASS = fLabels[0].values.tolist()
HUMAN_NAMES= dict(zip(LABEL_FILE_CLASS, fLabels[1].values.tolist()))

json_data = OrderedDict()




# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.json_dir):

    json_ext = 'json'
    file_names = [fn for fn in os.listdir(args.json_dir)
                  if any(fn.endswith(ext) for ext in json_ext)]
   
    process_num = len(file_names)
    
    print("Total process file count is {0}".format(process_num))
    
    for filename in file_names :

        print("Processing : {0}".format(filename))
        
        #json  파일을 연다.
        try:
            
            with open(os.path.join(args.json_dir,filename), 'r',encoding="UTF-8") as f:
                json_data = json.load(f)
                
        except IOError:
                print("Error: File does not appear to exist")
                continue
            
        plateName = GetPlateNameFromJson(json_data = json_data, enlabel = LABEL_FILE_CLASS, human_dic = HUMAN_NAMES )
        
        print('번호판:{}'.format(plateName))
            
         
 
else :
    print("Error! no json directory:",args.json_dir)      