# 전체 영상에서 차선에 있는 차량만 분리하는 스크립이다.
# 한 화면에 여러개의 차량이 있는 경우 1대 만을 잘라낸다. 이것이 기존 extract_crop_vehicle.py와의 차이점이다.
# 1차로, 2차로 , 3차로 입력을 통하여 해당 차로의 영상 및 json을 분리한다.
# 소스에는 jpg 및 json이 있어야 한다.

import os, shutil
import numpy as np
import sys, random
import json
import cv2
import matplotlib.pyplot as plt
import math
from PIL import Image
from label_tools import *
#---------------------------------------------------------
CROP_XSIZE = 1600       # crop 영상 싸이즈
CROP_YSIZE = 1600
base_dir = os.path.abspath(os.path.dirname(__file__))
database_dir  =  os.path.join(base_dir,'dataset')
labels_dir = os.path.join(database_dir,'test')
annots_dir = os.path.join(labels_dir,'images')
images_dir = os.path.join(labels_dir,'images')
abnormal_dir = os.path.join(labels_dir,'abnormal')
out_dir = os.path.join(labels_dir,'result')

#찾아야 할 대상
vehicle_list = ['car', 'truck','bus', 'motorcycle','bicycle']
#vehicle_list = ['motorcycle']

FrontSystem = False      #전면인지 아닌지
#검지하고자하는 영상에서의 비율 [y1,x1,y2,x2]
if FrontSystem :
    laneBoxRatio = [[0,0,0,0],[0.0,0.4,0.65,1],[0.0,0.0,0.65,0.6]]
else:
    laneBoxRatio = [[0.13,0.0,0.97,0.61],[0.13,0.34,0.97,0.99],[0.3,0.4,0.9,1]]

SUFFIX = '_crop'
#SUFFIX = ''
#--------------------------------------------------------

if not os.path.exists(out_dir) :
    os.makedirs(out_dir)
    
if not os.path.exists(abnormal_dir) :
    os.makedirs(abnormal_dir)

image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
imagefile_list = [fn for fn in os.listdir(images_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]

total_files_count = len(imagefile_list)

print('총 처리할 영상은 {} 개 입니다.'.format(total_files_count))

cbox = []

for filename in imagefile_list:
    print('processing: {}'.format(filename))
    #먼저 json 파일을 읽는다.
    #이 영상이 몇번째 차로를 찍었는지 확인한다.
    #차로에 해당 영상의 번호판이 있으면 crop을 한다.
    basefilename, ext = os.path.splitext(filename)
    jsonfilename = basefilename +'.json'
    new_jsonfilename = basefilename + SUFFIX  +'.json'
    new_imagefilename = basefilename + SUFFIX + ext
    
    vehicle_box_list = []
    if os.path.isfile(os.path.join(annots_dir,jsonfilename)):
        #json 파일을 읽는다.
        searchlane = 0
        if '_L1_' or '_1_' in filename:
            searchlane = 0
        elif '_L2_' or '_2_' in filename:
            searchlane = 1
        elif '_L3_' or '_3_' in filename:
            searchlane = 2
        else:
            print('파일이름에서 차선을 찾을수 없습니다: {}'.format(filename))
            continue
        
        
        
        with open(os.path.join(annots_dir,jsonfilename), 'r',encoding="UTF-8") as f:
            json_data = json.load(f)
            
            image_width = int (json_data['imageWidth'])
            image_height = int (json_data['imageHeight'])
            #이미지의 중심을 구한다.
            image_cx  = image_width / 2
            image_cy = image_height / 2
            image_center =  [image_cx,image_cy]
            tvehicle_box = None
            for item, shape in enumerate(json_data['shapes']):
                label = shape['label']
                if label in vehicle_list :
                    #[x,y] point 형식으로 받는다.
                    points = np.array(shape['points']).astype(int) # numpy로 변형
                    shape_type = shape['shape_type']
                    if shape_type == 'rectangle':
                        points = box2polygon(points) #test point를 polygon으로 만든다.
                    
                    box = np.array(Polygon2Box(points)).astype(int) 
                    
                    vehicle_box_list.append(box)
            #차량의 박스를 찾으면 어느것이 중심에서 가장 가까운지 찾는다.
            
            if len(vehicle_box_list) :

                min_dist = math.dist([0,0], image_center)
                min_ix = 0
                for ix, box in enumerate(vehicle_box_list):
                    b_xmin, b_xmax, b_ymin, b_ymax = BoxPoints(box)
                    b_cx,b_cy = Box2Center(box)                    
                    #박스의 중점을 구해서 영상의 중점과 가장 가까운 것을 찾는다.
                    dist = math.dist([b_cx,b_cy], image_center)
                    
                    if dist < min_dist :
                        min_ix = ix;
                        min_dist = dist
                #이제 가장 가운데 있는 차량의 박스를 찾았다.    tvehicle_box: target vehicle box
                tvehicle_box  = vehicle_box_list[min_ix]
                # 해당 박스만 삭제한다.
                vehicle_box_list = [i for i in vehicle_box_list if i not in tvehicle_box]
            
            else:  # 영상에서 차량을 발견하지 못하면... 
                continue
            #box is [y1,x1,y2,x2] 형식
            cx,cy = Box2Center(tvehicle_box)
            t_xmin, t_xmax, t_ymin, t_ymax = BoxPoints(tvehicle_box)
            image_bound_xmargin = min(t_xmin, image_width - t_xmax)
            image_bound_ymargin = min(t_ymin, image_height - t_ymax)
            crop_margin_xmax = CROP_XSIZE 
            crop_margin_ymax = CROP_YSIZE 
            #박스는 무조건 tvehicle_box 밖에 있어야 한다. 안에 있으면 다음 영상을 체크한다.
            
            overlappedBox = False  #박스가 겹치는게 있는지 여부
            left_valid = True
            right_valid = True
            top_valid = True
            bottom_valid = True
            
            for box in vehicle_box_list:
                b_xmin, b_xmax, b_ymin, b_ymax = BoxPoints(box)
                
                iou ,a1, a2 ,intsec = IoU2(box, tvehicle_box)
                
                if iou > 0.0 :
                    overlappedBox = True
                    break;
                
                # x 값 계산.
                if b_xmax <= t_xmin and not ( b_ymax < t_ymin or b_ymin > t_ymax) : 
                    crop_margin_xmax =  min(crop_margin_xmax, t_xmin - b_xmax - 1) # b_xmax가 포함되면 안되므로 -1을 더 뺀다.
                    left_valid = False
                if t_xmax <=  b_xmin and not ( b_ymax < t_ymin or b_ymin > t_ymax) : 
                    crop_margin_xmax =  min(crop_margin_xmax,b_xmin - t_xmax -1)
                    right_valid = False
                if b_ymax <= t_ymin and not ( b_xmax < t_xmin or b_xmin > t_xmax) : 
                    crop_margin_ymax =  min(crop_margin_ymax, t_ymin - b_ymax -1)
                    top_valid = False
                if t_ymax <=  b_ymin and not ( b_xmax < t_xmin or b_xmin > t_xmax) : 
                    crop_margin_ymax =  min(crop_margin_ymax, b_ymin - t_ymax -1)
                    bottom_valid = False
                if(b_xmax < t_xmin and b_ymax < t_ymin) :
                    crop_margin_xmax =  min(crop_margin_xmax, t_xmin - b_xmax - 1)
                    crop_margin_ymax =  min(crop_margin_ymax, t_ymin - b_ymax - 1)
                    left_valid = False
                    top_valid = False
                if(b_xmin > t_xmax and b_ymax < t_ymin) :
                    crop_margin_xmax =  min(crop_margin_xmax, b_xmin - t_xmax -1)
                    crop_margin_ymax =  min(crop_margin_ymax, t_ymin - b_ymax -1)
                    right_valid = False
                    top_valid = False
                if(b_xmax < t_xmin and b_ymin > t_ymax) :
                    crop_margin_xmax =  min(crop_margin_xmax, t_xmin - b_xmax -1)
                    crop_margin_ymax =  min(crop_margin_ymax, b_ymin - t_ymax -1)
                    left_valid = False
                    bottom_valid = False
                if(b_xmin > t_xmax and b_ymin > t_ymax) :
                    crop_margin_xmax =  min(crop_margin_xmax, b_xmin - t_xmax -1)
                    crop_margin_ymax =  min(crop_margin_ymax, b_ymin - t_ymax -1)
                    right_valid = False
                    bottom_valid = False
            
            if overlappedBox :
                print('차량이 곕침 중복됩니다.: {}'.format(filename))
                continue
            
            #이제 최대 gap을 구한다.
            #crop_margin_xmax = max(crop_margin_xmax,image_bound_xmargin)
            #crop_margin_ymax = max(crop_margin_ymax, image_bound_ymargin )
            crop_margin = min(crop_margin_xmax,crop_margin_ymax)
            #이 값을 토대로 최대 박스 크기를 계산한다.
            #crop 시작 위치이다.
            crop_sx = None
            crop_sy = None
            #crop 종료 위치이다.
            crop_ex = None
            crop_ey = None
            #차량의 넓이 높이 이다.
            crop_width = min(CROP_XSIZE,(t_xmax - t_xmin + 1) + 2*crop_margin)
            crop_height = min(CROP_YSIZE,(t_ymax - t_ymin + 1) + 2*crop_margin)
            crop_size  = min(crop_width,crop_height) 
            
            HCROP_XSIZE = CROP_XSIZE / 2.0
            HCROP_YSIZE = CROP_YSIZE / 2.0
            
            
            
            if left_valid:
                add = HCROP_XSIZE
                radd = max(cx + HCROP_XSIZE - image_width, 0) # 이미지 크기를 넘어가는 부분
                if right_valid:
                   add += radd 
                crop_sx = min(cx - add, t_xmin)
            else :
                crop_sx = max(t_xmin - crop_margin, cx - HCROP_XSIZE)
            if top_valid :
                add = HCROP_YSIZE
                badd = max(cy + HCROP_YSIZE - image_height, 0)
                if bottom_valid:
                    add += badd
                crop_sy = min(cy - add,t_ymin)
            else:    
                crop_sy = max(t_ymin - crop_margin, cy - HCROP_YSIZE)
            if right_valid:
                add = HCROP_XSIZE
                ladd = min( cx - HCROP_XSIZE, 0)
                if left_valid  :
                    add += -ladd
                crop_ex = max(cx  + add, t_xmax)
            else:
                crop_ex = min(t_xmax + crop_margin, cx + HCROP_XSIZE)
            if bottom_valid:
                add = HCROP_YSIZE
                tadd = min( cy - HCROP_YSIZE, 0)
                if top_valid:
                    add += -tadd
                crop_ey = max(cy + add, t_ymax)
            else:
                crop_ey = min(t_ymax + crop_margin, cy + HCROP_YSIZE)
            
            if crop_sx < 0 :
               crop_sx = 0
            if crop_sy < 0 :
                crop_sy = 0
            if crop_ex > image_width - 1:
                crop_ex = image_width - 1           
            if crop_ey > (image_height -1) :
                crop_ey = image_height - 1
            
            crop_width = crop_ex - crop_sx
            crop_height = crop_ey - crop_sy
            # crop 안에 있는 모든 객체를 저장한다.
            
            cropbox = np.array([[ crop_sx, crop_sy],[ crop_ex, crop_ey]])
            cropbox_polygon = box2polygon(cropbox)
            
            val = np.array([crop_sx, crop_sy])
            
            new_shapes = [] 
            for item, shape in enumerate(json_data['shapes']):
                points = np.array(shape['points']).astype(int) # numpy로 변형
                shape_type = shape['shape_type']
                label = shape['label']
                
                # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                tpoints = []
                if shape_type == 'rectangle':
                    tpoints = box2polygon(points) #test point를 polygon으로 만든다.
                else:
                    tpoints = points
                
                check_overlab = PolygonOverlab(tpoints,cropbox_polygon)
                
                if check_overlab :
                    points = SupressInBox(points, cropbox) #box 범위 안으로 points를 제한 한다.
                    points = points - val + 1
                    
                    shape['points'] = list(points) # 값을 업데이트 한다.
                    new_shapes.append(shape)

            #이미지의 크기를 리싸이즈한 크기로 변경한다.
            json_data['shapes'] = new_shapes
            json_data['version'] = '5.0.1'
            json_data['flags'] = {}
            json_data['imageWidth'] = crop_width
            json_data['imageHeight'] = crop_height
            json_data['imagePath'] = new_imagefilename
            #추가 내용을 저장하다. 
            json_data['parentImageWidth'] = image_width
            json_data['parentImageHeight'] = image_height
            json_data['parentImageName'] = filename
            json_data['imageData'] = None
            
            #json 파일로 저장한다.
            try:
                with open(os.path.join(out_dir,new_jsonfilename),'w', encoding='utf-8') as f:
                    json.dump(json_data,f,ensure_ascii=False,indent="\t", cls=NpEncoder)
                    
            except IOError:
                print("Error: json write error")
                continue
             #영상 파일을 연다.
            try:
                    imgRGB  = imread(os.path.join(images_dir,filename))
                    cropped_img = imgRGB[int(crop_sy):int(crop_ey+1), int(crop_sx):int(crop_ex+1)]
                    imwrite(os.path.join(out_dir, new_imagefilename),cropped_img)
                    # img = Image.open(os.path.join(images_dir,filename))
                    # area = (crop_sx, crop_sy, crop_ex, crop_ey)
                    # cropped_img = img.crop(area)
                    # cropped_img.save( os.path.join(out_dir, new_imagefilename))

            except IOError:
                print("Error: open jpeg imaeg error {0}".format(filename))
                continue
    
print('작업이 완료 되었습니다.')