# -*- coding: utf-8 -*-
"""
Created on Thu June 19 2024
json과 영상을 읽어서 Mask 영역을 제외한 json 파일과 영상으로 변환하는 기능을 한다.
@author: 김지수
"""

import os, sys
import json
from shapely.geometry import Polygon
import shutil
from PIL import Image, ImageDraw
from pathlib import Path

#---------------------
# 수정할 내용

INPUT_FOLDER_NAME = "D:\SPB_Data\labelconv\dataset\yolo\images" # image와 json 파일이 있는 폴더 경로
OUTPUT_FOLDER_NAME = "D:\SPB_Data\labelconv\dataset\yolo\images1" # 결과를 저장할 폴더
# MASK JSON 파일 경로
json_file_path = r'D:\SPB_Data\labelconv\dataset\yolo\mask'
json_file_path = os.path.normpath(json_file_path)
json_file_path = Path(json_file_path)

json_ext = ['json']
json_mask_file_names = [fn for fn in os.listdir(json_file_path)
                  if any(fn.endswith(ext) for ext in json_ext)]


if len(json_mask_file_names) == 0:
    sys.exit(0)
    print("Error : mask json 폴더에 json 파일이 없음 : {}".format(json_file_path))

# JSON 파일 읽기
with open(os.path.join(json_file_path,json_mask_file_names[0]), "r") as file:
    data = json.load(file)

# "mask" 객체의 points 추출
MASK_POINTS = []
for shape in data.get("shapes", []):
    if shape.get("label") == "mask":
        MASK_POINTS = [tuple(point) for point in shape.get("points", [])]
        break
    


# 마스킹영역의 좌표값 (튜플 형식으로 주기)
""""
MASK_POINTS = [
        (
          529.1509433962265,
          109.76415094339626
        ),
        (
          534.3396226415094,
          175.33018867924528
        ),
        (
          590.4716981132076,
          174.85849056603774
        ),
        (
          585.7547169811321,
          109.76415094339626
        )
      ]
"""
IOU_standard = 0.5 # iou 기준값
IMAGE_FILE_COPY = True #이미지 파일 복사 여부

# (참고) 코드진행 중 에러가 생기는 이유: 각 polygon pts 형식이 같아야한다. 따라서 에러가 생길 경우 pts 형식이 같은지 보기

#----------------------

# 겹치는 영역을 계산
def calculate_intersectoin(polygon1, polygon2):
    intersection = polygon1.intersection(polygon2)
    iou = intersection.area / polygon1.area
    return iou

# json 파일에 Mask 좌표 추가 및 Mask 영역 반영하여 기존 좌표 편집
def process_json_file(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        data = json.load(file)

    shapes = data.get('shapes', [])
    filtered_shapes = []

    for shape in shapes:
        if shape['shape_type'] == 'polygon':
            points = shape['points']
            polygon = Polygon(points)
            iou = calculate_intersectoin(polygon, MASK_POLYGON)

            if iou >= IOU_standard: 
                filtered_shapes.append(shape)
        elif shape['shape_type'] == 'rectangle':
            points = shape['points']
            polygon = Polygon([
                (points[0][0], points[0][1]),
                (points[1][0], points[0][1]),
                (points[1][0], points[1][1]),
                (points[0][0], points[1][1])
            ])
            iou = calculate_intersectoin(polygon, MASK_POLYGON)

            if iou >= IOU_standard:  
                filtered_shapes.append(shape)

    data['shapes'] = filtered_shapes

    output_file_path = os.path.join(OUTPUT_FOLDER_NAME, os.path.basename(file_path))
    with open(output_file_path, 'w', encoding='utf-8') as file:
        json.dump(data, file, ensure_ascii=False, indent=4)

    print(f"{file_path} 처리 완료")


def main():
    if not os.path.exists(OUTPUT_FOLDER_NAME):
        os.makedirs(OUTPUT_FOLDER_NAME)

    json_files = [f for f in os.listdir(INPUT_FOLDER_NAME) if f.endswith('.json')]
    for json_file in json_files:
        process_json_file(os.path.join(INPUT_FOLDER_NAME, json_file))
        
        if IMAGE_FILE_COPY: # image 파일 복사
            base_name = os.path.splitext(json_file)[0]
            possible_extensions = ['.jpg', '.jpeg', '.png']
            
            for ext in possible_extensions:
                image_file = base_name + ext
                image_file_path = os.path.join(INPUT_FOLDER_NAME, image_file)

                if os.path.exists(image_file_path):
                    # MASKPOINTS로 이루어진 다각형영역을 제외한 부분 검은색으로 칠하기
                    image = Image.open(image_file_path)
                    mask = Image.new('L', image.size, 0)
                    draw = ImageDraw.Draw(mask)
                    draw.polygon(MASK_POINTS, fill=255)

                    output_image = Image.composite(image, Image.new('RGB', image.size, (0, 0, 0)), mask)
                    
                    output_image_path = os.path.join(OUTPUT_FOLDER_NAME, image_file)
                    output_image.save(output_image_path)
                    print(f"{image_file_path} 처리 완료")
                    break
            else:
                print(f"{base_name}에 해당하는 이미지 파일을 찾을 수 없습니다.")


if __name__ == '__main__':
    if len(MASK_POINTS) < 3 :
        print("Error : Mask Region requires at least 3 points")
    else:
        MASK_POLYGON = Polygon(MASK_POINTS)
        main()
