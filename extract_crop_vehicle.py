# 전체 영상에서 차선에 있는 차량만 분리하는 스크립이다.
# 1차로, 2차로 , 3차로 입력을 통하여 해당 차로의 영상 및 json을 분리한다.
# 소스에는 jpg 및 json이 있어야 한다.

import os, shutil
import numpy as np
import sys, random
import json
import cv2
import matplotlib.pyplot as plt
from label_tools import *
#---------------------------------------------------------
CROP_XSIZE = 1600       # crop 영상 싸이즈
CROP_YSIZE = 1600
base_dir = os.path.abspath(os.path.dirname(__file__))
database_dir  =  os.path.join(base_dir,'dataset')
labels_dir = os.path.join(database_dir,'test')
annots_dir = os.path.join(labels_dir,'images')
images_dir = os.path.join(labels_dir,'images')
abnormal_dir = os.path.join(labels_dir,'abnormal')
out_dir = os.path.join(labels_dir,'result')

FrontSystem = False      #전민인지 아닌지
#검지하고자하는 영상에서의 비율 [y1,x1,y2,x2]
if FrontSystem :
    laneBoxRatio = [[0,0,0,0],[0.0,0.4,0.65,1],[0.0,0.0,0.65,0.6]]
else:
    laneBoxRatio = [[0,0,0,0],[0.3,0,0.9,0.6],[0.3,0.4,0.9,1]]

SUFFIX = '_crop'
#--------------------------------------------------------

if not os.path.exists(out_dir) :
    os.makedirs(out_dir)
    
if not os.path.exists(abnormal_dir) :
    os.makedirs(abnormal_dir)

image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
imagefile_list = [fn for fn in os.listdir(images_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]

total_files_count = len(imagefile_list)

print('총 처리할 영상은 {} 개 입니다.'.format(total_files_count))

cbox = []

for filename in imagefile_list:
    print('processing: {}'.format(filename))
    #먼저 json 파일을 읽는다.
    #이 영상이 몇번째 차로를 찍었는지 확인한다.
    #차로에 해당 영상의 번호판이 있으면 crop을 한다.
    basefilename, ext = os.path.splitext(filename)
    jsonfilename = basefilename +'.json'
    new_jsonfilename = basefilename + SUFFIX  +'.json'
    new_imagefilename = basefilename + SUFFIX + ext
    if os.path.isfile(os.path.join(annots_dir,jsonfilename)):
        #json 파일을 읽는다.
        searchlane = 0
        if '_L1_' in filename:
            searchlane = 1
        elif '_L2_' in filename:
            searchlane = 2
        else:
            print('파일이름에서 차선을 찾을수 없습니다: {}'.format(filename))
            continue
        
        
        
        with open(os.path.join(annots_dir,jsonfilename), 'r',encoding="UTF-8") as f:
            json_data = json.load(f)
            
            image_width = int (json_data['imageWidth'])
            image_height = int (json_data['imageHeight'])
            
            [ry1,rx1,ry2,rx2] = laneBoxRatio[searchlane]
            lain_x1 = image_width * rx1
            lain_y1 = image_height * ry1
            lain_x2 = image_width * rx2
            lain_y2 = image_height * ry2
            
            lbox = [lain_y1,lain_x1,lain_y2,lain_x2]
            
            new_shapes = []
            find_plate_in_ROI = False  #관심지역에서 번호판을 찾았는가?
            for item, shape in enumerate(json_data['shapes']):
                label = shape['label']
                # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                points = np.array(shape['points']).astype(int) # numpy로 변형
                shape_type = shape['shape_type']
                if shape_type == 'rectangle':
                    points = box2polygon(points) #test point를 polygon으로 만든다.
                    
                box = np.array(Polygon2Box(points)).astype(int) 
                
                if isInside(box,lbox) :
                    
                    #특별히 번호판이면 번호판을 기준으로 영상을 잘라낸다.
                    if 'type' in label:
                        #번호판을 찾았다.
                        find_plate_in_ROI = True
                        pBox = Polygon2Box(points)
                        #번호판의 센터 좌표를 찾는다.
                        cpoint = Box2Center(pBox)
                        
                        #중점이 차선영역에 있는지 확인한다.
                        if isPointInsideBox(cpoint,lbox) :
                            
                            # 중간
                            c_x1 = int(cpoint[0] - CROP_XSIZE / 2)
                            c_x2 = int(cpoint[0] + CROP_XSIZE / 2)
                            
                            if c_x1 < 0 :
                                c_x2 += -c_x1
                                c_x1 = 0
                            if c_x2 > image_width - 1:
                                c_x1 -= (c_x2 - image_width)
                                c_x1 = max(c_x1, 0)
                                c_x2 = image_width - 1
                            
                            c_y1 = int(cpoint[1] - (CROP_YSIZE *2)/3)
                            c_y2 = int(cpoint[1] + (CROP_YSIZE *1)/3)
                            
                            if c_y1 < 0 :
                                c_y2 += -c_y1
                                c_y1 = 0
                            if c_y2 > (image_height -1) :
                                c_y1 -= (c_y2 - image_height)
                                c_y1 = max(c_y1,0)
                                c_y2 = image_height - 1
                            cbox = [c_y1, c_x1, c_y2, c_x2]
                            #영상을 읽는다.
                            img_np = imread(os.path.join(images_dir,filename))
                            #이미지 리사이즈
                            cropped_imag_np = img_np[c_y1:c_y2,c_x1:c_x2,:]
                            #자른 이미지를 저장한다.
                            imwrite(os.path.join(out_dir,new_imagefilename),cropped_imag_np)
                            break
                            #프린트
                            #plt.imshow(cropped_imag_np)
                            #plt.show()
            
            if not find_plate_in_ROI:
                continue
            
            #다시 crop 된 박스를 기준으로 json을 만든다.
            new_shapes = []
            for item, shape in enumerate(json_data['shapes']):

                label = shape['label']
                # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                points = np.array(shape['points']).astype(int) # numpy로 변형
                shape_type = shape['shape_type']
                if shape_type == 'rectangle':
                    points = box2polygon(points) #test point를 polygon으로 만든다.
                    
                box = np.array(Polygon2Box(points)).astype(int) 
                
                iou, box1_area, box2_area,inter = IoU2(box,cbox)
                if iou > 0 :
                            
                    cboxpoints = [[cbox[1],cbox[0]],[cbox[3],cbox[0]],[cbox[3],cbox[2]],[cbox[1],cbox[2]]]
                    cboxpoints = np.array(cboxpoints).astype(int) 
                    points = SupressInBox(points, cboxpoints)
                    points[:,1] = points[:,1] - cbox[0]
                    points[:,0] = points[:,0] - cbox[1]
                    shape["shape_type"] = 'polygon'
                    shape['points'] = list(points)
                    new_shapes.append(shape)

            
        #이미지의 크기를 리싸이즈한 크기로 변경한다.
        json_data['shapes'] = new_shapes
        json_data['version'] = '5.0.1'
        json_data['flags'] = {}
        json_data['imageWidth'] = CROP_XSIZE
        json_data['imageHeight'] = CROP_YSIZE
        json_data['imagePath'] = new_imagefilename
        #추가 내용을 저장하다. 
        json_data['parentImageWidth'] = image_width
        json_data['parentImageHeight'] = image_height
        json_data['parentImageName'] = filename
        json_data['imageData'] = None

        #json 파일로 저장한다.
        try:
            with open(os.path.join(out_dir,new_jsonfilename),'w', encoding='utf-8') as f:
                json.dump(json_data,f,ensure_ascii=False,indent="\t", cls=NpEncoder)
                
        except IOError:
            print("Error: json write error")
            continue
    
print('작업이 완료 되었습니다.')