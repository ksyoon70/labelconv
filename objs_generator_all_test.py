#모든 type의 이미지를 생성한다.
import os,sys
import subprocess

ObjectType = ['vr','or']
python = 'c:/venvs/object_detection_api/Scripts/python.exe'
CHAR_DET_FOLDER = 'C://SPB_Data//chardet//datasets'  #복사할 폴더의 디렉토리 이름 
#복사가 잘 안되는 문제가 있다.

for type in ObjectType :
    if type == 'ch':        #문자 검사
        OUTPUT_FOLDER_NAME = 'ch_images'
    elif type == 'n':       #숫자검사
        OUTPUT_FOLDER_NAME = 'n_images'
        print("{0} type is Not supporeted yet".format(type))
        sys.exit(0)
    elif type == 'r':       #지역문자 검사
        OUTPUT_FOLDER_NAME = 'r_images'
    elif type == 'vr':       #v 지역문자 검사
        OUTPUT_FOLDER_NAME = 'vr_images'
    elif type == 'hr':       #h 지역문자 검사
        OUTPUT_FOLDER_NAME = 'hr_images'
    elif type == 'or':       #o 지역문자 검사
        OUTPUT_FOLDER_NAME = 'or_images'
    elif type == 'r6':       #6 지역문자 검사
        OUTPUT_FOLDER_NAME = 'r6_images'      
    else:
        print("{0} type is Not supporeted".format(type))
        sys.exit(0)  
    param1 = '-t{}'.format(type)
    OUPUT_PATH = os.path.join(CHAR_DET_FOLDER,OUTPUT_FOLDER_NAME)
    param2 ='-o{}'.format(OUPUT_PATH)
    p = subprocess.Popen([python,"objs_generator.py", param1,param2], shell=True)
    (output, err) = p.communicate()
    p_status = p.wait()
    #print ("Command output: {}\n".format(output) )
