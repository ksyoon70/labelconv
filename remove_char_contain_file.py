"""
Created on Tue Mar 15 14:19:22 2022
특정 단어를 포함한 파일을 옮기는 기능을 한다.
example
'c_' 를 포함하면 파일을 이동시킨다.
20220805_083902_1_13루2878c_13루2878.jpg.json
20220805_083902_1_13루2878c_13루2878.jpg.jpg


@author:  윤경섭
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:05:28 2022

@author: 윤경섭
"""

from distutils.file_util import move_file
import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile,move
from label_tools import insertlabel_with_points
from label_tools import NpEncoder

#------------------------------
# 수정할 내용

OUTPUT_FOLDER_NAME = 'result' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
IMAGE_FOLDER_NAME = 'images' #csv 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'images' #json 폴더가 있는 경로
MIDDLE_PATH = os.path.join('dataset','labels') #'dataset' #os.path.join('dataset','test')
FILE_COPY = False  #이미지 파일 복사 여부
ADD_RECOGNITION_RESULT = True #인식 결과를 파일에 붙일지 여부
FIND_STR = 'c_'         #찾으려는 문자열.
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)
DEFAULT_OUPUT_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,OUTPUT_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="특정 문자가 있는 파일을 이동시킨다.")
parser.add_argument("-j",
                    "--json_dir",
                    help="Complete Path to the file where the input .csv files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)


args = parser.parse_args()

# 디렉토리 생성
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)
    
if not os.path.exists(args.json_dir) :
    print("No json folder exists. check the folder :",args.json_dir)
    sys.exit(0)

if not os.path.exists(args.output_path) :
    createFolder(args.output_path)

json_data = OrderedDict()

ofilename = ""
# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.json_dir):

    json_ext = ['json','jpg','JPG','png','PNG','jpeg','JPEG']
    file_names = [fn for fn in os.listdir(args.json_dir)
                  if any(fn.endswith(ext) for ext in json_ext)]
   
    process_num = len(file_names)
    
    print("Total process file count is {0}".format(process_num))
    
    for filename in file_names :

        #print("processing : {0}".format(filename))
        
        if filename.find(FIND_STR) != -1 :
            src = os.path.join(args.image_dir,filename)
            dst = os.path.join(args.output_path,filename)
            if os.path.exists(src):                
                if FILE_COPY : # 파일을 result 디렉토리에 복사한다.
                    print("파일복사 : {0}".format(filename))
                    copyfile(src,dst)
                else:
                    print("파일이동 : {0}".format(filename))
                    move(src,dst)
            else:
                print("Error! no image file:",filename)
   
else :
    print("Error! no json directory:",args.json_dir)            


