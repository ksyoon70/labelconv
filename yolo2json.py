# -*- coding: utf-8 -*-
"""
Created on Thu Oct 13 09:42:39 2022
yolo 포멧을 읽어서 json으로 바꾸는 역할을 한다.
@author: headway
"""

import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile, move
from label_tools import *
from label_tools import NpEncoder
from JsonMng import *
from pathlib import Path
#------------------------------
# 수정할 내용

src_image_dir = r'E:\SPB_Data\labelconv\dataset\yolo\images' #image 파일에 있는 영상 파일이 있는 경로
src_image_dir = os.path.normpath(src_image_dir)
src_image_dir = Path(src_image_dir)

src_label_dir = r'E:\SPB_Data\labelconv\dataset\yolo\images' #txt 폴더가 있는 경로
src_label_dir = os.path.normpath(src_label_dir)
src_label_dir = Path(src_label_dir)

dst_dir = r'E:\SPB_Data\labelconv\dataset\yolo\images1' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
dst_dir = os.path.normpath(dst_dir)
dst_dir = Path(dst_dir)


#OUTPUT_FOLDER_NAME = 'images1' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
#IMAGE_FOLDER_NAME = 'images' #image 파일에 있는 영상 파일이 있는 경로
#YOLO_FOLDER_NAME = 'images' #txt 폴더가 있는 경로
#MIDDLE_PATH = os.path.join('dataset','yolo') #'dataset' #os.path.join('dataset','test')
IMAGE_FILE_COPY = True #이미지 파일 복사 여부
ADD_RECOGNITION_RESULT = True #인식 결과를 파일에 붙일지 여부
#DEFAULT_LABEL_FILE = "./LPR_Car-Plate_Labels.txt"  #라벨 파일이름
DEFAULT_LABEL_FILE =  "./tracking_yolo_labels.txt"
MOVE_FILE = True
YOLO_TAG_DELETE = True #yolo txt file 지울지 여부
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = src_image_dir
DEFAULT_JSON_PATH = src_label_dir
DEFAULT_OUPUT_PATH = dst_dir

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="json-to-yolo label converter")
parser.add_argument("-l",
                    "--labelfile",
                    help="Label file where the text files are stored.",
                    type=str,default=DEFAULT_LABEL_FILE)
parser.add_argument("-y",
                    "--yolo_dir",
                    help="Complete Path to the file where the yolo label .txt files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)


args = parser.parse_args()

fLabels = pd.read_csv(args.labelfile, header = None )
CLASS_NAMES = fLabels[0].values.tolist()
HUMAN_NAMES= dict(zip(CLASS_NAMES, fLabels[1].values.tolist()))

#yolo 포멧으로 변경한다.
def yolo_convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)


# 디렉토리 생성
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)
    
if not os.path.exists(args.yolo_dir) :
    print("No yolo label folder exists. check the folder :",args.yolo_dir)
    sys.exit(0)

if not os.path.exists(args.output_path) :
    createFolder(args.output_path)


ofilename = ""
# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.image_dir):

    image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
    file_names = [fn for fn in os.listdir(args.image_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]
   
    process_num = len(file_names)
    
    print("Total process file count is {0}".format(process_num))
    
    for filename in file_names :

        print("processing : {0}".format(filename))
        
        readlines = None
        dw = None
        dh = None
        
        #json  파일을 연다.
        try:
            if not os.path.exists(os.path.join(args.image_dir,filename)) :
                print("Error! no image file: {}".format(filename))
                continue
            imgRGB  = imread(os.path.join(args.image_dir,filename))
            dh = imgRGB.shape[0]
            dw = imgRGB.shape[1]
            basename, ext = os.path.splitext(filename)
            yolo_filename = basename + '.txt'
            
            with open(os.path.join(args.yolo_dir,yolo_filename), 'r',encoding="UTF-8") as f:
                readlines = f.readlines()
                
        except IOError:
                if not os.path.exists(os.path.join(args.yolo_dir,yolo_filename)) :
                    print('Yolo label file not exit : {}'.format(yolo_filename))
                else:
                    print("Error: File does not appear to exist")
                continue
        
        json_dir = args.output_path    
        jsonMng = JsonMng(json_dir,imgRGB.shape,filename)
        for dt in readlines:
    
            # Split string to float
            c, x, y, w, h = map(float, dt.split(' ')) # c : category

            box_sx = int((x - w / 2) * dw)
            box_ex = int((x + w / 2) * dw)
            box_sy = int((y - h / 2) * dh)
            box_ey = int((y + h / 2) * dh)
            
            obj_box = [[box_sx, box_ex, box_ex, box_sx],[box_sy,box_sy,box_ey,box_ey]]
            category = HUMAN_NAMES[int(c)]
            jsonMng.addObject(box=obj_box, label = category)
         
        jsonMng.save()

        if YOLO_TAG_DELETE:
            os.remove(os.path.join(args.yolo_dir,yolo_filename))
        
        #이미지 파일을 복사한다.
        src = os.path.join(args.image_dir,filename)
        dst = os.path.join(args.output_path,filename)
        if os.path.exists(src):
            if MOVE_FILE:
                shutil.move(src,dst)
            else:    
                copyfile(src,dst)
            
            
          
else :
    print("Error! no json directory:",args.json_dir)