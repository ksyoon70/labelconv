"""
Created on 2022년 7월 21일
차량 영상 및 해당 json을 읽어 지역문자, 용도문자, 번호를 원하는 해상도로 저장하는 기능을 구현
예) python objs_generator -l ./LPR_Labels1.txt -i ./image -j ./annots -o ./result -r 225,225 -f False -t ch
-l label 파일
-i 이미지 위치
-j json 위치
-o 결과 파일 저장 위치
-r resize 만일 이것이 없으면 resize를 하지 않는다.
-f fixratio 영상 가로세로 비율을 고정할지 안할지 여부
-t type ch: 문자  n: number r:지역문자 vr:세로 지역문자 hr:가로지역문자 or: 영지역문자 r6: 6지역문자
@author:  윤경섭
"""

import os,sys
import argparse
import pandas as pd
import cv2
from label_tools import *
import matplotlib.pyplot as plt
#------------------------------
# 수정할 내용

OUTPUT_FOLDER_NAME = 'out' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
IMAGE_FOLDER_NAME = 'images' #이미지 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'images' #json annots 폴더가 있는 경로
MIDDLE_PATH =  os.path.join('dataset','labels')
DEFAULT_LABEL_FILE =  "./LPR_Total_Labels.txt"  #"./LPR_Labels1.txt"  #라벨 파일이름
CROP_MARGIN = 0
DEFULT_FIXED_RATIO=False         #영상의 크기를 싸이즈에 맞게 늘일지 여부 True : 고정 False : 늘림
DEFAULT_TYPE='vr' #읽어올 default 타입을 설정한다. ch: 문자  n: number r:지역문자 vr:세로 지역문자 hr:가로지역문자 or: 영지역문자 r6: 6지역문자

CHAR_DET_FOLDER = 'C://SPB_Data//chardet//datasets'  #복사할 폴더의 디렉토리 이름
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)

OBJTYPES = ['ch','hr','vr','or','r6']

for DEFAULT_TYPE in OBJTYPES :

    if DEFAULT_TYPE == 'ch':        #문자 검사
        OUTPUT_FOLDER_NAME = 'ch_images'
    elif DEFAULT_TYPE == 'n':       #숫자검사
        OUTPUT_FOLDER_NAME = 'n_images'
        print("{0} type is Not supporeted yet".format(args.object_type))
        sys.exit(0)
    elif DEFAULT_TYPE == 'r':       #지역문자 검사
        OUTPUT_FOLDER_NAME = 'r_images'
    elif DEFAULT_TYPE == 'vr':       #v 지역문자 검사
        OUTPUT_FOLDER_NAME = 'vr_images'
    elif DEFAULT_TYPE == 'hr':       #h 지역문자 검사
        OUTPUT_FOLDER_NAME = 'hr_images'
    elif DEFAULT_TYPE == 'or':       #o 지역문자 검사
        OUTPUT_FOLDER_NAME = 'or_images'
    elif DEFAULT_TYPE == 'r6':       #6 지역문자 검사
        OUTPUT_FOLDER_NAME = 'r6_images'      
    else:
        print("{0} type is Not supporeted".format(DEFAULT_TYPE))
        sys.exit(0)   

    DEFAULT_OUPUT_PATH = os.path.join(CHAR_DET_FOLDER,OUTPUT_FOLDER_NAME)

    # Initiate argument parser
    parser = argparse.ArgumentParser(
        description="object split and save in jpeg and annotation files")

    parser.add_argument("-l",
                        "--labelfile",
                        help="Label file where the text files are stored.",
                        type=str,default=DEFAULT_LABEL_FILE)

    parser.add_argument("-j",
                        "--json_dir",
                        help="Complete Path to the file where the input .json files are stored.",
                        type=str,default=DEFAULT_JSON_PATH)
    parser.add_argument("-i",
                        "--image_dir",
                        help="Path to the folder where the input image files are stored. ",
                        type=str, default=DEFAULT_IMAGES_PATH)
    # 이미지의 크기를 일정 크기로 변경한다.
    parser.add_argument('-r', '--resize', type=int, nargs=2,help='resize imagesize width, height',default=[224,224], required=False)

    # 이미지를 리싸이즈 할때 strech를 할지 결정한다.
    parser.add_argument('-f', '--fixratio', type=bool, help='fixed ratio or strech image option True or False',default=DEFULT_FIXED_RATIO)
    # 출력 디렉토리를 설정ㅎㄴ다.
    parser.add_argument("-o",
                        "--output_path",
                        help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)
    # 검색할 object type를 설정한다. 
    parser.add_argument("-t",
                        "--object_type",
                        help="object type ch : character n: number r: region", type=str,default=DEFAULT_TYPE)

    args = parser.parse_args()


    fLabels = pd.read_csv(args.labelfile, header = None )
    LABEL_FILE_CLASS = fLabels[0].values.tolist()
    HUMAN_NAMES= dict(zip(LABEL_FILE_CLASS, fLabels[1].values.tolist()))
    #클래스를 각각 그룹별로 나눈다.
    CH_CLASS =  LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('Ga'):LABEL_FILE_CLASS.index('Bae') + 1] #문자열 클래스
    NUM_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('n1'):LABEL_FILE_CLASS.index('n0') + 1]  #숫자 클래스
    REGION_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('vSeoul'):LABEL_FILE_CLASS.index('UlSan6') + 1] #지역문자 클래스
    VREGION_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('vSeoul'):LABEL_FILE_CLASS.index('vUlSan') + 1] #Vertical 지역문자 클래스
    HREGION_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('hSeoul'):LABEL_FILE_CLASS.index('hUlSan') + 1] #Horizontal 지역문자 클래스
    OREGION_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('OpSeoul'):LABEL_FILE_CLASS.index('OpUlSan') + 1] #Orange 지역문자 클래스
    REGION6_CLASS = LABEL_FILE_CLASS[LABEL_FILE_CLASS.index('Seoul6'):LABEL_FILE_CLASS.index('UlSan6') + 1] #6 지역문자 클래스

    if 'I' in CH_CLASS:
        print('{0} is in CH_CLASS'.format('I'))


    resize = []

    b_RESIZE = False
    RESIZE_IMAGE_WIDTH = 0
    RESIZE_IMAGE_HEIGHT = 0

    if args.resize[0] : 
        b_RESIZE = True

    for i in args.resize:
        resize.append(i)

    RESIZE_IMAGE_HEIGHT = resize[0]
    RESIZE_IMAGE_WIDTH = resize[1]


    class_label = [];

    if args.object_type == 'ch':        #문자 검사
        class_label = CH_CLASS
    elif args.object_type == 'n':       #숫자검사
        class_label = NUM_CLASS
        print("{0} type is Not supporeted yet".format(args.object_type))
        sys.exit(0)
    elif args.object_type == 'r':       #지역문자 검사
        class_label = REGION_CLASS
    elif args.object_type == 'vr':       #v 지역문자 검사
        class_label = VREGION_CLASS
    elif args.object_type == 'hr':       #h 지역문자 검사
        class_label = HREGION_CLASS
    elif args.object_type == 'or':       #o 지역문자 검사
        class_label = OREGION_CLASS
    elif args.object_type == 'r6':       #6 지역문자 검사
        class_label = REGION6_CLASS     
    else:
        print("{0} type is Not supporeted".format(args.object_type))
        sys.exit(0)
        

    #이미지 폴더가 있는지 확인한다.

    if not os.path.exists(args.image_dir) :
        print("No images folder exists. check the folder :",args.image_dir)
        sys.exit(0)
        
    if not os.path.exists(args.json_dir) :
        print("No json folder exists. check the folder :",args.json_dir)
        sys.exit(0)

    #복사하려는 대상이 있으면 일단 디렉토리를 삭제한다. 22.10.10
    if os.path.exists(args.output_path) :
        shutil.rmtree(args.output_path) 

    if not os.path.exists(args.output_path) :
        createFolder(args.output_path)
        


        
        
    json_data = OrderedDict()

    ofilename = ""



    # json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
    if os.path.exists(args.json_dir):

        json_ext = 'json'
        file_names = [fn for fn in os.listdir(args.json_dir)
                    if any(fn.endswith(ext) for ext in json_ext)]
    
        process_num = len(file_names)
        
        print("Total process file count is {0}".format(process_num))
        
        for filename in file_names :

            print("Processing : {0}".format(filename))
            
            #json  파일을 연다.
            try:
                
                with open(os.path.join(args.json_dir,filename), 'r',encoding="UTF-8") as f:
                    json_data = json.load(f)
                    
            except IOError:
                    print("Error: File does not appear to exist")
                    continue
                
                
            json_data['imageData'] = None
            
            basename, ext = os.path.splitext(filename)
            
            ofilename = basename
            
            #object의 시작 위치이다.
            crop_sx = None
            crop_sy = None
            #object 종료 위치이다.
            crop_ex = None
            crop_ey = None
            #object 넓이 높이 이다.
            crop_width = None
            crop_height = None
            
            crop_polygon = None
            
            find_object = False
            obj_name_ext = None
            
            image_width = int (json_data['imageWidth'])
            image_height = int (json_data['imageHeight'])  

            new_shapes = []
            
            ratio_h = 0;
            ratio_v = 0;

            for item, shape in enumerate(json_data['shapes']):
                label = shape['label']
                
                if label in class_label:
                    
                    find_object = True
                    obj_name_ext = HUMAN_NAMES[label]
                    points = np.array(shape['points']).astype(int) # numpy로 변형
                    shape_type = shape['shape_type']
                    
                    # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                    tpoints = []
                    if shape_type == 'rectangle':
                        tpoints = box2polygon(points) #test point를 polygon으로 만든다.
                    else:
                        tpoints = points
                        
                    #줄이기 전에 잘라낼 위치를 정한다.
                    crop_xs = points[:,0]
                    crop_ys = points[:,1]

                    crop_sx = np.min(crop_xs,axis=0) - CROP_MARGIN
                    if crop_sx < 0:
                        crop_sx = 0
                    crop_sy = np.min(crop_ys,axis=0) - CROP_MARGIN
                    if crop_sy < 0:
                        crop_sy = 0
                    crop_ex = np.max(crop_xs,axis=0)  + CROP_MARGIN 
                    if crop_ex >= image_width:
                        crop_ex = image_width - 1
                    crop_ey = np.max(crop_ys,axis=0)  + CROP_MARGIN
                    if crop_ey >= image_height:
                        cropey = image_height - 1
                    crop_width =  crop_ex - crop_sx
                    crop_height = crop_ey - crop_sy

                    #이미지의 크기를 resize하는 옵션이면.
                    ratio = 0
                    ratio_h = RESIZE_IMAGE_WIDTH/crop_width
                    ratio_v = RESIZE_IMAGE_HEIGHT/crop_height
                    
                    if not args.fixratio :       # 영상을 스트레칭하는 옵션이면...
                        points[:,0] = (points[:,0] - crop_sx)*ratio_h
                        points[:,1] = (points[:,1] - crop_sy)*ratio_v
                    else :
                        if ratio_v <= ratio_h : # ratio_v 를 사용하는 경우
                            points[:,1] = (points[:,1] - crop_sy)*ratio_v
                            points[:,0] = (points[:,0] - crop_sx)*ratio_v + (RESIZE_IMAGE_WIDTH/2 - crop_width*ratio_v/2)
                        else :
                            points[:,0] = (points[:,0] - crop_sx)*ratio_h
                            points[:,1] = (points[:,1] - crop_sy)*ratio_h + (RESIZE_IMAGE_HEIGHT/2 - crop_height*ratio_h/2)
        
                    
                    
                    shape['points'] = list(points) # 값을 업데이트 한다.
                    new_shapes.append(shape)
            
            if find_object :
            
                json_data['shapes'] = new_shapes #최종 업데이트를 한다.
                        
                #이미지 파일이 있는지 확인한다.
                if not os.path.exists(os.path.join(args.image_dir,json_data['imagePath'])) :
                    print("Error! no image file:",json_data['imagePath'])
                    continue
                    
                src_image_filename = json_data['imagePath']
                
                #json 이미지 파일 이름을 바꾼다. 
                image_filename, image_ext = os.path.splitext(json_data['imagePath'])
                json_data['imagePath'] = ofilename + '_' + obj_name_ext + image_ext
                dst_image_filename = ofilename + '_' + obj_name_ext + image_ext
                
                #이미지의 크기를 리싸이즈한 크기로 변경한다.
                json_data['imageWidth'] = RESIZE_IMAGE_WIDTH
                json_data['imageHeight'] = RESIZE_IMAGE_HEIGHT
                #추가 내용을 저장하다. 
                json_data['parentImageWidth'] = image_width
                json_data['parentImageHeight'] = image_height
                json_data['parentImageName'] = src_image_filename
            

                #json 파일로 저장한다.
                try:
                    with open( os.path.splitext(os.path.join(DEFAULT_OUPUT_PATH,ofilename))[0]+'_' + obj_name_ext+'.json','w', encoding='utf-8') as f:
                        json.dump(json_data,f,ensure_ascii=False,indent="\t", cls=NpEncoder)
                        
                except IOError:
                    print("Error: json write error")
                    continue
                
                #영상 파일을 연다.
                try:
                    #img = Image.open(os.path.join(args.image_dir,src_image_filename))
                    img_array = np.fromfile(os.path.join(args.image_dir,src_image_filename), np.uint8)
                    img_np = cv2.imdecode(img_array, cv2.IMREAD_COLOR)
                    box = list(range(0,4))
                    box_sy = int(crop_sy)
                    box_sx= int(crop_sx)
                    box_ey = int(crop_ey+1)
                    box_ex= int(crop_ex+1)
                    cropped_imag_np = img_np[box_sy:box_ey,box_sx:box_ex,:]
                    if not args.fixratio :       # 영상을 스트레칭하는 옵션이면...
                        desired_size = (RESIZE_IMAGE_HEIGHT,RESIZE_IMAGE_WIDTH)
                        dst_np = cv2.resize(cropped_imag_np,desired_size,interpolation=cv2.INTER_LINEAR)
                        
                    else :
                        desired_size = max(RESIZE_IMAGE_HEIGHT,RESIZE_IMAGE_WIDTH)
                        old_size = [cropped_imag_np.shape[1],cropped_imag_np.shape[0]]
                        ratio = float(desired_size)/max(old_size)
                        new_size = tuple([int(x*ratio) for x in old_size])
                        #원영상에서 ratio 만큼 곱하여 리싸이즈한 번호판 영상을 얻는다.
                        cropped_img = cv2.resize(cropped_imag_np,new_size,interpolation=cv2.INTER_LINEAR)
                        dst_np = np.zeros((desired_size, desired_size, 3), dtype = "uint8")
                        dst_np = cv2.cvtColor(dst_np, cv2.COLOR_BGR2RGB)
                        h = new_size[1]
                        w = new_size[0]
                        yoff = round((desired_size-h)/2)
                        xoff = round((desired_size-w)/2)
                        #320x320영상에 번호판을 붙여 넣는다.
                        dst_np[yoff:yoff+h, xoff:xoff+w , :] = cropped_img

                    imwrite(os.path.join(args.output_path, dst_image_filename),dst_np)
                    
                except IOError:
                    print("Error: open jpeg imaeg error {0}".format(src_image_filename))
                    continue

            
    else :
        print("Error! no json directory:",args.json_dir)       