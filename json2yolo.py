# -*- coding: utf-8 -*-
"""
Created on Thu Oct 13 09:42:39 2022
json과 영상을 읽어서 yolo 포멧으로 변경하는 기능을 한다.
@author: headway
"""

import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile, move
from label_tools import *
from label_tools import NpEncoder
from pathlib import Path
#------------------------------
# 수정할 내용

OUTPUT_FOLDER_NAME = 'result' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
IMAGE_FOLDER_NAME = 'images' #csv 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'images' #json 폴더가 있는 경로
MIDDLE_PATH = os.path.join('dataset','yolo') #'dataset' #os.path.join('dataset','test')

src_image_dir = r'E:\SPB_Data\labelconv\dataset\yolo\noblockMasking_data_1280x720' #image 파일에 있는 영상 파일이 있는 경로
src_image_dir = os.path.normpath(src_image_dir)
src_image_dir = Path(src_image_dir)

src_json_dir = r'E:\SPB_Data\labelconv\dataset\yolo\noblockMasking_data_1280x720' #json 파일에 있는 영상 파일이 있는 경로
src_json_dir = os.path.normpath(src_json_dir)
src_json_dir = Path(src_json_dir)


yolo_out_dir = r'E:\SPB_Data\labelconv\dataset\yolo\noblockMasking_data_1280x720_out' # 파일에 있는 영상 파일이 있는 경로
yolo_out_dir = os.path.normpath(yolo_out_dir)
yolo_out_dir = Path(yolo_out_dir)



IMAGE_FILE_COPY = True #이미지 파일 복사 여부
ADD_RECOGNITION_RESULT = True #인식 결과를 파일에 붙일지 여부
#DEFAULT_LABEL_FILE = "./LPR_Car-Plate_Labels.txt"  #라벨 파일이름
DEFAULT_LABEL_FILE =  "./tracking labels_자동차만.txt"
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = src_image_dir  #os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = src_json_dir #os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)
DEFAULT_OUPUT_PATH = yolo_out_dir #os.path.join(ROOT_DIR,MIDDLE_PATH,OUTPUT_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="json-to-yolo label converter")
parser.add_argument("-l",
                    "--labelfile",
                    help="Label file where the text files are stored.",
                    type=str,default=DEFAULT_LABEL_FILE)
parser.add_argument("-j",
                    "--json_dir",
                    help="Complete Path to the file where the input .csv files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)


args = parser.parse_args()

fLabels = pd.read_csv(args.labelfile, header = None )
CLASS_NAMES = fLabels[0].values.tolist()
HUMAN_NAMES= dict(zip(CLASS_NAMES, fLabels[1].values.tolist()))

#yolo 포멧으로 변경한다.
def yolo_convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)


# 디렉토리 생성
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)
    
if not os.path.exists(args.json_dir) :
    print("No json folder exists. check the folder :",args.json_dir)
    sys.exit(0)

if not os.path.exists(args.output_path) :
    createFolder(args.output_path)

json_data = OrderedDict()

ofilename = ""
# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.json_dir):

    json_ext = 'json'
    file_names = [fn for fn in os.listdir(args.json_dir)
                  if any(fn.endswith(ext) for ext in json_ext)]
   
    process_num = len(file_names)
    
    print("Total process file count is {0}".format(process_num))
    
    for filename in file_names :

        print("processing : {0}".format(filename))
        
        #json  파일을 연다.
        try:
            
            with open(os.path.join(args.json_dir,filename), 'r',encoding="UTF-8") as f:
                json_data = json.load(f)
                
        except IOError:
                print("Error: File does not appear to exist")
                continue
        
        image_width = int (json_data['imageWidth'])
        image_height = int (json_data['imageHeight'])
        
        basename, ext = os.path.splitext(filename)

        img_size = [image_width, image_height]

        #이미지 파일이 있는지 확인한다.
        if not os.path.exists(os.path.join(args.image_dir,json_data['imagePath'])) :
            print("Error! no image file:",json_data['imagePath'])
            continue
            
        src_image_filename = json_data['imagePath']
        
        ofilename = basename 
        
        #json 이미지 파일 이름을 바꾼다. 
        image_filename, image_ext = os.path.splitext(json_data['imagePath'])
        dst_image_filename = ofilename + image_ext
        writelines = []
        line = None
        for item, shape in enumerate(json_data['shapes']):
            label = shape['label']
            
            if label in CLASS_NAMES:
                
                find_object = True
                obj_name_ext = HUMAN_NAMES[label]
                points = np.array(shape['points']).astype(int) # numpy로 변형
                shape_type = shape['shape_type']
                
                # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
                tpoints = []
                if shape_type == 'rectangle':
                    tpoints = box2polygon(points) #test point를 polygon으로 만든다.
                else:
                    tpoints = points
                    
                #줄이기 전에 잘라낼 위치를 정한다.
                crop_xs = points[:,0]
                crop_ys = points[:,1]

                crop_sx = np.min(crop_xs,axis=0)
                if crop_sx < 0:
                    crop_sx = 0
                crop_sy = np.min(crop_ys,axis=0)
                if crop_sy < 0:
                    crop_sy = 0
                crop_ex = np.max(crop_xs,axis=0)
                if crop_ex >= image_width:
                    crop_ex = image_width - 1
                crop_ey = np.max(crop_ys,axis=0)
                if crop_ey >= image_height:
                    cropey = image_height - 1
                crop_width =  crop_ex - crop_sx
                crop_height = crop_ey - crop_sy
                box = (crop_sx, crop_ex, crop_sy, crop_ey)
                
                bb = yolo_convert(img_size, box)
                class_num = CLASS_NAMES.index(label)
                line = '{0} {1:.6f} {2:.6f} {3:.6f} {4:.6f}\n'.format(class_num, bb[0], bb[1], bb[2], bb[3])
                writelines.append(line) 
                
            else:
                print('레이블 목록에 없습니다. {} filename = {}'.format(label,filename))

        #json 파일로 저장한다.
        try:
            with open( os.path.splitext(os.path.join(DEFAULT_OUPUT_PATH,ofilename))[0]+'.txt','w', encoding='utf-8') as f:
                f.writelines(writelines)
                
        except IOError:
            print("Error: json write error")
            continue
            
        if IMAGE_FILE_COPY : # 파일을 result 디렉토리에 복사한다.
            src = os.path.join(args.image_dir,src_image_filename)
            dst = os.path.join(args.output_path,dst_image_filename)
            if os.path.exists(src):
                copyfile(src,dst)
                #move(src,dst)
            else:
                print("Error! no image file:",filename)
            
          
else :
    print("Error! no json directory:",args.json_dir)