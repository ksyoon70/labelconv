"""
Created on Tue Mar 15 14:19:22 2022
영상을 읽어 원해는 해상도로 resize하고 json로 해상도에 맞추어 바꾼 후 저장한다.
@author:  윤경섭
"""

import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile
from label_tools import insertlabel_with_points, PolygonOverlab,SupressInBox,box2polygon
import numpy as np
from label_tools import NpEncoder
from PIL import Image

#------------------------------
# 수정할 내용

OUTPUT_FOLDER_NAME = 'out' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
IMAGE_FOLDER_NAME = 'images' #이미지 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'annots' #json 폴더가 있는 경로
MIDDLE_PATH =  os.path.join('dataset','training')
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)
DEFAULT_OUPUT_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,OUTPUT_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="Sample TensorFlow XML-to-TFRecord converter")
parser.add_argument("-j",
                    "--json_dir",
                    help="Complete Path to the file where the input .csv files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
# 이미지의 크기를 일정 크기로 변경한다.
parser.add_argument('-r', '--resize', type=int, nargs=2,help='resize imagesize width, height',default=[320,320], required=False)


parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)


args = parser.parse_args()

resize = []

b_RESIZE = False
RESIZE_IMAGE_WIDTH = 0
RESIZE_IMAGE_HEIGHT = 0

if args.resize[0] : 
    b_RESIZE = True

for i in args.resize:
    resize.append(i)

RESIZE_IMAGE_WIDTH = resize[0]
RESIZE_IMAGE_HEIGHT = resize[1]

# 디렉토리 생성
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)
    
if not os.path.exists(args.json_dir) :
    print("No json folder exists. check the folder :",args.json_dir)
    sys.exit(0)

if not os.path.exists(args.output_path) :
    createFolder(args.output_path)

json_data = OrderedDict()

ofilename = ""
# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.json_dir):

    json_ext = 'json'
    file_names = [fn for fn in os.listdir(args.json_dir)
                  if any(fn.endswith(ext) for ext in json_ext)]
   
    process_num = len(file_names)
    
    print("Total process file count is {0}".format(process_num))
    
    for filename in file_names :

        print("Processing : {0}".format(filename))
        
        #json  파일을 연다.
        try:
            
            with open(os.path.join(args.json_dir,filename), 'r',encoding="UTF-8") as f:
                json_data = json.load(f)
                
        except IOError:
                print("Error: File does not appear to exist")
                continue
            
            
        json_data['imageData'] = None
        
        basename, ext = os.path.splitext(filename)
        
        ofilename = basename
        
        #번호판의 넓이 높이 이다.
        plate_width = None
        plate_height = None
        
        plate_polygon = None
        
       
        image_width = int (json_data['imageWidth'])
        image_height = int (json_data['imageHeight'])  

        new_shapes = []

        for item, shape in enumerate(json_data['shapes']):
            points = np.array(shape['points']).astype(int) # numpy로 변형
            shape_type = shape['shape_type']
            
            # rectangle 형태이면 폴리곤 타입으로 바꾸어 준다.
            tpoints = []
            if shape_type == 'rectangle':
                tpoints = box2polygon(points) #test point를 polygon으로 만든다.
            else:
                tpoints = points

            #이미지의 크기를 resize하는 옵션이면.
            ratio = 0
            if image_width >= image_height:
                ratio = RESIZE_IMAGE_WIDTH / image_width
                points[:,0] = points[:,0]*ratio
                points[:,1] = points[:,1]*ratio + (RESIZE_IMAGE_HEIGHT/2 - image_height*ratio/2)
            else :
                ratio = RESIZE_IMAGE_HEIGHT / plate_height
                points[:,1] = points[:,1]*ratio
                points[:,0] = points[:,0]*ratio + (RESIZE_IMAGE_WIDTH/2 - image_width*ratio/2)
            
            shape['points'] = list(points) # 값을 업데이트 한다.
            new_shapes.append(shape)

            json_data['shapes'] = new_shapes
                    
     
        
        #이미지 파일이 있는지 확인한다.
        if not os.path.exists(os.path.join(args.image_dir,json_data['imagePath'])) :
            print("Error! no image file:",json_data['imagePath'])
            continue
            
        src_image_filename = json_data['imagePath']
        
        #json 이미지 파일 이름을 바꾼다. 
        image_filename, image_ext = os.path.splitext(json_data['imagePath'])
        json_data['imagePath'] = ofilename + image_ext
        dst_image_filename = ofilename + image_ext
        
        #이미지의 크기를 리싸이즈한 크기로 변경한다.
        json_data['imageWidth'] = RESIZE_IMAGE_WIDTH
        json_data['imageHeight'] = RESIZE_IMAGE_HEIGHT
        #추가 내용을 저장하다. 
        json_data['parentImageWidth'] = image_width
        json_data['parentImageHeight'] = image_height
        json_data['parentImageName'] = src_image_filename
       

        #json 파일로 저장한다.
        try:
            with open( os.path.splitext(os.path.join(DEFAULT_OUPUT_PATH,ofilename))[0]+'.json','w', encoding='utf-8') as f:
                json.dump(json_data,f,ensure_ascii=False,indent="\t", cls=NpEncoder)
                
        except IOError:
            print("Error: json write error")
            continue
        
        #영상 파일을 연다.
        try:
            img = Image.open(os.path.join(args.image_dir,src_image_filename))

            #cropped_img.show()

            old_size = img.size
            desired_size = max(RESIZE_IMAGE_WIDTH,RESIZE_IMAGE_HEIGHT)
            ratio = float(desired_size)/max(old_size)
            new_size = tuple([int(x*ratio) for x in old_size])
            img = img.resize(new_size, Image.ANTIALIAS)
            new_im = Image.new("RGB", (desired_size, desired_size))
            new_im.paste(img, ((desired_size-new_size[0])//2,
                (desired_size-new_size[1])//2))
            #new_im.show()
            new_im.save( os.path.join(args.output_path, dst_image_filename))
            
        except IOError:
            print("Error: open jpeg imaeg error {0}".format(src_image_filename))
            continue

          
else :
    print("Error! no json directory:",args.json_dir)       


