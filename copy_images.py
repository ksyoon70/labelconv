#-*- encoding: utf8 -*-
"""
Created on Thu Jan  5 18:25:18 2023
이 파일은 디렉토리간 파일을 복사할 때 사용한다.
@author: headway
"""

import os, shutil
import sys
from pathlib import Path, PureWindowsPath
from pathlib import Path
from label_tools import *
#------------------------------------------------

src_dir = r'Z:\01_프로젝트\01_객체추적\01.YOLO_학습용영상\masked_data_1280x720\images'
src_dir = os.path.normpath(src_dir)
src_dir = Path(src_dir)

ROOT_DIR = os.path.dirname(__file__)
MIDDLE_PATH = 'dataset'
DEST_ROOT_FOLDER_NAME = 'yolo'
IMAGE_FOLDER_NAME = 'images'
DEFAULT_DEST_IMAGES_PATH = r'E:\SPB_Data\labelconv\dataset\yolo\images'
#DEFAULT_DEST_IMAGES_PATH = r'Y:\영상라벨링\작업완료\56차_시즌2_(0619)\images'
DIV_COUNT = 20

JSON_COPY = True

MOVE_FILE = True

src_json_dir = r'Z:\01_프로젝트\01_객체추적\01.YOLO_학습용영상\masked_data_1280x720\labels'
src_json_dir = os.path.normpath(src_json_dir)
src_json_dir = Path(src_json_dir)

#------------------------------------------------

dst_dir = DEFAULT_DEST_IMAGES_PATH
dst_dir = os.path.normpath(dst_dir)
dst_dir = Path(dst_dir)

if not os.path.exists(src_dir) :
    print("Error : source images folder exists. check the folder : {}".format(src_dir))
    sys.exit(0)

if not os.path.exists(dst_dir) :
    #print("Error : dest images folder exists. check the folder :",dst_dir)
    #sys.exit(0)
    print("Error : dest images folder exists. So make new folder : {}".format(dst_dir))
    createFolder(dst_dir)

totalcount = 7389
proccnt = 0
count = 0

#총파일 갯수 파악
#for path, dirs, files in os.walk(src_dir):
#    totalcount += len(files)

print('총파일은 {}개 '.format(totalcount))
mod = int(totalcount / DIV_COUNT)  # 기본 5%마다 표시
    
for path, dirs, files in os.walk(src_dir):
    for file in files:
        #print('processing : {}'.format(file))
        count += 1
        if count % mod == 0 :
            proccnt += 1
            print('processing : {}%'.format(proccnt*100/DIV_COUNT))
        src = os.path.join(path,file)
        dst = os.path.join(dst_dir,file)
        try:
            if MOVE_FILE:
                shutil.move(src, dst)
            else:
                shutil.copyfile(src, dst)

            if JSON_COPY :
                basename, ext = os.path.splitext(file)
                jfile = basename + '.txt'
                src = os.path.join(src_json_dir,jfile)
                dst = os.path.join(dst_dir,jfile)
                if os.path.exists(src):
                    if MOVE_FILE:
                        shutil.move(src, dst)
                    else:
                        shutil.copyfile(src, dst)

            if totalcount < count:
                break

        except IOError:
                print("Error: File does not appear to exist : {}".format(file))
                continue
    if totalcount < count:
        break   

print('작업을 완료했습니다')