#-*- encoding: utf8 -*-
"""
Created on Thu Jan  5 18:25:18 2023
이 파일은 샘플 파일에서 같은 파일을 찾아서 원하는 디렉토리로 복사 또는 이동한다.
@author: headway
"""

import os, shutil
import sys
from pathlib import Path, PureWindowsPath
from pathlib import Path
#------------------------------------------------

src_dir = r'X:\영상라벨링\작업완료\72차_시즌2(1023)\images'
src_dir = Path(src_dir)
ref_dir =  r'D:\SPB_Data\labelconv\dataset\test\exception' #참조 디렉토리
ref_dir = Path(ref_dir)
ROOT_DIR = os.path.dirname(__file__)
MIDDLE_PATH = 'dataset'
DEST_ROOT_FOLDER_NAME = 'test'
IMAGE_FOLDER_NAME = 'temp'
SRC_IMAGE_MOVE = True  #이미지 이동 여부
DEFAULT_DEST_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,DEST_ROOT_FOLDER_NAME,IMAGE_FOLDER_NAME)
DIV_COUNT = 20
#------------------------------------------------

dst_dir = DEFAULT_DEST_IMAGES_PATH

if not os.path.exists(src_dir) :
    print("Error : source images folder exists. check the folder :",src_dir)
    sys.exit(0)
#참조할 디렉토리에 디렉토리가 있는지 확인한다.    
if not os.path.exists(ref_dir) :
    print("Error : reference images folder exists. check the folder :",ref_dir)
    sys.exit(0)

if not os.path.exists(dst_dir) :
    print("Error : dest images folder exists. check the folder :",dst_dir)
    sys.exit(0)

totalcount = 0
proccnt = 0
count = 0

#총파일 갯수 파악
for path, dirs, files in os.walk(src_dir):
    totalcount += len(files)
 
    
image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']

ref_imagefile_list = [fn for fn in os.listdir(ref_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]

total_ref_file_count = len(ref_imagefile_list)

print('총파일은 {}개 '.format(totalcount))
mod = int(total_ref_file_count / DIV_COUNT)  # 기본 5%마다 표시

for path, dirs, files in os.walk(src_dir):
    for file in files:
        s_basefilename, s_ext = os.path.splitext(file)
        if s_ext == '.json' : #json 파일은 비교하지 않는다.
            continue 
        for rfile in ref_imagefile_list :
             r_basefilename, r_ext = os.path.splitext(rfile)
             if  r_basefilename  in s_basefilename:  #원소스파일에 가져오려는 파일이 있으면.
                
                try:
                    # 이미지 파일을 복사한다.
                    src = os.path.join(path,file)
                    dst = os.path.join(dst_dir,file)
                    if os.path.isfile(src) :
                        if SRC_IMAGE_MOVE:
                            shutil.move(src, dst)
                        else:
                            shutil.copyfile(src, dst)
                    #json 파일이 있으면 복사한다.
                    json_file = s_basefilename + '.json'
                    src = os.path.join(path,json_file)
                    if os.path.isfile(src) :
                        dst = os.path.join(dst_dir,json_file)
                        if SRC_IMAGE_MOVE:
                            shutil.move(src, dst)
                        else:
                            shutil.copyfile(src, dst)
                    
                    count += 1
                    if count % mod == 0 :
                        proccnt += 1
                        print('processing : {:2f}%'.format(count*100/total_ref_file_count))
                    break
                    
                except IOError:
                        print("Error: File does not appear to exist")
                        continue 

print('작업을 완료했습니다')