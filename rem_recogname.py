"""
Created on Tue Mar 15 14:19:22 2022
인펙영상 파일에서 인식된 내용에 추가로 인식된 내용을 파일 명에서 지우는 역할을 한다.
이미지 파일이 있으면 json 파일과 이름을 동일하게 만든다.
example 
00317318_38더1739_38더1739.json
00317318_38더1739_38더1739.jpg
--> 
00317318_38더1739.json
00317318_38더1739.jpg

@author:  윤경섭
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:05:28 2022

@author: 윤경섭
"""

import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile, move
from label_tools import *
from label_tools import NpEncoder

#------------------------------
# 수정할 내용

OUTPUT_FOLDER_NAME = 'result' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
IMAGE_FOLDER_NAME = 'images' #csv 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'images' #json 폴더가 있는 경로
MIDDLE_PATH = os.path.join('dataset','test') #'dataset' #os.path.join('dataset','test')
IMAGE_FILE_COPY = True #이미지 파일 복사 여부
ADD_RECOGNITION_RESULT = True #인식 결과를 파일에 붙일지 여부
DEFAULT_LABEL_FILE = "./LPR_Labels1.txt"  #라벨 파일이름
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)
DEFAULT_OUPUT_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,OUTPUT_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="Sample TensorFlow XML-to-TFRecord converter")
parser.add_argument("-l",
                    "--labelfile",
                    help="Label file where the text files are stored.",
                    type=str,default=DEFAULT_LABEL_FILE)
parser.add_argument("-j",
                    "--json_dir",
                    help="Complete Path to the file where the input .csv files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)


args = parser.parse_args()

fLabels = pd.read_csv(args.labelfile, header = None )
CLASS_NAMES = fLabels[0].values.tolist()
HUMAN_NAMES= dict(zip(CLASS_NAMES, fLabels[1].values.tolist()))

# 디렉토리 생성
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)
    
if not os.path.exists(args.json_dir) :
    print("No json folder exists. check the folder :",args.json_dir)
    sys.exit(0)

if not os.path.exists(args.output_path) :
    createFolder(args.output_path)

json_data = OrderedDict()

ofilename = ""
# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.json_dir):

    json_ext = 'json'
    file_names = [fn for fn in os.listdir(args.json_dir)
                  if any(fn.endswith(ext) for ext in json_ext)]
   
    process_num = len(file_names)
    
    print("Total process file count is {0}".format(process_num))
    
    for filename in file_names :

        print("processing : {0}".format(filename))
        
        #json  파일을 연다.
        try:
            
            with open(os.path.join(args.json_dir,filename), 'r',encoding="UTF-8") as f:
                json_data = json.load(f)
                
        except IOError:
                print("Error: File does not appear to exist")
                continue
        plateName = GetPlateNameFromJson(json_data = json_data, enlabel = CLASS_NAMES, human_dic = HUMAN_NAMES )
            
        json_data['imageData'] = None
        
        json_data['number'] = plateName
        
        basename, ext = os.path.splitext(filename)
        
        index_1th_ch = filename.find('_')
        index_2th_ch = filename.rfind('_')
        if plateName == None:
            ofilename = filename[0:-5]
        else :
            if (index_1th_ch == -1 and index_2th_ch == -1):
                ofilename = filename[0:-5] + '_' + plateName
            elif index_1th_ch != index_2th_ch and (index_1th_ch != -1 and index_2th_ch != 0) :
                ofilename = filename[0:index_1th_ch + 1] + plateName
            elif index_1th_ch == index_2th_ch and (index_1th_ch != -1 and index_2th_ch != 0) :
                #type8-93735_93더3735.json
                ofilename = filename[0:index_1th_ch + 1] + plateName
            else:
                print("Error! not recoginition filename:",filename)
                continue
            #이미지 파일이 있는지 확인한다.
            if not os.path.exists(os.path.join(args.image_dir,json_data['imagePath'])) :
                print("Error! no image file:",json_data['imagePath'])
                continue
            
        src_image_filename = json_data['imagePath']
        
        #json 이미지 파일 이름을 바꾼다. 
        image_filename, image_ext = os.path.splitext(json_data['imagePath'])
        json_data['imagePath'] = ofilename + image_ext
        dst_image_filename = ofilename + image_ext
       

        #json 파일로 저장한다.
        try:
            with open( os.path.splitext(os.path.join(DEFAULT_OUPUT_PATH,ofilename))[0]+'.json','w', encoding='utf-8') as f:
                json.dump(json_data,f,ensure_ascii=False,indent="\t", cls=NpEncoder)
                
        except IOError:
            print("Error: json write error")
            continue
            
        if IMAGE_FILE_COPY : # 파일을 result 디렉토리에 복사한다.
            src = os.path.join(args.image_dir,src_image_filename)
            dst = os.path.join(args.output_path,dst_image_filename)
            if os.path.exists(src):
                copyfile(src,dst)
                #move(src,dst)
            else:
                print("Error! no image file:",filename)
            
          
else :
    print("Error! no json directory:",args.json_dir)            


