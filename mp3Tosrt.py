import speech_recognition as sr
from pydub import AudioSegment
from pydub.silence import split_on_silence
import os, sys
from pathlib import Path

#-------------변경할 내용 설정-----------------------------------
# MP3 파일 경로
file_path = r'D:\share\20240716_atc_a_victory_for_opponents_of_female_genital_mutilation_in_the_gambia.mp3'
ffmpeg_path = r'C:\ffmpeg\bin'
#---------------------------------------------------------------

# 파일 경로 설정
file_path = os.path.normpath(file_path)
ffmpeg_path = os.path.normpath(ffmpeg_path)

# FFmpeg 경로 설정
AudioSegment.converter = os.path.join(ffmpeg_path, 'ffmpeg.exe')
AudioSegment.ffmpeg = os.path.join(ffmpeg_path, 'ffmpeg.exe')
AudioSegment.ffprobe = os.path.join(ffmpeg_path, 'ffprobe.exe')

# MP3를 WAV로 변환
audio = AudioSegment.from_mp3(file_path)
wav_path = f"{os.path.splitext(os.path.basename(file_path))[0]}.wav"
audio.export(wav_path, format="wav")

# 침묵을 기준으로 오디오를 문장 단위로 나누기
# min_silence_len: 최소 침묵 길이(ms), silence_thresh: 음성 인식 기준 데시벨 값
audio_chunks = split_on_silence(
    audio, 
    min_silence_len=350,  # 0.5초 이상의 침묵을 문장 경계로 간주
    silence_thresh=-40  # -40 dB 이하의 소리면 침묵으로 간주
)

print(f"총 {len(audio_chunks)}개의 문장 단위 구간이 발견되었습니다.")

# 음성 인식 및 SRT 파일 생성
recognizer = sr.Recognizer()
srt_data = []
sentence_index = 1
current_time = 0  # 시간 계산을 위한 변수

# 각 문장 청크를 처리하여 SRT 파일로 변환
for i, chunk in enumerate(audio_chunks):
    # 각 문장 단위 오디오 청크 처리
    chunk_silent = AudioSegment.silent(duration=500)  # 각 문장 구간 사이에 0.5초 침묵 추가
    audio_chunk = chunk_silent + chunk + chunk_silent

    # 청크의 길이(초 단위)
    chunk_duration = len(audio_chunk) / 1000.0  # ms 단위를 초 단위로 변환

    # 임시 파일로 저장
    chunk_filename = f"chunk_{i}.wav"
    audio_chunk.export(chunk_filename, format="wav")

    with sr.AudioFile(chunk_filename) as source:
        audio_data = recognizer.record(source)

        try:
            # Google Speech Recognition으로 문장 단위 텍스트 인식
            text = recognizer.recognize_google(audio_data, language='en-US')
            
            # 문장 시작 시간과 종료 시간 계산
            start_time = current_time
            end_time = start_time + chunk_duration

            # SRT 형식의 시간 정보로 변환
            start_srt_time = f"{int(start_time//3600):02}:{int((start_time%3600)//60):02}:{int(start_time%60):02},000"
            end_srt_time = f"{int(end_time//3600):02}:{int((end_time%3600)//60):02}:{int(end_time%60):02},000"

            # SRT 파일 형식의 문장 추가
            srt_entry = f"{sentence_index}\n{start_srt_time} --> {end_srt_time}\n{text}\n"
            srt_data.append(srt_entry)

            # 현재 시간 업데이트
            current_time = end_time
            sentence_index += 1

        except sr.UnknownValueError:
            print(f"문장 {sentence_index}: 음성 인식 실패.")
            srt_entry = f"{sentence_index}\n{start_srt_time} --> {end_srt_time}\n[인식 실패]\n"
            srt_data.append(srt_entry)
            sentence_index += 1
        except sr.RequestError as e:
            print(f"Google 음성 인식 서비스에 접근할 수 없습니다; {e}")
            sys.exit(1)

    # 임시 파일 삭제
    os.remove(chunk_filename)

# SRT 파일로 저장
srt_file_path = f"{os.path.splitext(file_path)[0]}.srt"
with open(srt_file_path, "w", encoding='utf-8') as srt_file:
    srt_file.writelines(srt_data)

print(f"SRT 파일 생성 완료: {srt_file_path}")
