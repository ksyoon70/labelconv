"""
Created on 2022년 7월 27일
차량 영상 및 해당 json을 읽어 지역문자, 용도문자, 번호를 읽어내어 가로 세로의 비 표시하고, 평균 가로 세로 크기를 알려 준다.
영상을 리싸이즈 할때 어느정도로 해야 할지 알아보기 위하여 사용한다. 
예) python objsplit -i ./image -j ./annots  -t ch
-i 이미지 위치
-j json 위치
-t type ch: 문자  n: number r:지역문자
@author:  윤경섭
"""

import os,sys,shutil
import argparse
import pandas as pd
from label_tools import *
#------------------------------
# 수정할 내용


IMAGE_FOLDER_NAME = 'cropped' #이미지 파일에 있는 영상 파일이 있는 경로
JSON_FOLDER_NAME = 'cropped' #'annots' #json 폴더가 있는 경로
EXCEPTION_FOLDER_NAME = 'exception'  #예외로 뽑아낼 폴더
MIDDLE_PATH =  os.path.join('dataset','test')
DEFAULT_LABEL_FILE = "./LPR_Total_Labels.txt" #"./LPR_Labels1.txt"  #라벨 파일이름
CHECK_LIST = ['car','bus','motorcycle']
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_JSON_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,JSON_FOLDER_NAME)
DEFAULT_EXCEPT_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,EXCEPTION_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="object split and save in jpeg and annotation files")
parser.add_argument("-l",
                    "--labelfile",
                    help="Label file where the text files are stored.",
                    type=str,default=DEFAULT_LABEL_FILE)
parser.add_argument("-j",
                    "--json_dir",
                    help="Complete Path to the file where the input .json files are stored.",
                    type=str,default=DEFAULT_JSON_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
parser.add_argument("-d",
                    "--dest_dir",
                    help="Path to the folder where the suspicious image files are stored. ",
                    type=str, default=DEFAULT_EXCEPT_PATH)

args = parser.parse_args()


    
#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)
    
if not os.path.exists(args.json_dir) :
    print("No json folder exists. check the folder :",args.json_dir)
    sys.exit(0)

if not os.path.exists(args.dest_dir) :
    print('의심 영상 저장 폴더를 만듭니다. : {}'.format(args.dest_dir))
    createFolder(args.dest_dir)

    
json_data = OrderedDict()

ofilename = ""


# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.json_dir):

    json_ext = 'json'
    json_file_names = [fn for fn in os.listdir(args.json_dir)
                  if any(fn.endswith(ext) for ext in json_ext)]
   
    process_json_num = len(json_file_names)

    image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
    imagefile_list = [fn for fn in os.listdir(args.image_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]
    imagefile_num = len(imagefile_list)
    #이미지 이름 중에서 확장자를 뺀이름만 남긴다.
    image_basename_list = [os.path.splitext(file)[0] for file in imagefile_list]

    print("총 이미지 파일수 : {0}".format(imagefile_num))
    
    
    crop_width_sum = 0
    crop_height_sum = 0
    label_error = False
    for filename in imagefile_list :

        #print("Processing : {0}".format(filename))
        
        #json  파일을 연다.
        vehicle_cnt = 0
        try:
            json_file = os.path.splitext(filename)[0] + '.json'
            
            with open(os.path.join(args.json_dir,json_file), 'r',encoding="UTF-8") as f:
                json_data = json.load(f)
                
        except IOError:
                print("Error: {} does not appear to exist".format(json_file))
                continue
        
               
        for item, shape in enumerate(json_data['shapes']):
            label = shape['label']
            
            if label in CHECK_LIST: #차량인지 확인한다.
                vehicle_cnt += 1

        #json을 다 읽으면
        if vehicle_cnt >= 2:
            #영상 파일과 영상을 옮긴다.
            try:
                src_file = os.path.join(args.image_dir,filename)
                dst_file = os.path.join(args.dest_dir,filename)
                shutil.move(src_file,dst_file)
                #json 파일과 영상을 옮긴다.
                src_file = os.path.join(args.image_dir,json_file)
                dst_file = os.path.join(args.dest_dir,json_file)
                print('json 점검 : {} 파일을 {}롤 옮겼습니다'.format(filename,dst_file))
                shutil.move(src_file,dst_file)
            except IOError:
                print("Error: {} does not appear to exist".format(filename))
                continue               
     
          
else :
    print("Error! no json directory:",args.json_dir)       