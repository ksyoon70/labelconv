# -*- coding: utf-8 -*-
"""
Created on Sat Sep 10 16:36:21 2022
이미지들 중에서 train, validation 영상을 비율별로 나눈다.
기본 영상은 /datasets/labels/annots  /datasets/labels/imgases에 저장되어 있으며
아웃풋 영상은 /datasets/out/...아래로 저장된다.
번호판, 헬멧, 차량, 이륜차에 대한 train, validation 으로 나눈다.
@author: headway
"""

import os, shutil
import sys, random
#---------------------------------------------------------
train_ratio  = 0.8
valid_ratio = 1- train_ratio

base_dir = os.path.abspath(os.path.dirname(__file__))
database_dir  =  os.path.join(base_dir,'dataset')
labels_dir = os.path.join(database_dir,'labels')
annots_dir = os.path.join(labels_dir,'rear')
images_dir = os.path.join(labels_dir,'rear')

out_dir = os.path.join(labels_dir,'out')
out_train = os.path.join(out_dir,'train')
out_train_annot = os.path.join(out_train,'images')
out_train_image = os.path.join(out_train,'images')
out_valid = os.path.join(out_dir,'valid')
out_valid_annot = os.path.join(out_valid,'images')
out_valid_image = os.path.join(out_valid,'images')
#--------------------------------------------------------

if not os.path.exists(out_dir) :
    os.makedirs(out_dir)

if not os.path.exists(out_train) :
    os.makedirs(out_train)
    
if not os.path.exists(out_train_annot) :
    os.makedirs(out_train_annot)
    
if not os.path.exists(out_train_image) :
    os.makedirs(out_train_image)
    
if not os.path.exists(out_valid) :
    os.makedirs(out_valid)
    
if not os.path.exists(out_valid_annot) :
    os.makedirs(out_valid_annot)
    
if not os.path.exists(out_valid_image) :
    os.makedirs(out_valid_image)

image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
imagefile_list = [fn for fn in os.listdir(images_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]

total_files_count = len(imagefile_list)

train_cnt = int (total_files_count * train_ratio)
valid_cnt = total_files_count - train_cnt

random.shuffle(imagefile_list)


for filename in imagefile_list[0:train_cnt]:
    src_file = os.path.join(images_dir,filename)  
    dst_file = os.path.join(out_train_image,filename)
    shutil.copy(src_file,dst_file)
    src_file = os.path.join(annots_dir,os.path.splitext(filename)[0] +'.json')
    dst_file = os.path.join(out_train_annot,os.path.splitext(filename)[0] +'.json')
    shutil.copy(src_file,dst_file)
    
for filename in imagefile_list[train_cnt:]:
    src_file = os.path.join(images_dir,filename)  
    dst_file = os.path.join(out_valid_image,filename)
    shutil.copy(src_file,dst_file)
    src_file = os.path.join(annots_dir,os.path.splitext(filename)[0] +'.json')
    dst_file = os.path.join(out_valid_annot,os.path.splitext(filename)[0] +'.json')
    shutil.copy(src_file,dst_file)    
    
print('작업이 종료되었습니다.')
