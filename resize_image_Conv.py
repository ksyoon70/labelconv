"""
Created on Tue Mar 15 14:19:22 2022
영상을 읽어 원해는 해상도로 resize하여 해상도에 맞추어 저장한다.
@author:  윤경섭
"""

import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile
from label_tools import insertlabel_with_points, PolygonOverlab,SupressInBox,box2polygon
import numpy as np
from label_tools import NpEncoder
from PIL import Image

#------------------------------
# 수정할 내용

OUTPUT_FOLDER_NAME = 'out' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
IMAGE_FOLDER_NAME = 'images' #이미지 파일에 있는 영상 파일이 있는 경로
MIDDLE_PATH =  os.path.join('dataset','test')
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,IMAGE_FOLDER_NAME)
DEFAULT_OUPUT_PATH = os.path.join(ROOT_DIR,MIDDLE_PATH,OUTPUT_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="Sample TensorFlow XML-to-TFRecord converter")

parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)
# 이미지의 크기를 일정 크기로 변경한다.
parser.add_argument('-r', '--resize', type=int, nargs=2,help='resize imagesize width, height',default=[320,320], required=False)


parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)


args = parser.parse_args()

resize = []

b_RESIZE = False
RESIZE_IMAGE_WIDTH = 0
RESIZE_IMAGE_HEIGHT = 0

if args.resize[0] : 
    b_RESIZE = True

for i in args.resize:
    resize.append(i)

RESIZE_IMAGE_WIDTH = resize[0]
RESIZE_IMAGE_HEIGHT = resize[1]

# 디렉토리 생성
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)

if not os.path.exists(args.output_path) :
    createFolder(args.output_path)


ofilename = ""
# json 디렉토리에서 json 파일을 하나씩 읽어 들인다. 
if os.path.exists(args.image_dir):

        
    file_names = os.listdir(args.image_dir)
   
    process_num = len(file_names)
    
    print("Total process file count is {0}".format(process_num))
    
    for filename in file_names :

        print("Processing : {0}".format(filename))
        
       
        
        #영상 파일을 연다.
        try:
            img = Image.open(os.path.join(args.image_dir,filename))

            old_size = img.size
            desired_size = max(RESIZE_IMAGE_WIDTH,RESIZE_IMAGE_HEIGHT)
            ratio = float(desired_size)/max(old_size)
            new_size = tuple([int(x*ratio) for x in old_size])
            img = img.resize(new_size, Image.ANTIALIAS)
            new_im = Image.new("RGB", (desired_size, desired_size))
            new_im.paste(img, ((desired_size-new_size[0])//2,
                (desired_size-new_size[1])//2))
            #new_im.show()
            new_im.save( os.path.join(args.output_path, filename))
            
        except IOError:
            print("Error: open jpeg imaeg error {0}".format(filename))
            continue

          
else :
    print("Error! no json directory:",args.json_dir)       


