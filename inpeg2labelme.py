# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:05:28 2022

@author: 윤경섭
"""

import os,sys
import pandas as pd
import argparse
import json
from collections import OrderedDict
from PIL import Image
from shutil import copyfile
from label_tools import insertlabel_with_points

#------------------------------
# 수정할 내용
CSV_FILENAME = 'new_gt37.csv' #inpeg 에서 레이블링한 csv 파일 이름
OUTPUT_FOLDER_NAME = 'result' # labelme로 출력할 디렉토리 이름 (현재 디렉토리 아래로 저장된다.)
IMAGE_FOLDER_NAME = 'images' #csv 파일에 있는 영상 파일이 있는 경로
IMAGE_FILE_COPY = True #이미지 파일 복사 여부
ADD_RECOGNITION_RESULT = True #인식 결과를 파일에 붙일지 여부
#------------------------------

ROOT_DIR = os.path.dirname(__file__)
DEFAULT_IMAGES_PATH = os.path.join(ROOT_DIR,IMAGE_FOLDER_NAME)
DEFAULT_CSV_PATH = os.path.join(ROOT_DIR,CSV_FILENAME)
DEFAULT_OUPUT_PATH = os.path.join(ROOT_DIR,OUTPUT_FOLDER_NAME)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="Sample TensorFlow XML-to-TFRecord converter")
parser.add_argument("-c",
                    "--csv_filename",
                    help="Complete Path to the file where the input .csv files are stored.",
                    type=str,default=DEFAULT_CSV_PATH)
parser.add_argument("-o",
                    "--output_path",
                    help="Path of output images and jsons", type=str,default=DEFAULT_OUPUT_PATH)
parser.add_argument("-i",
                    "--image_dir",
                    help="Path to the folder where the input image files are stored. ",
                    type=str, default=DEFAULT_IMAGES_PATH)

args = parser.parse_args()

# 디렉토리 생성
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


#이미지 폴더가 있는지 확인한다.

if not os.path.exists(args.image_dir) :
    print("No images folder exists. check the folder :",args.image_dir)
    sys.exit(0)

if not os.path.exists(args.output_path) :
    createFolder(args.output_path)

json_data = OrderedDict()

if os.path.exists(args.csv_filename):
    reader = pd.read_csv(filepath_or_buffer=args.csv_filename, encoding="utf-8", sep=",")
    rows = reader.shape[0]
    cols = reader.shape[1]
    for row in range(rows) :
        filename = reader.values[row, 0]
        print("filename:",filename)
        
        #이미지 파일을 읽어 본다.
        img_path = os.path.join(DEFAULT_IMAGES_PATH,filename)
        
        image_height = 0
        image_width = 0
        
        if os.path.exists(img_path) :
            im = Image.open(img_path)
            image_width, image_height = im.size
        else:
            print("Error! no image file:",filename)
        
        region_shape_attributes = json.loads( reader.values[row, 5])
        all_points_x = region_shape_attributes['all_points_x']
        all_points_y = region_shape_attributes['all_points_y']
        
        points_xy= [ [x,y] for x, y in zip(all_points_x ,all_points_y)] 
       
        #print('all_points_x:',region_shape_attributes['all_points_x'])
        #print('all_points_y:',region_shape_attributes['all_points_y'])
        region_attributes = json.loads(reader.values[row, 6])
        #print('region:',region_attributes['region'])
        #print('type:',region_attributes['type'])
        #print('usage:',region_attributes['usage'])
        #print('number:',region_attributes['number'])
        commercial = region_attributes['commercial']

        # labelme용 json 파일을 만들어 결과 파일에 저장한다.
        shapes=[]
        insertlabel_with_points(shapes,points_xy,label='plate')
        """
        plate_info_dic = {'label':'plate'}
        plate_info_dic['points']= points_xy
        plate_info_dic['group_id'] = None
        plate_info_dic["shape_type"] = "polygon"
        plate_info_dic["flags"] = {}
        shapes.append(plate_info_dic)
        """
        json_data['version'] = '5.0.1'
        json_data['flags'] = {}
        json_data['shapes'] = shapes
        
        json_data['imageData'] = None
        json_data['imageHeight'] = image_height
        json_data['imageWidth'] = image_width

        json_data['region'] = region_attributes['region']
        json_data['type'] = region_attributes['type']
        json_data['usage'] = region_attributes['usage']
        json_data['number'] = region_attributes['number']
        if commercial:
            json_data['commercial'] = commercial['bool']
        else :
            json_data['commercial'] = None
        
        #json 화면 덤프 기능
        #print(json.dumps(json_data, ensure_ascii=False,indent="\t"))
        
        basename, ext = os.path.splitext(filename)
        #인식결과를 파일명에 표시하려면.
        if ADD_RECOGNITION_RESULT :
            ofilename = basename +  '_' + region_attributes['region'] + region_attributes['type'] + region_attributes['usage'] + region_attributes['number']  + ext
        else:
            ofilename = filename
            
        json_data['imagePath'] = ofilename

        #json 파일로 저장한다.
        with open( os.path.splitext(os.path.join(DEFAULT_OUPUT_PATH,ofilename))[0]+'.json','w', encoding='utf-8') as f:
            json.dump(json_data,f,ensure_ascii=False,indent="\t")
            
        if IMAGE_FILE_COPY : # 파일을 result 디렉토리에 복사한다.
            src = os.path.join(DEFAULT_IMAGES_PATH,filename)
            dst = os.path.join(DEFAULT_OUPUT_PATH,ofilename)
            if os.path.exists(src):
                copyfile(src,dst)
            else:
                print("Error! no image file:",filename)
            
          
else :
    print("Error! no csv filename:",args.csv_filename)            