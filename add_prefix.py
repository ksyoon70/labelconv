# 이미지와 json 파일에 공통적으로 prefix를 붙이는 코드이다.
# 작성자 윤경섭
# 작성일 2022년 12월 14일

import os, shutil
import sys
import argparse
import json	
from label_tools import *
#---------------------------------------------------------
base_dir = os.path.abspath(os.path.dirname(__file__))
database_dir  =  os.path.join(base_dir,'dataset')
labels_dir = os.path.join(database_dir,'test')
annots_dir = os.path.join(labels_dir,'images')
images_dir = os.path.join(labels_dir,'images')
out_dir = os.path.join(labels_dir,'out')
PRE_FIX = '008_'       #ex_slane_ 도공단차로 ex_mlane_ 도공다차로
#---------------------------------------------------------

if not os.path.exists(out_dir) :
    os.makedirs(out_dir)

# Initiate argument parser
parser = argparse.ArgumentParser(
    description="이미지 파일과 json 파일에서 원하는 타입의 번호판 만 추출하는 스크립트이다")


args = parser.parse_args()    

image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
imagefile_list = [fn for fn in os.listdir(images_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]

total_files_count = len(imagefile_list)

print('검토할 총 영상 수: {}'.format(total_files_count))

for filename in imagefile_list:
    
    # 해당 이미지에 해당하는 json 파일을 찾는다.
    basename, ext = os.path.splitext(filename)
    new_basename = PRE_FIX + basename  #새로운 이름.
    jsonfilename = os.path.splitext(filename)[0] +'.json'
    if os.path.isfile(os.path.join(annots_dir,jsonfilename)):
        #json 파일을 읽는다.
        
        with open(os.path.join(annots_dir,jsonfilename), 'r',encoding="UTF-8") as f:
            json_data = json.load(f)
            json_data['imagePath'] = new_basename + ext
            
                
        f.close
        
        src_file = os.path.join(images_dir,filename) 
        dst_file = os.path.join(out_dir,new_basename + ext)
        shutil.copy(src_file,dst_file) 
                
        dst_file = os.path.join(out_dir,new_basename + '.json')
        
        
        #json 파일로 저장한다.
        try:
            with open(dst_file,'w', encoding='utf-8') as f:
                json.dump(json_data,f,ensure_ascii=False,indent="\t", cls=NpEncoder)
                
            f.close
                
        except IOError:
            print("Error: json write error")
            continue
    else:
        #jpeg 파일만 있다고 하면...
        try:
            src_file = os.path.join(images_dir,filename) 
            dst_file = os.path.join(out_dir,new_basename + ext)
            shutil.copy(src_file,dst_file)
        except IOError:
            print("Error: image write error")
            continue 
    
print('작업이 종료되었습니다.')