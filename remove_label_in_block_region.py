from SutherlandHodgman import  *
import os, shutil
import sys
from pathlib import Path
from label_tools import *
#------------------------------
# 이 파일을 json으로 이미 라벨링 된 영상 중에서 특정 영역의 polygon과 검지된 label과의 교집합의 면적이
#교집합 / rect 면적이 얼마 이상이 되면 그 레이블을 삭제하는 코드 이다.
#------------------------------
# 환경 설정 내용
# 지역명으로 동일 비디오 파일이 있는 영상 폴더

INTERSEC_TH = 0.5           # 가림 최저 값

ROOT_DIR = os.path.dirname(__file__)

video_src_dir = r'D:\test\경기북부_샘플영상\경기_연천_동막사거리'
video_src_dir = os.path.normpath(video_src_dir)
video_src_dir = Path(video_src_dir)

src_dir = r'E:\SPB_Data\labelconv\dataset\yolo\images1'
src_dir = os.path.normpath(src_dir)
src_dir = Path(src_dir)

block_src_dir = r'E:\SPB_Data\labelconv\dataset\yolo\경기_연천_동막사거리_영역'       # block 영역이 있는 부분있 있는 곳.
block_src_dir = os.path.normpath(block_src_dir)
block_src_dir = Path(block_src_dir)

dst_dir = r'E:\SPB_Data\labelconv\dataset\yolo\수정후'
dst_dir = os.path.normpath(dst_dir)
dst_dir = Path(dst_dir)

#수정된 파일 이동 여부
MOVE_FILE = True

ref_dir = r'E:\SPB_Data\labelconv\dataset\yolo\수정전'
ref_dir = os.path.normpath(ref_dir)
ref_dir = Path(ref_dir)

#찾아야 할 대상
vehicle_list = ['car', 'truck','bus', 'motorcycle']

#------------------------------

def ensure_ccw(polygon):
        """
        polygon: 리스트나 ndarray 형태의 [[x1, y1], [x2, y2], ..., [xN, yN]] 점들
        polygon을 반시계(CCW) 방향이 되도록 순서를 재배열하여 반환한다.
        """
        # Shoelace formula로 면적 계산 (Signed area)
        area = 0
        n = len(polygon)
        for i in range(n):
            x1, y1 = polygon[i]
            x2, y2 = polygon[(i + 1) % n]
            area += x1 * y2 - y1 * x2

        # area가 음수라면, 시계방향(CW)이므로 반시계(CCW)가 되도록 reverse
        if area < 0:
            polygon = polygon[::-1]

        return polygon

def polygon_area(points):
    """
    points: [(x0, y0), (x1, y1), ..., (x(n-1), y(n-1))] 형태의 리스트
    """
    area = 0.0
    n = len(points)

    for i in range(n):
        x1, y1 = points[i]
        x2, y2 = points[(i + 1) % n]  # 마지막 점 다음은 0번 점과 연결
        area += x1*y2 - y1*x2

    return abs(area) / 2.0

if not os.path.exists( os.path.join(video_src_dir)) :
    print("Error :{} 폴더가 존재하지 않습니다.".format(video_src_dir))
    sys.exit(0)

if not os.path.exists( os.path.join(src_dir)) :
    print("Error :{} 폴더가 존재하지 않습니다.".format(src_dir))
    sys.exit(0)

if not os.path.exists( os.path.join(block_src_dir)) :
    print("Error :{} 폴더가 존재하지 않습니다.".format(block_src_dir))
    sys.exit(0)

if not os.path.exists( os.path.join(dst_dir)) :
    print("Error :{} 폴더가 존재하지 않습니다. 생성합니다.".format(dst_dir))
    createFolder(os.path.join(dst_dir))


if not os.path.exists( os.path.join(ref_dir)) :
    print("Error :{} 폴더가 존재하지 않습니다. 생성합니다.".format(ref_dir))
    createFolder(os.path.join(ref_dir))

video_ext = ['mp4']
video_file_names = [fn for fn in os.listdir(video_src_dir)
                  if any(fn.endswith(ext) for ext in video_ext)]
video_file_list_prefix = [
    fn.rsplit('_', 1)[0] if '라벨링' in fn else fn.rsplit('.', 1)[0]
    for fn in video_file_names
]

if __name__ == "__main__":
    
    #마스크 영역을 읽는다.
    json_ext = ['json']
    json_mask_file_names = [fn for fn in os.listdir(block_src_dir)
                  if any(fn.endswith(ext) for ext in json_ext)]
     # Clip polygon (사각형)
    subject_polygon = []
    with open(os.path.join(block_src_dir,json_mask_file_names[0]), 'r',encoding="UTF-8") as f:
            json_data = json.load(f)
            
            image_width = int (json_data['imageWidth'])
            image_height = int (json_data['imageHeight'])
            #이미지의 중심을 구한다.
            image_cx  = image_width / 2
            image_cy = image_height / 2
            image_center =  [image_cx,image_cy]
            tvehicle_box = None
            for item, shape in enumerate(json_data['shapes']):
                label = shape['label']
                if label == 'block':
                    #[x,y] point 형식으로 받는다.
                    points = np.array(shape['points']).astype(int) # numpy로 변형
                    shape_type = shape['shape_type']
                    if shape_type == 'rectangle':
                        points = box2polygon(points) #test point를 polygon으로 만든다.
                    
                    box = np.array(points).astype(int)
                    box = box.tolist() 
                    box = ensure_ccw(box)
                    subject_polygon.append(box)  #[ pt for pt in box]

    if len(subject_polygon) > 0 :     #마스크 영역을 얻으면..

        #subject_polygon = ensure_ccw(subject_polygon)
        # json 파일들을 열어서 겹치는 영역 확인
        json_ext = ['json']
        json_files = [fn for fn in os.listdir(src_dir)
                    if any(fn.endswith(ext) for ext in json_ext)]
        jfile_contains_keywords = [jfile for jfile in json_files if any(keyword in jfile for keyword in video_file_list_prefix)]
        
        if len(jfile_contains_keywords) > 0 :
            for jfn in jfile_contains_keywords:
                modify = False      # json 파일 수정 여부
                with open(os.path.join(src_dir,jfn), 'r',encoding="UTF-8") as f:
                    json_data = json.load(f)

                    src_image = json_data['imagePath']

                    new_shapes = []  # 최종적으로 남길 shape들을 저장할 리스트
                    
                    for item, shape in enumerate(json_data['shapes']):
                        remove_shape = False
                        label = shape['label']
                        if label in vehicle_list :
                            #[x,y] point 형식으로 받는다.
                            points = np.array(shape['points']).astype(int) # numpy로 변형
                            shape_type = shape['shape_type']
                            if shape_type == 'rectangle':
                                points = box2polygon(points) #test point를 polygon으로 만든다.
                            
                            box = np.array(points).astype(int)

                            vbox = box.tolist()

                            vbox = ensure_ccw(vbox)
                            vbox_area = polygon_area(vbox)

                             # Create SutherlandHodgman instance
                            for sub_poly in subject_polygon:        #여러개의 block이 있을 경우에 대비하여 수정
                                sh = SutherlandHodgman(sub_poly, vbox)
                                # Clipping 수행
                                intersection_polygon = sh.clip()

                                # 교집합 면적 계산
                                area = sh.compute_area(intersection_polygon)
                                ratio = area / vbox_area
                                #print(f"교집합 면적: {area}")

                                if ratio > INTERSEC_TH:
                                    modify = True
                                    remove_shape = True
                                    break
                            if remove_shape :
                                continue
                            new_shapes.append(shape)

                if modify :
                    if MOVE_FILE:
                        #json 파일 백업
                        src = os.path.join(src_dir,jfn)
                        dst = os.path.join(ref_dir,jfn)
                        shutil.move(src, dst)
                        #이미지 파일 복사
                        src = os.path.join(src_dir,src_image)
                        dst = os.path.join(ref_dir,src_image)
                        shutil.copy(src, dst)

                    json_data['shapes'] = new_shapes

                    with open(os.path.join(dst_dir,jfn), "w", encoding="utf-8") as f_out:
                        json.dump(json_data,f_out,ensure_ascii=False,indent="\t", cls=NpEncoder)

                    #이미지 파일 이동
                    src = os.path.join(src_dir,src_image)
                    dst = os.path.join(dst_dir,src_image)
                    shutil.move(src, dst) 

print("작업이 끝났습니다.")




    

    