# -*- coding: utf-8 -*-
"""
Created on Sat Sep 10 16:36:21 2022
이미지들 중에서 train, validation 영상을 비율별로 나눈다.
기본 영상은 /datasets/labels/annots  /datasets/labels/imgases에 저장되어 있으며
아웃풋 영상은 /datasets/out/...아래로 저장된다.
번호판, 헬멧, 차량, 이륜차에 대한 train, validation 으로 나눈다.
@author: headway
"""

import os, shutil
import sys, random
from pathlib import Path
from label_tools import *
#---------------------------------------------------------
train_ratio  = 0.8
valid_ratio = 1- train_ratio

base_dir = os.path.abspath(os.path.dirname(__file__))

annots_ext = '.txt'  # json이면 '.json' 으로 바꾼다.

images_dir = r'E:\SPB_Data\labelconv\dataset\yolo\noblockMasking_data_1280x720_out'
images_dir = os.path.normpath(images_dir)
images_dir = Path(images_dir)


annots_dir = r'E:\SPB_Data\labelconv\dataset\yolo\noblockMasking_data_1280x720_out'
annots_dir = os.path.normpath(annots_dir)
annots_dir = Path(annots_dir)


abnormal_dir = r'E:\SPB_Data\labelconv\dataset\yolo\noblockMasking_data_1280x720_abnormal'
abnormal_dir = os.path.normpath(abnormal_dir)
abnormal_dir = Path(abnormal_dir)

out_train_annot = r'E:\SPB_Data\yolov8\data\jwis_tracking\train\labels'
out_train_annot = os.path.normpath(out_train_annot)
out_train_annot = Path(out_train_annot)

out_train_image = r'E:\SPB_Data\yolov8\data\jwis_tracking\train\images'
out_train_image = os.path.normpath(out_train_image)
out_train_image = Path(out_train_image)

out_valid_annot = r'E:\SPB_Data\yolov8\data\jwis_tracking\valid\labels'
out_valid_annot = os.path.normpath(out_valid_annot)
out_valid_annot = Path(out_valid_annot)

out_valid_image = r'E:\SPB_Data\yolov8\data\jwis_tracking\valid\images'
out_valid_image = os.path.normpath(out_valid_image)
out_valid_image = Path(out_valid_image)

""""
database_dir  =  os.path.join(base_dir,'dataset')
labels_dir = os.path.join(database_dir,'yolo')
annots_dir = os.path.join(labels_dir,'images')
images_dir = os.path.join(labels_dir,'images')

abnormal_dir = os.path.join(labels_dir,'abnormal')

out_dir = os.path.join(labels_dir,'out')
out_train = os.path.join(out_dir,'train')
out_train_annot = os.path.join(out_train,'images')
out_train_image = os.path.join(out_train,'images')
out_valid = os.path.join(out_dir,'valid')
out_valid_annot = os.path.join(out_valid,'images')
out_valid_image = os.path.join(out_valid,'images')
"""

DIV_COUNT = 20
#--------------------------------------------------------

if not os.path.exists(images_dir) :
    print("Error : source images folder exists. check the folder : {}".format(images_dir))
    sys.exit(0)

if not os.path.exists(annots_dir) :
    print("Error : source annotation folder exists. check the folder : {}".format(annots_dir))
    sys.exit(0)

if not os.path.exists(abnormal_dir) :
    print("Error : dest folder exists. So make new folder : {}".format(abnormal_dir))
    createFolder(abnormal_dir)

if not os.path.exists(out_train_annot) :
    print("Error : dest folder exists. So make new folder : {}".format(out_train_annot))
    createFolder(out_train_annot)

if not os.path.exists(out_train_image) :
    print("Error : dest folder exists. So make new folder : {}".format(out_train_image))
    createFolder(out_train_image)

if not os.path.exists(out_valid_annot) :
    print("Error : dest folder exists. So make new folder : {}".format(out_valid_annot))
    createFolder(out_valid_annot)

if not os.path.exists(out_valid_image) :
    print("Error : dest folder exists. So make new folder : {}".format(out_valid_image))
    createFolder(out_valid_image)

image_ext = ['jpg','JPG','png','PNG','jpeg','JPEG']
#image_ext = ['jpg','JPG','jpeg','JPEG']
imagefile_list = [fn for fn in os.listdir(images_dir)
                  if any(fn.endswith(ext) for ext in image_ext)]

total_files_count = len(imagefile_list)

train_cnt = int (total_files_count * train_ratio)
valid_cnt = total_files_count - train_cnt

random.shuffle(imagefile_list)

print('총파일은 {}개, train 파일은 {}개 validation 파일은 {}개'.format(total_files_count,train_cnt,valid_cnt))


mod = int(train_cnt / DIV_COUNT)  # 기본 5%마다 표시
proccnt = 0
count = 0
print('train split 시작')
for filename in imagefile_list[0:train_cnt]:
    src_file = os.path.join(images_dir,filename)  
    dst_file = os.path.join(out_train_image,filename)
    
    # with open(src_file, 'rb') as f:
    #     check_chars = f.read()[-2:]
    
    # if check_chars != b'\xff\xd9':
    #     print('Not complete image!!! {}'.format(filename))
    #     dst_file = os.path.join(abnormal_dir,filename)
    #     shutil.move(src_file,dst_file)
    #     src_file = os.path.join(annots_dir,os.path.splitext(filename)[0] +'.json')
    #     dst_file = os.path.join(abnormal_dir,os.path.splitext(filename)[0] +'.json')
    #     shutil.move(src_file,dst_file)
    # else:
    try:
        shutil.copy(src_file,dst_file)
    except IOError:
            print("Error: File does not appear to exist : {}".format(filename))
            continue
    src_file = os.path.join(annots_dir,os.path.splitext(filename)[0] + annots_ext)
    dst_file = os.path.join(out_train_annot,os.path.splitext(filename)[0] + annots_ext)
    try:
        shutil.copy(src_file,dst_file)
        count += 1
        if count % mod == 0 :
            proccnt += 1
            print('processing : {}%'.format(proccnt*100/DIV_COUNT))
        
    except IOError:
            print("Error: File does not appear to exist : {}".format(filename))
            continue
 
print('train split 완료')
print('validation split 시작')
mod = int(valid_cnt / DIV_COUNT)  # 기본 5%마다 표시
proccnt = 0
count = 0    
for filename in imagefile_list[train_cnt:]:
    src_file = os.path.join(images_dir,filename)  
    dst_file = os.path.join(out_valid_image,filename)
    
    # with open(src_file, 'rb') as f:
    #     check_chars = f.read()[-2:]
    # #jpg에 오류가 있으면 사용하지 않는다. png는 해당이 안되는 듯.
    # if check_chars != b'\xff\xd9':
    #     print('Not complete image!!! {}'.format(filename))
    #     dst_file = os.path.join(abnormal_dir,filename)
    #     shutil.move(src_file,dst_file)
    #     src_file = os.path.join(annots_dir,os.path.splitext(filename)[0] +'.json')
    #     dst_file = os.path.join(abnormal_dir,os.path.splitext(filename)[0] +'.json')
    #     shutil.move(src_file,dst_file)
    # else:
    try:
        shutil.copy(src_file,dst_file)
    except IOError:
            print("Error: File does not appear to exist : {}".format(filename))
            continue
    src_file = os.path.join(annots_dir,os.path.splitext(filename)[0] + annots_ext)
    dst_file = os.path.join(out_valid_annot,os.path.splitext(filename)[0] + annots_ext)
    try:
        shutil.copy(src_file,dst_file)
        count += 1
        if count % mod == 0 :
            proccnt += 1
            print('processing : {}%'.format(proccnt*100/DIV_COUNT))
    except IOError:
            print("Error: File does not appear to exist : {}".format(filename))
            continue    
print('validation split 완료')     
print('작업이 종료되었습니다.')
